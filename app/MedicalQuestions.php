<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MedicalQuestions extends Model
{
    public $timestamps = false;
    protected $table    = 'medical_questions';
    protected $fillable = ['questions', 'answers'];
}
