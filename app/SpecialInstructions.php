<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecialInstructions extends Model
{
    public $timestamps = false;
    protected $table    = 'special_instructions';
    protected $fillable = ['code', 'instructions'];
}
