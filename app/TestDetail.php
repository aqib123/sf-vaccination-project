<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestDetail extends Model
{
    protected $table    = 'test_details';
    protected $fillable = ['codes', 'sample_requirements', 'turnaround', 'special_instructions', 'sample_type_guide', 'test_id'];

    public function test () {
        return $this->hasOne(Tests::class, 'id', 'test_id');
    }
}
