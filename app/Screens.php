<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Screens extends Model
{
    protected $table    = 'screens_details';
    protected $fillable = ['name', 'url', 'association_id', 'code'];
}
