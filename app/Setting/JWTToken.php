<?php
/**
 * Created by PhpStorm.
 * User: Ayaz
 * Date: 6/22/2019
 * Time: 10:06 PM
 */

namespace App\Setting;


abstract class JWTToken
{
    /*
     *
     * @Length = bytes
     * @ExpireIn = Hours
     */
    const Length = 32;
    const ExpireIn = 24;
    const Revoked = 1;
    const NotRevoked = 0;
}