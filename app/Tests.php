<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tests extends Model
{
    public $timestamps = false;
    protected $table    = 'tests';
    protected $fillable = ['code', 'names'];
}
