<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TravellingDetails extends Model
{
    public $timestamps = false;
    protected $table    = 'traveling_details';
    protected $fillable = ['country_code', 'recomended_vaccinations', 'vaccination_considered', 'malaria_affected', 'required_vaccination'];

    public function getRecomendedVaccinationsAttribute($value)
    {
        $record = [];
        if($value)
        {
            $value = json_decode($value, true);
        }
        return $value;
    }

    public function getVaccinationConsideredAttribute($value)
    {
        $record = [];
        if($value)
        {
           $value = json_decode($value);
        }
        return $value;
    }

    public function getRequiredVaccinationAttribute($value)
    {
        $record = [];
        if($value)
        {
            $value = json_decode($value);
        }
        return $value;
    }
}
