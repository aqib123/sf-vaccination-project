<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ClientAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            if(Auth::user()->type == 2 && Auth::user()->active == 1 && Auth::user()->user_type != 0){
                return $next($request);
            }
            Auth::logout();
            return redirect('login_account')->with('error','Permission Denied!!! You do not have access.');
        }
        return redirect('login_account')->with('error','Permission Denied!!! You do not have access.');
    }
}
