<?php

namespace App\Http\Controllers\WebControllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;
use App\Http\GeneralFunctions;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Crypt;
use App\MedicalQuestions;

class CorporateServiceController extends Controller
{
	private $table;
    private $tableName;
    use GeneralFunctions;

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     *
     * Get Corporate Service Page
     *
     */
    public function index()
    {   
        return view('website.layouts.pages.corporate_services');
    }

    /**
     *
     * Get On Site Vaccination Clinic
     *
     */
    public function onsiteVaccinationClinic()
    {
        return view('website.layouts.pages.onsite_vaccination_clinic');
    }
    
    /**
     *
     * Get Online testing service
     *
     */
    public function onsiteTestingService()
    {
        return view('website.layouts.pages.onsite_testing_service');
    }

    /**
     *
     * Get Online testing service
     *
     */
    public function employeeHealth()
    {
        return view('website.layouts.pages.employee_health');
    }

    /**
     *
     * Go to Travel Vaccination
     *
     */
    public function travelVaccination()
    {
        return view('website.layouts.pages.travel_vaccination');
    }
    
    /**
     *
     * Go to Anti Malarial
     *
     */
    public function antiMalarial()
    {
        return view('website.layouts.pages.anti_malarial');
    }
    
                    
}