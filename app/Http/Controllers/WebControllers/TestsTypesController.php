<?php

namespace App\Http\Controllers\WebControllers;

use Illuminate\Http\Request;
use Auth;
use App\SampleType;
use App\Http\Controllers\Controller;
use App\Http\GeneralFunctions;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Crypt;
use App\SpecialInstructions;
use App\Tests;
use App\TestDetail;

class TestsTypesController extends Controller
{
	private $table;
    private $tableName;
    use GeneralFunctions;

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->table = new SampleType;
        $this->tableName = 'sample_types';
    }

    /**
     *
     * Get Sample Type Record
     *
     */
    public function getSampleTypeRecords()
    {   
        $sampleType = $this->table->get()->toArray();
        return view('website.layouts.pages.sample_requirements', compact('sampleType'));
    }

    /**
     *
     * Get Special Instrauction Record
     *
     */
    public function getspecialInstrauctionsRecord()
    {
        $specialInstructionsRecord = SpecialInstructions::get()->toArray();
        return view('website.layouts.pages.special_instrcutions', compact('specialInstructionsRecord'));
    }

    /**
     *
     * Get Test A to Z Records
     *
     */
    public function getTestAtoZ($code)
    {
        $tests = Tests::where('code', $code)->get()->toArray();
        return view('website.layouts.pages.tests', compact('tests'));
    }
    
    /**
     *
     * Get Test Details
     *
     */
    public function getTestDetails($code, $testId)
    {
        $sampleTypes = ['badge-danger@F','badge-secondary@G', 'badge-success@H', 'badge-warning@B', 'bg-dark-blue@K', 'bg-magenda@A', 'bg-primary@C'];
        $detail = TestDetail::with('test')->where('test_id', $testId)->first();
        // check if sample Requirments has any test codes
        $detail->sample_requirements = GeneralFunctions::checkAndReplaceSampleCodes($detail->sample_requirements);
        if($detail->sample_type_guide)
        {
            $sampleTypeGuide = $detail->sample_type_guide;
            $sampleTypeGuide = json_decode($sampleTypeGuide, true);
            // dd($detail->sample_type_guide);
            if(count($sampleTypeGuide) > 0) {
                foreach ($sampleTypeGuide as $key => $value) {
                    if(in_array($value['code'], $sampleTypes))
                    {
                        $sampleCode = explode("@",$value['code']);
                        $sampleTypeGuide[$key]['code'] = '<span class="badge '.$sampleCode[0].'">'.$sampleCode[1].'</span>';
                    }
                }
            }
        }
        $detail->sample_type_guide = $sampleTypeGuide;
        return view('website.layouts.pages.test_detail', compact('detail'));
    }
            
}