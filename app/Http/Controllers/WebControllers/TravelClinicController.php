<?php

namespace App\Http\Controllers\WebControllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;
use App\Http\GeneralFunctions;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Crypt;

class TravelClinicController extends Controller
{
	private $table;
    private $tableName;
    use GeneralFunctions;

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     *
     * Get Travel Clinic Page
     *
     */
    public function index()
    {   
        $countriesList = GeneralFunctions::getCountries();
        return view('website.layouts.pages.travel_clinic', compact('countriesList'));
    }        
}