<?php

namespace App\Http\Controllers\WebControllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;
use App\Http\GeneralFunctions;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Crypt;
use App\MedicalQuestions;

class OccupationalHealthController extends Controller
{
	private $table;
    private $tableName;
    use GeneralFunctions;

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     *
     * Get Occupational Health Service Page
     *
     */
    public function index()
    {   
        return view('website.layouts.pages.ocupational_health_services');
    }

    /**
     *
     * Get Occupational Health Vaccination
     *
     */
    public function occupationalHealthVaccination()
    {
        return view('website.layouts.pages.ocupational_health_vaccination');
    }

    /**
     *
     * Get Occupational Blood Test
     *
     */
    public function occupationalHealthBloodTest()
    {
        return view('website.layouts.pages.ocupational_blood_test');
    }

    /**
     *
     * Get Medical
     *
     */
    public function occupationalMedical()
    {
        $medicalQuestions = MedicalQuestions::get()->toArray();
        return view('website.layouts.pages.medical', compact('medicalQuestions'));
    }

    /**
     *
     * Get Vitamin Defficiancy 
     *
     */
    public function vitaminDeficiencies()
    {
        return view('website.layouts.pages.vitamin_deficiencies');
    }

    /**
     *
     * Get Hyperhidrosis
     *
     */
    public function hyperhidrosis()
    {
        return view('website.layouts.pages.hyperhidrosis');
    }

    /**
     *
     * Get Chronic Migrain Treatment
     *
     */
    public function chronicMigrain()
    {
        return view('website.layouts.pages.chronic_migrain');
    }
    
                 
}