<?php

namespace App\Http\Controllers\AdminControllers;

use Illuminate\Http\Request;
use Auth;
use App\TutorialRequest;
use App\Http\Controllers\Controller;

class TutorialRequestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $result = TutorialRequest::orderBy('created_at','desc')->paginate(10);
        return view('admin.layouts.pages.tutorial_request', compact('result'));
    }
}
