<?php

namespace App\Http\Controllers\AdminControllers;

use Illuminate\Http\Request;
use Auth;
use App\SampleType;
use App\Http\Controllers\Controller;
use App\Http\GeneralFunctions;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Crypt;

class SampleTypesController extends Controller
{
	private $table;
    private $tableName;
    use GeneralFunctions;

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->table = new SampleType;
        $this->tableName = 'sample_types';
        $this->middleware('auth');
    }

    /**
     *
     * Sample Types Form
     *
     */
    public function index(Request $req)
    {
    	return view('admin.layouts.pages.sample_type.list');
    }

    /**
     *
     * Store Sample Types Record
     *
     */
    public function store(Request $req)
    {
        $this->validate($req, [
            'smaple_type' => 'required',
            'vacutainer' => 'required',
        ], [
            'smaple_type.required' => 'Name is required',
            'vacutainer.required' => 'vacutainer is required'
        ]);
        // Save Record
        $data = $req->only(['smaple_type', 'vacutainer', 'anticoagulant', 'capacity']);
        if($req->id)
        {
            $data = $req->only(['smaple_type', 'vacutainer', 'anticoagulant', 'capacity', 'id']);
            $record = $this->table->where('id', $req->id)->update(['smaple_type' => $data['smaple_type'], 'vacutainer' => $data['vacutainer'], 'anticoagulant' => $data['anticoagulant'], 'capacity' => $data['capacity']]);
            return back()->with('success', 'Sample Type Updated Successfully');
        }
        $record = $this->table->create($data);
        if($record) 
        {
            return back()->with('success', 'Sample Type Added Successfully');
        }
    }

    /**
     *
     * Sample Types Data table record
     *
     */
    public function dataTableRequest(Request $request)
    {
        try {
            $sampleType = $this->table->select(['id', 'smaple_type', 'vacutainer', 'anticoagulant', 'capacity']);//->orderBy('users.id', 'Asc');
            return DataTables::of($sampleType)
                ->addIndexColumn()
                ->addColumn('action', function ($sampleType) {
                    $aciton = '<div class="btn-group" role="group"><button id="btnGroupDrop1" type="button" class="btn btn-secondary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button><div class="dropdown-menu" aria-labelledby="btnGroupDrop1">';
                    $aciton .= '<a href="' . route("edit.sample_type.form", ["id" => \Illuminate\Support\Facades\Crypt::encryptString($sampleType->id)]) . '" class="dropdown-item"><i class="fa fa-edit"></i>Edit Sample Type</a></div></div>';
                    return $aciton;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        catch (\Throwable $exception)
        {
            return DataTables::of([])->make(true);
        }
    }

    /**
     *
     * Sample Type Form
     *
     */
    public function sampleTypeForm(Request $request)
    {
        return view('admin.layouts.pages.sample_type.form');
    }

    /**
     *
     * Edit Sample Type Form
     *
     */
    public function editSampleType($id)
    {   
        $sampleType = $this->table->where('id', Crypt::decryptString($id))->first();
        return view('admin.layouts.pages.sample_type.form', compact('sampleType'));
    }    
}