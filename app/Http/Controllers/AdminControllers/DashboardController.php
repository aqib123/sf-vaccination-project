<?php

namespace App\Http\Controllers\AdminControllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\TutorialRequest;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
	private $userTable;
	private $tutorialRequest;

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->userTable = new User;
        $this->tutorialRequest = new TutorialRequest;
        $this->middleware('auth');
    }

    /**
     *
     * Dashboard Stats
     *
     */
    public function index(Request $req)
    {
    	$totalUserRegistration = $this->userTable->where('user_type', '!=', 0)->count();
    	$totalTutorialRequest = $this->tutorialRequest->count();
    	return view('admin.layouts.pages.dashboard', compact('totalUserRegistration', 'totalTutorialRequest'));
    }
}