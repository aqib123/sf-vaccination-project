<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\GeneralFunctions;
use App\Setting\UserRole;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;
use App\Screens;
use App\Permission;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\Facades\DataTables;

class RolesController extends Controller
{
    use GeneralFunctions;
    public function rolesList()
    {
        return view('admin.layouts.pages.roles.roles_list');
    }

    public function roleForm(Request $request)
    {
        $data['roles_details'] = [];
        $data['role_info']     = [];
        $data['screens'] = Screens::all();
        $data['screens'] = $data['screens']->toArray();
        return view('admin.layouts.pages.roles.roles_form', $data);
    }

    public function editRole($id)
    {
        $getRolesData = Role::with('permissions')->where('id', Crypt::decryptString($id))->first();
        if (!is_null($getRolesData))
        {
            $data['role_info'] = $getRolesData->toArray();
            $data['roles_details'] = $this->assemble_permission_array($getRolesData->toArray());
            $data['screens'] = Screens::all();
            $data['screens'] = $data['screens']->toArray();
            return view('admin.layouts.pages.roles.roles_form', $data);
        }
        else
        {
            return back()->withErrors('No role is found');
        }
    }

    public function addRoleWithPermissions(Request $req)
    {
        $this->validate($req, [
            'name' => 'required|max:100'
        ], [
            'name.required' => 'Role name is required',
            'name.max' => 'Role name should be less then 100 characters'
        ]);
        try
        {
            if ($req && $req->id != '') {
                $requestId = Crypt::decryptString($req->id);
                if ($requestId == UserRole::SuperAdmin)
                {
                    return redirect()->route('role.list')->withErrors('You do not have permission to change it');
                }
                $rolesInstance = Role::find($requestId);
                if (in_array($requestId, [UserRole::Admin,UserRole::Manager]) != true)
                {
                    $rolesInstance->update(['name' => $req->name]);
                }

                $rolesInstance->permissions()->detach();

                if (isset($req->permissions)) {

                    foreach ($req->input('permissions') as $key => $value) {
                        $getScreenId = Screens::where('code', $key)->first();

                        foreach ($value as $innerKey => $valueKey) {

                            $getPermission = Permission::where('name', $innerKey)->first();
                            $rolesInstance->permissions()->attach([$getPermission->id => ['screen_id' => $getScreenId->id]]);
                        }
                    }
                }
                return redirect()->route('role.list')->with('status', 'Record has been updated successfully');
            }

            $checkRoleExistAlready = Role::/*where('created_by', Auth::user()->id)->*/where('name', $req->input('name'))->get();
            if (count($checkRoleExistAlready) > 0) {
                return back()->withErrors('Role Name Already Exist. Please Change it to new Name');
            }
            $owner = new Role();
            $owner->name = $req->input('name');
            $owner->created_by = Auth::user()->id;
            $owner->save();

            if (isset($req->permissions)) {
                foreach ($req->input('permissions') as $key => $value) {
                    $getScreenId = Screens::where('code', $key)->first();
                    foreach ($value as $innerKey => $valueKey) {
                        $getPermission = Permission::where('name', $innerKey)->first();
                        $owner->permissions()->attach([$getPermission->id => ['screen_id' => $getScreenId->id]]);
                    }
                }
            }

            return redirect()->route('role.list')->with('status', 'Record has been saved successfully');
        }
        catch (\Throwable $exception)
        {
            Log::error('PermissionError : '.$exception->getMessage());
            return back()->withErrors('An error occurred while adding permissions');
        }
    }

    public function assemble_permission_array(array $data)
    {
        $assembleArray = [];
        $permissions   = [1 => 'add', 2 => 'edit', 3 => 'delete', 4 => 'view'];
        foreach ($data['permissions'] as $key => $value) {
            $getScreenCode = Screens::where('id', $value['screen_id'])->first();
            $getPermissionCode = $permissions[$value['pivot']['permission_id']];
            $assembleArray[$getScreenCode->code][$getPermissionCode] = true;
        }
        return $assembleArray;
    }

    public function deleteRole($id)
    {
        try
        {
            $deleteRecord = Role::with('permissions')->find(Crypt::decryptString($id));
            if (!is_null($deleteRecord))
            {
                if ($deleteRecord->permissions->count() > 0)
                {
                    return redirect()->back()->withErrors( 'Role cannot be delete because some of its information is still being used');
                }
                else
                {
                    $deleteRecord->delete();
                    return back()->with('status', 'Record has been deleted successfully');
                }
            }
            else
            {
                return back()->withErrors('No record found');
            }
        }
        catch (\Exception $e)
        {
            return back()->withErrors( 'Record is referenced in other record');
        }
    }

    public function dataTableRequest(Request $request)
    {
        try
        {
            $roles = Role::with('user')->select(['id', 'name', 'created_by', 'created_at']);
            return DataTables::of($roles)
                ->addIndexColumn()
                ->addColumn('action', function ($roles){
                    if ($roles->id != UserRole::SuperAdmin)
                    {
                        $aciton = '<div class="btn-group" role="group"><button id="btnGroupDrop1" type="button" class="btn btn-secondary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button><div class="dropdown-menu" aria-labelledby="btnGroupDrop1">';
                        $aciton .= '<a href="'. route("edit.role", ["id" => \Illuminate\Support\Facades\Crypt::encryptString($roles->id)]).'" class="dropdown-item"><i class="fa fa-edit"></i>Edit</a>';
                        if (in_array ($roles->id, [2,3,4]) != true)
                        {
                            $aciton .= '<a href="'. route('delete.role', ['id' => \Illuminate\Support\Facades\Crypt::encryptString($roles->id)]) .'" class="dropdown-item delete_btn"><i class="fa fa-trash"></i>Delete</a></div></div>';
                        }
                    }
                    else
                    {
                        $aciton = '<lable class="badge badge-danger">Can not Change</lable>';
                    }

                    return $aciton;
                })
                ->make(true);
        }
        catch (\Exception $exception)
        {
            return DataTables::of([])->make(true);
        }
    }
}
