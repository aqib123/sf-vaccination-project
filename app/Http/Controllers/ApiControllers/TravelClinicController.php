<?php

namespace App\Http\Controllers\ApiControllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;
use App\Http\GeneralFunctions;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Crypt;
use App\TravellingDetails;

class TravelClinicController extends Controller
{
	private $table;
    private $tableName;
    use GeneralFunctions;

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->tableName = new TravellingDetails;
    }

    /**
     *
     * Get Vaccination Details upon Country
     *
     */
    public function getVaccinationDetails($countryCode)
    {   
        $record = $this->tableName->where('country_code', $countryCode)->first();
        if($record){
            return response()->json(['status' => true, 'data' => $record, 'country' => GeneralFunctions::getCountries($countryCode)]);
        }
        return response()->json(['status' => false, 'data' => []]);
    }        
}