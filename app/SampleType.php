<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SampleType extends Model
{
    public $timestamps = false;
    protected $table    = 'sample_types';
    protected $fillable = ['smaple_type', 'vacutainer', 'anticoagulant', 'capacity'];
}
