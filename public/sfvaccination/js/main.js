(function($) {

	"use strict";

	var fullHeight = function() {

		$('.js-fullheight').css('height', $(window).height());
		$(window).resize(function(){
			$('.js-fullheight').css('height', $(window).height());
		});

	};
	fullHeight();

	$('#sidebarCollapse').on('click', function () {
	    $('#sidebar').toggleClass('active');
	});

	jQuery('.dropdown_section .dropdown h2').click(function(){
		jQuery('.dropdown_content').not(jQuery(this).next('.dropdown_content')).slideUp();
		jQuery(this).toggleClass('on').next('.dropdown_content').slideToggle();
	});

})(jQuery);
