<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta name="x-apple-disable-message-reformatting">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="telephone=no" name="format-detection">
    <title></title>
    <!--[if (mso 16)]>
    <style type="text/css">
        a {text-decoration: none;}
    </style>
    <![endif]-->
    <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]-->
</head>

<body>
<div class="es-wrapper-color">
    <!--[if gte mso 9]>
    <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
        <v:fill type="tile" color="#ffffff"></v:fill>
    </v:background>
    <![endif]-->
    <table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
            <td class="esd-email-paddings" valign="top">
                <table class="es-content es-preheader esd-header-popover" align="center" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        <td class="es-adaptive esd-stripe" style="background-color: rgb(247, 247, 247);" esd-custom-block-id="8428" align="center" bgcolor="#f7f7f7">
                            {{--<table class="es-content-body" style="background-color: transparent;" align="center" width="600" cellspacing="0" cellpadding="0">
                                <tbody>
                                <tr>
                                    <td class="esd-structure es-p10" align="left">
                                        <!--[if mso]><table width="580"><tr><td width="280" valign="top"><![endif]-->
                                        <table class="es-left" align="left" cellspacing="0" cellpadding="0">
                                            <tbody>
                                            <tr>
                                                <td class="esd-container-frame" align="left" width="280">
                                                    <table width="100%" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                        <tr>
                                                            <td class="es-infoblock esd-block-text es-m-txt-c" align="left">
                                                                <p>Put your preheader text here</p>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <!--[if mso]></td><td width="20"></td><td width="280" valign="top"><![endif]-->
                                        <table class="es-right" align="right" cellspacing="0" cellpadding="0">
                                            <tbody>
                                            <tr>
                                                <td class="esd-container-frame" align="left" width="280">
                                                    <table width="100%" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                        <tr>
                                                            <td class="es-infoblock esd-block-text es-m-txt-c" align="right">
                                                                <p><a href="https://viewstripo.email/" target="_blank">View in browser</a></p>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <!--[if mso]></td></tr></table><![endif]-->
                                    </td>
                                </tr>
                                </tbody>
                            </table>--}}
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table class="es-header" align="center" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        <td class="es-adaptive esd-stripe" esd-custom-block-id="8429" align="center">
                            <table class="es-header-body" align="center" width="600" cellspacing="0" cellpadding="0">
                                <tbody>
                                <tr>
                                    <td class="esd-structure es-p25t es-p10b es-p20r es-p20l" align="left">
                                        <!--[if mso]><table width="560" cellpadding="0"
                                                            cellspacing="0"><tr><td width="205" valign="top"><![endif]-->
                                        <table class="es-left" align="left" cellspacing="0" cellpadding="0">
                                            <tbody>
                                            <tr>
                                                <td class="es-m-p0r es-m-p20b esd-container-frame" align="center" width="205" valign="top">
                                                    <table width="100%" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                        <tr>
                                                            <td class="esd-block-image es-m-p0l es-m-txt-c" align="right">
                                                                <a href="https://viewstripo.email/" target="_blank"><img src="https://tlr.stripocdn.email/content/guids/CABINET_944049925f70694686e3cfdb5a5862f4/images/69781523356282391.png" alt="Logo" style="display: block;" title="Logo" width="78"></a>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <!--[if mso]></td><td width="10"></td><td width="345" valign="top"><![endif]-->
                                        <table align="right" cellspacing="0" cellpadding="0">
                                            <tbody>
                                            <tr>
                                                <td class="esd-container-frame" align="left" width="345">
                                                    <table width="100%" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left" class="esd-block-text es-p15t es-p10l">
                                                                <h1 style="font-size: 18px; font-family: 'lucida sans unicode', 'lucida grande', sans-serif;">MTS (My Tutor Source)</h1>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <!--[if mso]></td></tr></table><![endif]-->
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table class="es-content" align="center" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        <td class="esd-stripe" align="center">
                            <table class="es-content-body" style="border-left:1px solid transparent;border-right:1px solid transparent;border-top:1px solid transparent;border-bottom:1px solid transparent;" align="center" width="600" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                                <tbody>
                                <tr>
                                    <td class="esd-structure es-p20t es-p40b es-p40r es-p40l" esd-custom-block-id="8537" align="left">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tbody>
                                            <tr>
                                                <td class="esd-container-frame" align="left" width="518">
                                                    <table width="100%" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                        <tr>
                                                            <td class="esd-block-text es-m-txt-c" align="center">
                                                                <h2 style="font-family: 'lucida sans unicode', 'lucida grande', sans-serif;">Hey {{ $userName }}</h2>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="esd-block-text es-m-txt-c es-p15t" align="center">
                                                                <p style="font-family: 'lucida sans unicode', 'lucida grande', sans-serif;">Your {{ env('APP_NAME') }} password is <strong>{{ $password }}</strong>. If you don’t know why you got this email, please tell us straight away so we can fix this for you.</p>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table class="es-content esd-footer-popover" align="center" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr> </tr>
                    <tr>
                        <td class="esd-stripe" esd-custom-block-id="8442" style="background-color: rgb(247, 247, 247);" align="center" bgcolor="#f7f7f7">
                            <table class="es-footer-body" align="center" width="600" cellspacing="0" cellpadding="0">
                                <tbody>
                                <tr>
                                    <td class="esd-structure es-p20t es-p20b es-p20r es-p20l" esd-general-paddings-checked="false" align="left">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tbody>
                                            <tr>
                                                <td class="esd-container-frame" align="center" width="560" valign="top">
                                                    <table width="100%" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                        <tr>
                                                            <td class="esd-block-text es-p5b" align="center">
                                                                <h3 style="line-height: 150%; font-family: 'lucida sans unicode', 'lucida grande', sans-serif;">Let's get social</h3>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" class="esd-block-social">
                                                                <table cellpadding="0" cellspacing="0" class="es-table-not-adapt es-social">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td align="center" valign="top" class="es-p10r">
                                                                            <a target="_blank" href> <img src="https://demo.stripocdn.email/content/assets/img/social-icons/circle-colored/twitter-circle-colored.png" alt="Tw" title="Twitter" width="32"> </a>
                                                                        </td>
                                                                        <td align="center" valign="top" class="es-p10r">
                                                                            <a target="_blank" href> <img src="https://demo.stripocdn.email/content/assets/img/social-icons/circle-colored/facebook-circle-colored.png" alt="Fb" title="Facebook" width="32"> </a>
                                                                        </td>
                                                                        <td align="center" valign="top" class="es-p10r">
                                                                            <a target="_blank" href> <img src="https://demo.stripocdn.email/content/assets/img/social-icons/circle-colored/youtube-circle-colored.png" alt="Yt" title="Youtube" width="32"> </a>
                                                                        </td>
                                                                        <td align="center" valign="top">
                                                                            <a target="_blank" href> <img src="https://demo.stripocdn.email/content/assets/img/social-icons/circle-colored/vk-circle-colored.png" alt="VK" title="Vkontakte" width="32"> </a>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="esd-block-text es-p10t es-p10b" align="center" esd-links-color="#3d85c6">
                                                                <p style="line-height: 150%; color: #333333; font-family: 'lucida sans unicode', 'lucida grande', sans-serif;">If you&nbsp;don't recognize this message, contact your system adminstator at <a target="_blank" href="http://www.google.com" style="line-height: 150%; color: #3d85c6; font-family: 'lucida sans unicode', 'lucida grande', sans-serif;">MTS</a></p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="esd-block-text es-p10t es-p10b" align="center">
                                                                <p style="font-family: 'lucida sans unicode', 'lucida grande', sans-serif;">My Tutor Source</p>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div style="position: absolute; left: -9999px; top: -9999px; margin: 0px;"></div>
</body>

</html>