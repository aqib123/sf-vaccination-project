@extends('website.layouts.base')

@section('styling')
<style>
    table {
        border: unset !important;
        padding: unset !important; 
        width: 100%;
    }
</style>
@endsection

@section('content')
<div class="container">
    @include('website.layouts.logo')
    <div class="tab-content padding-20 bg-custom-white bx-wrapper">
        <div class="section-header">
            <div class="section-heading">
                <h3 class="text-custom-black fw-700">Sample Requirements</h3>
                <div class="section-description">
                    <h5 class="text-custom-black fw-700">Vacutainer</h5>
                </div>
            </div>
        </div>
        <div class="tab-pane fade show active" id="start">
            <div class="tab-inner">
                <div class="row">
                    <div class="col-lg-12 table-responsive">
                        <table class="table borderless">
                            <thead class="thead-blue">
                                <tr>
                                    <th>Sample type</th>
                                    <th>Vacutainer</th>
                                    <th>Anticoagulant</th>
                                    <th>Capacity</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($sampleType as $key => $value)
                                @php
                                    $sampType = explode("@",$value['smaple_type']);
                                @endphp
                                <tr>
                                    <td><span class="badge {{$sampType[0]}}">{{$sampType[1]}}</span></td>
                                    <td>{{$value['vacutainer']}}</td>
                                    <td>{{$value['anticoagulant']}}</td>
                                    <td>{{$value['capacity']}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <p>* 10ml EDTA tubes are used for specific PCR assays</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-content padding-20 bg-custom-white bx-wrapper">
        <div class="section-header">
            <div class="section-heading">
                <h3 class="text-custom-black fw-700">Other Types</h3>
            </div>
        </div>
        <div class="tab-pane fade show active" id="start">
            <div class="tab-inner">
                <div class="row">
                    <div class="col-lg-12 table-responsive">
                        <table class="table borderless">
                            <thead class="thead-blue">
                                <tr>
                                    <th>Sample type</th>
                                    <th>Description</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($sampleType as $key => $value)
                                @php
                                    $sampType = explode("@",$value['smaple_type']);
                                @endphp
                                <tr>
                                    <td><span class="badge {{$sampType[0]}}">{{$sampType[1]}}</span></td>
                                    <td>{{$value['vacutainer']}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <p>* 10ml EDTA tubes are used for specific PCR assays</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="section-padding-footer">
    </section>
</div>
@endsection