@extends('website.layouts.base')
@section('content')
<input type="hidden" value="PK" id='hiddenchekuae'>	<div id="content" style="width: 100%;">
				<div id="post-2722" class="post-2722 page type-page status-publish hentry">
			<span class="entry-title" style="display: none;">Admission Counselling Services</span><span class="vcard" style="display: none;"><span class="fn"><a href="{{url('/')}}" title="Posts by admin" rel="author">admin</a></span></span><span class="updated" style="display:none;">2018-07-06T09:26:40+00:00</span>																		<div class="post-content">
				<div class="fusion-fullwidth fullwidth-box fusion-fullwidth-1  fusion-parallax-none nonhundred-percent-fullwidth remove-spacing" style="border-color:#eae9e9;border-bottom-width: 0px;border-top-width: 0px;border-bottom-style: solid;border-top-style: solid;padding-bottom:0px;padding-top:60px;padding-left:px;padding-right:px;"><style type="text/css" scoped="scoped">.fusion-fullwidth-1 {
                            padding-left: px !important;
                            padding-right: px !important;
                        }</style><div class="fusion-row"><div class="fusion-title title fusion-title-size-three custom_heading"><h3 class="title-heading-left">Admission Counselling Services</h3><div class="title-sep-container"><div class="title-sep sep-single"></div></div></div><div class="fusion-sep-clear"></div><div class="fusion-separator sep-single" style="border-color:#e0dede;border-top-width:1px;margin-left: auto;margin-right: auto;margin-top:px;margin-bottom:30px;width:100%;max-width:55%;"></div><p style="font-weight: 400;">Apart from other online educational services, MTS also offers Admission Counselling to students around the globe. MTS provides customized help to candidates seeking admission in some of the best universities in the world.</p>
<p style="font-weight: 400;">Students are given the opportunity to work closely with our Admission consultants who have themselves secured Admissions in Top universities in the UK, USA, Australia and Canada etc. Our consultants provide customized hourly help or end to end admission counselling (from College Selection to Admission Application submission). Our Admission Counselling rates for hourly help vary from $25 to $120/hour, whereas for end to end counselling services MTS charges from $1500 to $5000 per candidate. These rates are based on the counselor’s overall profiles and number of years of experience. Customers have the flexibility to choose their own counselors from our pool of highly competent and experienced professionals.</p>
</div></div>
							</div>
																	</div>
					</div>
@endsection