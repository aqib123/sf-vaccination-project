@extends('website.layouts.base')

@section('styling')
<style>
    table {
        border: unset !important;
        padding: unset !important; 
        width: 100%;
    }
    tr td {
        color: #155fa1;
    }
</style>
@endsection

@section('content')
<div class="container">
    @include('website.layouts.logo')
    <div class="tab-content padding-20 bg-custom-white bx-wrapper">
        <div class="section-header">
            <div class="section-heading">
                <h3 class="text-custom-black fw-700">Medical</h3>
            </div>
        </div>
        <div class="tab-pane fade show active" id="start">
        <div class="row">
            <div class="col_1 icon">
                <img width="250" height="250" src="https://secureservercdn.net/160.153.138.163/pnu.4b0.myftpupload.com/wp-content/uploads/2021/02/SF-Vaccinations-Monogram-Only.png?time=1634316888" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="SF Vaccinations" loading="lazy" srcset="https://secureservercdn.net/160.153.138.163/pnu.4b0.myftpupload.com/wp-content/uploads/2021/02/SF-Vaccinations-Monogram-Only.png 250w, https://secureservercdn.net/160.153.138.163/pnu.4b0.myftpupload.com/wp-content/uploads/2021/02/SF-Vaccinations-Monogram-Only-150x150.png 150w, https://secureservercdn.net/160.153.138.163/pnu.4b0.myftpupload.com/wp-content/uploads/2021/02/SF-Vaccinations-Monogram-Only-100x100.png 100w" sizes="(max-width: 250px) 100vw, 250px">     
            </div>
            <div class="col_10">
                <h3>We offer a range of occupational health services including medicals, screening and blood tests and vaccinations.</h3>
                <p>For information on occupational health vaccinations and blood tests, please visit our <a href="https://sfvaccinations.co.uk/occupational-health-vaccinations/">vaccination</a> and <a href="https://sfvaccinations.co.uk/blood-tests/">blood test</a> page. <span style="display: inline !important; float: none; background-color: transparent; color: #333333; cursor: text; font-family: Georgia,'Times New Roman','Bitstream Charter',Times,serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;">Please find below a list of our medical services (N.B. the list is not exhaustive):-</span></p>
                <ul>
                <li>Offshore Medicals – £165</li>
                <li>ENG1 and ML5 Medicals – £115</li>
                <li>Wind Turbine (RUK) Medicals ) including Chester Step Test and Certificate – £200</li>
                <li>Visa Medical Checks – £115</li>
                <li>DVLA Medical – £115</li>
                <li>Drug and Alcohol Testing – from £70</li>
                <li>ECG – £70</li>
                <li>Chester Step Test – £70</li>
                </ul>
                <p>To book an appointment using our online system, please follow the link below:-</p>
                <h4><a href="https://ins-health-care.uk1.cliniko.com/bookings">BOOK APPOINTMENT</a></h4>
                <p>&nbsp;</p>
                <p>Alternatively, you can contact our medical services provider on:-</p>
                <p><span class="fa fa-envelope mr-3"></span> <a href="mailto:medical@inshealth.co.uk">medical@inshealth.co.uk</a></p>
                <p><span class="fa fa-mobile mr-3"></span> <a href="tel:07447618490">07447618490</a></p>
                <p><span class="fa fa-phone mr-3"></span> <a href="tel:01238730299">0123 8730299</a></p>
            </div>
            <div class="col_4 last sidebar"></div>
            <div style="clear:both;"></div>
        </div>
            <!-- <div class="tab-inner">
                <div class="row">
                    <div class="col-lg-12 table-responsive">
                        <table class="table borderless">
                            <thead class="thead-blue">
                                <tr>
                                    <th>Questions</th>
                                    <th>Answers</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($medicalQuestions as $key => $value)
                                <tr>
                                    <td><b>{{$value['questions']}}</b></td>
                                    <td>{{$value['answers']}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
    <section class="section-padding-30">
    </section>
</div>
@endsection