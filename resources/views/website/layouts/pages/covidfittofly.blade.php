@extends('website.layouts.base')
@section('content')
<div class="container">
    @include('website.layouts.logo')
    <div class="bg-image p-5 text-center shadow-1-strong rounded mb-5 text-white" style="background-image: url('https://mdbcdn.b-cdn.net/img/new/slides/003.jpg'); background-size:cover; width:100%;">
        <h1 class="mb-3 h2" style="color: #ffffff;">Covid19 Testing for Travellers Leaving the UK</h1>
    </div>
    <!-- Section One -->
    <div class="uk-panel uk-panel-box uk-width-medium-4-6 uk-container-center">
        <h3>
            <span style="color: #e65774;"><strong>Get tested in our London Clinic for just £175 </strong></span>
        </h3>
        <p>If possible, we recommend attending our Central London – Moorgate clinic. The samples taken here are taken to our labs by couriers and will begin processing next morning.</p>
        <p>Opening hours at our Moorgate clinic are Mon-Fri 9:00 – 17:00.</p>
        <p>For in-clinic appointments, book online at either of our Central London clinics. The cost per test is £175. A £25 booking deposit secures your appointment.</p>
        <p>Book at&nbsp;<strong><a href="https://www.citydoc.org.uk/moorgate-travel-health/">Citydoc Moorgate</a></strong></p>
        <p>Please order home testing kits if you are displaying Covid-19 symptoms or have been in contact with someone with symptoms as you will be denied access to the clinic.</p>
        <p style="text-align: center;"><strong>To make a booking please book an appointment online or contact our booking line.</strong></p>
        <p><a class="uk-button uk-button-large uk-width-medium-3-6 uk-align-center uk-container-center pink-button" href="{{url('covid-19-london-clinic-testing') }}">BOOK AN IN-CLINIC APPOINTMENT ONLINE</a></p>
    </div>
    <!-- Section Two -->
</div>
@endsection