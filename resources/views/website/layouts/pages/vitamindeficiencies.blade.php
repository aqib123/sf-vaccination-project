@extends('website.layouts.base')
@section('styling')
<style type="text/css">
    .row {
        margin-left: unset !important;
    }
    h2, .h2 {
        font-size: 1.2rem;
    }
</style>
@endsection
@section('content')
<div class="container">
    @include('website.layouts.logo')
    <section class="section-padding div_with_white_back">
        <div class="section-header">
            <div class="section-heading">
                <h3 class="text-custom-black fw-700">Chronic Migraines Treatment</h3>
            </div>
        </div>
        <div class="row">
            <div class="col_12">
                 <h1>Private Vitamin Blood Tests</h1>
<p>We offer a range of private blood tests to help determine a possible deficiency within certain vitamins e.g. vitamin B12, vitamin D, etc. The tests can also be used to determine the effectiveness of treatment. We have listed some of the common tests in the table below but further tests are available on request.</p>
<p>A consultation is required with a healthcare professional before any blood test is done. We may need to consult with your GP regarding any blood tests previously completed. Some patients will be referred back to their GP for treatment if this is available on the NHS.</p>
<h2><strong>Private Vitamin Injections</strong></h2>
<p>At our private clinic in Woodhouse Medical Practice, we provide a range of vitamin injections depending on your medical requirement. These include vitamin B12, vitamin D, and a mixture of vitamins B and C. A consultation is completed with our healthcare professional and a blood test may need to be completed before the administration of any injections to ensure this is medically suitable for you.</p>

                                <div class="dropdown_section">
                        <h2>
                            Vitamin D (colecalciferol)                      </h2>

                                                        <div class="dropdown">
                                    <h2>
                                        Role of vitamin D in the body                                   </h2>
                                    <div class="dropdown_content">
                                        <p><span style="display: inline !important; float: none; background-color: transparent; color: #333333; cursor: text; font-family: Georgia,'Times New Roman','Bitstream Charter',Times,serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;">Vitamin D helps the body absorb calcium and phosphate, which is needed for healthy bones and muscles. It has an important role in maintaining a healthy immune system. There is some evidence to suggest that it can help prevent some diseases such as cancer, diabetes and heart disease.</span></p>
                                    </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        Symptoms of vitamin D deficiency                                    </h2>
                                    <div class="dropdown_content">
                                        <ul>
<li>B<span style="display: inline !important; float: none; background-color: transparent; color: #333333; cursor: text; font-family: Georgia,'Times New Roman','Bitstream Charter',Times,serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;">one pain </span></li>
<li>L<span style="display: inline !important; float: none; background-color: transparent; color: #333333; cursor: text; font-family: Georgia,'Times New Roman','Bitstream Charter',Times,serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;">ower back pain </span></li>
<li>M<span style="display: inline !important; float: none; background-color: transparent; color: #333333; cursor: text; font-family: Georgia,'Times New Roman','Bitstream Charter',Times,serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;">uscle pain </span></li>
<li>T<span style="display: inline !important; float: none; background-color: transparent; color: #333333; cursor: text; font-family: Georgia,'Times New Roman','Bitstream Charter',Times,serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;">iredness </span></li>
<li>L<span style="display: inline !important; float: none; background-color: transparent; color: #333333; cursor: text; font-family: Georgia,'Times New Roman','Bitstream Charter',Times,serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;">ack of energy </span></li>
<li>F<span style="display: inline !important; float: none; background-color: transparent; color: #333333; cursor: text; font-family: Georgia,'Times New Roman','Bitstream Charter',Times,serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;">requent infections </span></li>
<li>H<span style="display: inline !important; float: none; background-color: transparent; color: #333333; cursor: text; font-family: Georgia,'Times New Roman','Bitstream Charter',Times,serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;">air loss&nbsp;</span></li>
</ul>
                                    </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        People at higher risk of developing vitamin D deficiency                                    </h2>
                                    <div class="dropdown_content">
                                        <p>Vitamin D deficiency is common in the UK and certain groups have a higher risk than others. These are as follows:-</p>
<ul>
<li>People who have little sun exposure e.g. housebound, those who cover up in the sun, etc.</li>
<li>People with darker skin</li>
<li>Elderly people</li>
<li>Pregnant and breast-feeding women</li>
<li>People with a BMI&gt;30</li>
<li>Strict sunscreen use when outdoors</li>
<li>People who may have fat malabsorption e.g. Crohns’ disease, coeliac disease and some liver and kidney diseases</li>
<li>People taking certain medicines e.g. anti-epileptics, antiretrovirals, glucocorticoids</li>
<li>Vegans</li>
</ul>
                                    </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        Vitamin D blood test                                    </h2>
                                    <div class="dropdown_content">
                                        <p><strong>Blood test to diagnose vitamin D deficiency:&nbsp;</strong></p>
<ul>
<li>Vitamin D levels</li>
<li>Bone profile [calcium, phosphate, alkaline phosphatase, albumin]</li>
<li>Urea and electrolytes</li>
</ul>
<p><strong>Cost: £130</strong></p>
<p>If a blood test to only determine vitamin D levels are required, then this is <strong>£60</strong></p>
<p><strong>Pre and post-treatment blood tests</strong></p>
<ul>
<li>Vitamin D</li>
<li>Calcium</li>
<li>Urea and electrolytes</li>
<li>ALP</li>
</ul>
<p><strong>Cost: £100</strong></p>
<p><strong>
                </strong></p><div class="gf_browser_chrome gform_wrapper" id="gform_wrapper_1"><form method="post" enctype="multipart/form-data" id="gform_1" action="/vitamin-deficiencies/"><strong>
                        <div class="gform_heading">
                            <h3 class="gform_title">For more information or to book an appointment</h3>
                            <span class="gform_description"></span>
                        </div>
                        <div class="gform_body"><ul id="gform_fields_1" class="gform_fields top_label form_sublabel_below description_below"><li id="field_1_5" class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_1_5">Name<span class="gfield_required">*</span></label><div class="ginput_container ginput_container_text"><input name="input_5" id="input_1_5" type="text" value="" class="large" placeholder="Name" aria-required="true" aria-invalid="false"></div></li><li id="field_1_2" class="gfield field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_1_2">Email</label><div class="ginput_container ginput_container_email">
                            <input name="input_2" id="input_1_2" type="text" value="" class="large" placeholder="Email" aria-invalid="false">
                        </div></li><li id="field_1_3" class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_1_3">Phone<span class="gfield_required">*</span></label><div class="ginput_container ginput_container_phone"><input name="input_3" id="input_1_3" type="text" value="" class="large" placeholder="Phone" aria-required="true" aria-invalid="false"></div></li><li id="field_1_4" class="gfield field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_1_4">Message</label><div class="ginput_container ginput_container_textarea"><textarea name="input_4" id="input_1_4" class="textarea large" placeholder="Message" aria-invalid="false" rows="10" cols="50"></textarea></div></li>
                            </ul></div>
        <div class="gform_footer top_label"> <input type="submit" id="gform_submit_button_1" class="gform_button button" value="Submit" onclick="if(window[&quot;gf_submitting_1&quot;]){return false;}  window[&quot;gf_submitting_1&quot;]=true;  " onkeypress="if( event.keyCode == 13 ){ if(window[&quot;gf_submitting_1&quot;]){return false;} window[&quot;gf_submitting_1&quot;]=true;  jQuery(&quot;#gform_1&quot;).trigger(&quot;submit&quot;,[true]); }"> 
            <input type="hidden" class="gform_hidden" name="is_submit_1" value="1">
            <input type="hidden" class="gform_hidden" name="gform_submit" value="1">
            
            <input type="hidden" class="gform_hidden" name="gform_unique_id" value="">
            <input type="hidden" class="gform_hidden" name="state_1" value="WyJbXSIsIjllNGI4ZWNhOWE5OGNjNDY1NDIwYzNlOTU4NzhkODYyIl0=">
            <input type="hidden" class="gform_hidden" name="gform_target_page_number_1" id="gform_target_page_number_1" value="0">
            <input type="hidden" class="gform_hidden" name="gform_source_page_number_1" id="gform_source_page_number_1" value="1">
            <input type="hidden" name="gform_field_values" value="">
            
        </div>
                        
                        </strong></form></div><script type="text/javascript"> jQuery(document).bind('gform_post_render', function(event, formId, currentPage){if(formId == 1) {if(typeof Placeholders != 'undefined'){
                        Placeholders.enable();
                    }} } );jQuery(document).bind('gform_post_conditional_logic', function(event, formId, fields, isInit){} );</script><script type="text/javascript"> jQuery(document).ready(function(){jQuery(document).trigger('gform_post_render', [1, 1]) } ); </script><p></p>
                                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>                                                                         </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        Vitamin D treatment                                 </h2>
                                    <div class="dropdown_content">
                                        <p>The best way of increasing vitamin D levels is by increasing ultraviolet B sunlight exposure of at least the face and arms for 20 minutes, 2-3 times per week. However, people with melanomas should seek specialist advice.</p>
<p>Increasing vitamin D intake through the diet can also help. Foods that are rich in vitamin D include fortified cereals and fat spreads, red meat, liver, egg yolks and oily fish.</p>
<p>In some patients, sun exposure and dietary intake have little effect on vitamin D levels, so oral vitamin D supplements are recommended. The dose and frequency depends on the level of deficiency, which is established through a blood test.</p>
<p>If oral supplements fail to improve vitamin D levels, we then recommend having a course of vitamin D injections.</p>
<p><span style="display: inline !important; float: none; background-color: transparent; color: #333333; cursor: text; font-family: Georgia,'Times New Roman','Bitstream Charter',Times,serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;">Vitamin D toxicity can occur in high doses. High doses of vitamin D over prolonged periods of time can lead to higher calcium levels, which can then weaken the bones and damage the heart and kidneys. Therefore, it is essential to have blood tests completed before and after treatment. This is to ensure the safety and effectiveness of treatment.</span></p>
                                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>                                                                         </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        Vitamin D injections                                    </h2>
                                    <div class="dropdown_content">
                                        <p>There are two types of vitamin D injections:-</p>
<p><strong>1.Ergocalciferol (vitamin D2)</strong></p>
<p><b>Cost: </b>£125 per dose</p>
<p><strong>Dose: </strong>300,000 units every 3 months depending on response</p>
<p><strong>Patient information leaflet:</strong></p>
<p><a href="https://www.medicines.org.uk/emc/files/pil.9133.pdf">https://www.medicines.org.uk/emc/files/pil.9133.pdf</a></p>
<p><strong>2.Colecalciferol (vitamin D3)</strong></p>
<p>This is not licensed in the UK and is manufactured abroad. It is given if a patient have previously failed to respond to ergocalciferol injections.</p>
<p><b>Cost: </b>£200 per dose</p>
<p><strong>Dose: </strong>300,000 units every 3 months depending on response</p>

                <div class="gf_browser_chrome gform_wrapper" id="gform_wrapper_1"><form method="post" enctype="multipart/form-data" id="gform_1" action="/vitamin-deficiencies/">
                        <div class="gform_heading">
                            <h3 class="gform_title">For more information or to book an appointment</h3>
                            <span class="gform_description"></span>
                        </div>
                        <div class="gform_body"><ul id="gform_fields_1" class="gform_fields top_label form_sublabel_below description_below"><li id="field_1_5" class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_1_5">Name<span class="gfield_required">*</span></label><div class="ginput_container ginput_container_text"><input name="input_5" id="input_1_5" type="text" value="" class="large" placeholder="Name" aria-required="true" aria-invalid="false"></div></li><li id="field_1_2" class="gfield field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_1_2">Email</label><div class="ginput_container ginput_container_email">
                            <input name="input_2" id="input_1_2" type="text" value="" class="large" placeholder="Email" aria-invalid="false">
                        </div></li><li id="field_1_3" class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_1_3">Phone<span class="gfield_required">*</span></label><div class="ginput_container ginput_container_phone"><input name="input_3" id="input_1_3" type="text" value="" class="large" placeholder="Phone" aria-required="true" aria-invalid="false"></div></li><li id="field_1_4" class="gfield field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_1_4">Message</label><div class="ginput_container ginput_container_textarea"><textarea name="input_4" id="input_1_4" class="textarea large" placeholder="Message" aria-invalid="false" rows="10" cols="50"></textarea></div></li>
                            </ul></div>
        <div class="gform_footer top_label"> <input type="submit" id="gform_submit_button_1" class="gform_button button" value="Submit" onclick="if(window[&quot;gf_submitting_1&quot;]){return false;}  window[&quot;gf_submitting_1&quot;]=true;  " onkeypress="if( event.keyCode == 13 ){ if(window[&quot;gf_submitting_1&quot;]){return false;} window[&quot;gf_submitting_1&quot;]=true;  jQuery(&quot;#gform_1&quot;).trigger(&quot;submit&quot;,[true]); }"> 
            <input type="hidden" class="gform_hidden" name="is_submit_1" value="1">
            <input type="hidden" class="gform_hidden" name="gform_submit" value="1">
            
            <input type="hidden" class="gform_hidden" name="gform_unique_id" value="">
            <input type="hidden" class="gform_hidden" name="state_1" value="WyJbXSIsIjllNGI4ZWNhOWE5OGNjNDY1NDIwYzNlOTU4NzhkODYyIl0=">
            <input type="hidden" class="gform_hidden" name="gform_target_page_number_1" id="gform_target_page_number_1" value="0">
            <input type="hidden" class="gform_hidden" name="gform_source_page_number_1" id="gform_source_page_number_1" value="1">
            <input type="hidden" name="gform_field_values" value="">
            
        </div>
                        </form>
                        </div><script type="text/javascript"> jQuery(document).bind('gform_post_render', function(event, formId, currentPage){if(formId == 1) {if(typeof Placeholders != 'undefined'){
                        Placeholders.enable();
                    }} } );jQuery(document).bind('gform_post_conditional_logic', function(event, formId, fields, isInit){} );</script><script type="text/javascript"> jQuery(document).ready(function(){jQuery(document).trigger('gform_post_render', [1, 1]) } ); </script>
                                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>                                                                         </div>
                                </div>
                                                </div>
                                    <div class="dropdown_section">
                        <h2>
                            Vitamin B12 (hydroxocobalamin)                      </h2>

                                                        <div class="dropdown">
                                    <h2>
                                        Role of vitamin B12 in the body                                     </h2>
                                    <div class="dropdown_content">
                                        <p dir="ltr">Vitamin B12 is required for physical growth and red blood cell production. It is also required for the absorption of folic acid and carbohydrates from food and for keeping the nerve system healthy. It has an important role in maintaining a healthy immune system.</p>
                                    </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        Symptoms of B12 deficiency                                  </h2>
                                    <div class="dropdown_content">
                                        <p><span style="display: inline !important; float: none; background-color: transparent; color: #333333; cursor: text; font-family: Georgia,'Times New Roman','Bitstream Charter',Times,serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;">The most predominant feature of B12 deficiency is:- </span></p>
<ul>
<li>t<span style="display: inline !important; float: none; background-color: transparent; color: #333333; cursor: text; font-family: Georgia,'Times New Roman','Bitstream Charter',Times,serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;">iredness and lack of energy</span><span style="display: inline !important; float: none; background-color: transparent; color: #333333; cursor: text; font-family: Georgia,'Times New Roman','Bitstream Charter',Times,serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;">&nbsp;</span></li>
</ul>
<p><span style="display: inline !important; float: none; background-color: transparent; color: #333333; cursor: text; font-family: Georgia,'Times New Roman','Bitstream Charter',Times,serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;">Other symptoms can include:- </span></p>
<ul>
<li><span style="display: inline !important; float: none; background-color: transparent; color: #333333; cursor: text; font-family: Georgia,'Times New Roman','Bitstream Charter',Times,serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;">sore mouth and tongue</span></li>
<li><span style="display: inline !important; float: none; background-color: transparent; color: #333333; cursor: text; font-family: Georgia,'Times New Roman','Bitstream Charter',Times,serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;">numbness and tingling in the arms and legs </span></li>
<li><span style="display: inline !important; float: none; background-color: transparent; color: #333333; cursor: text; font-family: Georgia,'Times New Roman','Bitstream Charter',Times,serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;">memory loss </span></li>
<li><span style="display: inline !important; float: none; background-color: transparent; color: #333333; cursor: text; font-family: Georgia,'Times New Roman','Bitstream Charter',Times,serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;">mood swings</span></li>
<li><span style="display: inline !important; float: none; background-color: transparent; color: #333333; cursor: text; font-family: Georgia,'Times New Roman','Bitstream Charter',Times,serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;">depression&nbsp; </span></li>
</ul>
                                    </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        People at higher risk of developing vitamin B12 deficiency                                  </h2>
                                    <div class="dropdown_content">
                                        <p dir="ltr">Vitamin B12 deficiency is usually as a result of poor absorption of vitamin B12 or poor dietary intake. People that are most affected include:-</p>
<div dir="ltr">
<ul>
<li>Vegans</li>
<li>People over the age of 50</li>
<li>Patients with thyroid disorders</li>
<li>Patients with autoimmune disorders such as Coeliac disease or Crohns disease</li>
<li>Patients taking certain medication such as metformin, methotrexate.</li>
</ul>
</div>
                                    </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        Vitamin B12 blood tests                                 </h2>
                                    <div class="dropdown_content">
                                        <p><strong>Total B12&nbsp;</strong></p>
<p><strong>Cost:</strong> £50</p>
<p><strong>Active B12</strong></p>
<p><strong>Cost:</strong> £75</p>
<p><span style="display: inline !important; float: none; background-color: transparent; color: #333333; cursor: text; font-family: Georgia,'Times New Roman','Bitstream Charter',Times,serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;">The active B12 test is more sensitive when determining B12 deficiency as it measures the amount of B12 being transported to tissues.</span></p>
<p><strong>Intrinsic Factor Antibody</strong></p>
<p><strong>Cost: </strong>£90</p>
<p>This test will help determine if there is an issue with the absorption of B12 and therefore will determine if the anaemia is a long-term problem.</p>
<p><strong>Methylmalonic Acid Test</strong></p>
<p><strong>Cost:</strong> £250</p>
<p>This test is used when B12 levels are within the normal range but symptoms of deficiency are still present.</p>
<p>It is worth noting that as the body only requires a small amount of the vitamin daily,a deficiency may take months or years to develop. Furthermore, the total amount of Vitamin B12 may appear within normal range but the active B12 taken up by the cells maybe lower.</p>
                                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>                                                                         </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        Vitamin B12 (hydroxocobalamin) injection                                    </h2>
                                    <div class="dropdown_content">
                                        <p dir="ltr">Vitamin B12 injections can be used to help prevent and treat anaemia due to vitamin B12 deficiency. As vitamin B12 is water-soluble, the body will excrete any amount not required by the body.</p>
<p><strong>Dose: </strong>maintenance dose – 1mg every 2-3months</p>
<p><strong>Cost: </strong>£50 per dose</p>
<p><strong>Patient information leaflet:</strong></p>
<p><a href="http://www.medicines.org.uk/emc/PIL.31591.latest.pdf">www.medicines.org.uk/emc/PIL.31591.latest.pdf</a></p>

                <div class="gf_browser_chrome gform_wrapper" id="gform_wrapper_1"><form method="post" enctype="multipart/form-data" id="gform_1" action="/vitamin-deficiencies/">
                        <div class="gform_heading">
                            <h3 class="gform_title">For more information or to book an appointment</h3>
                            <span class="gform_description"></span>
                        </div>
                        <div class="gform_body"><ul id="gform_fields_1" class="gform_fields top_label form_sublabel_below description_below"><li id="field_1_5" class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_1_5">Name<span class="gfield_required">*</span></label><div class="ginput_container ginput_container_text"><input name="input_5" id="input_1_5" type="text" value="" class="large" placeholder="Name" aria-required="true" aria-invalid="false"></div></li><li id="field_1_2" class="gfield field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_1_2">Email</label><div class="ginput_container ginput_container_email">
                            <input name="input_2" id="input_1_2" type="text" value="" class="large" placeholder="Email" aria-invalid="false">
                        </div></li><li id="field_1_3" class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_1_3">Phone<span class="gfield_required">*</span></label><div class="ginput_container ginput_container_phone"><input name="input_3" id="input_1_3" type="text" value="" class="large" placeholder="Phone" aria-required="true" aria-invalid="false"></div></li><li id="field_1_4" class="gfield field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_1_4">Message</label><div class="ginput_container ginput_container_textarea"><textarea name="input_4" id="input_1_4" class="textarea large" placeholder="Message" aria-invalid="false" rows="10" cols="50"></textarea></div></li>
                            </ul></div>
        <div class="gform_footer top_label"> <input type="submit" id="gform_submit_button_1" class="gform_button button" value="Submit" onclick="if(window[&quot;gf_submitting_1&quot;]){return false;}  window[&quot;gf_submitting_1&quot;]=true;  " onkeypress="if( event.keyCode == 13 ){ if(window[&quot;gf_submitting_1&quot;]){return false;} window[&quot;gf_submitting_1&quot;]=true;  jQuery(&quot;#gform_1&quot;).trigger(&quot;submit&quot;,[true]); }"> 
            <input type="hidden" class="gform_hidden" name="is_submit_1" value="1">
            <input type="hidden" class="gform_hidden" name="gform_submit" value="1">
            
            <input type="hidden" class="gform_hidden" name="gform_unique_id" value="">
            <input type="hidden" class="gform_hidden" name="state_1" value="WyJbXSIsIjllNGI4ZWNhOWE5OGNjNDY1NDIwYzNlOTU4NzhkODYyIl0=">
            <input type="hidden" class="gform_hidden" name="gform_target_page_number_1" id="gform_target_page_number_1" value="0">
            <input type="hidden" class="gform_hidden" name="gform_source_page_number_1" id="gform_source_page_number_1" value="1">
            <input type="hidden" name="gform_field_values" value="">
            
        </div>
                        </form>
                        </div><script type="text/javascript"> jQuery(document).bind('gform_post_render', function(event, formId, currentPage){if(formId == 1) {if(typeof Placeholders != 'undefined'){
                        Placeholders.enable();
                    }} } );jQuery(document).bind('gform_post_conditional_logic', function(event, formId, fields, isInit){} );</script><script type="text/javascript"> jQuery(document).ready(function(){jQuery(document).trigger('gform_post_render', [1, 1]) } ); </script>
                                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>                                                                         </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        Vitamin B12 injections vs oral vitamin B12 tablets                                  </h2>
                                    <div class="dropdown_content">
                                        <p>Vitamin B12 comes in different forms such as an injectable solution and oral tablets. It can also be part of a multivitamin tablet.</p>
<p>It is known that, only a small proportion of the oral vitamin B12 gets absorbed, that is, even if there are no abnormalities of the gut affecting absorption. The vitamin B12 injection by passes the absorption in the stomach. This means there will be a higher concentration in the body and it will have a quicker effect than tablets. Therefore, symptoms tend to improve much sooner with vitamin B12 injections.</p>
                                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>                                                                         </div>
                                </div>
                                                </div>
                                    <div class="dropdown_section">
                        <h2>
                              Vitamins B and C                      </h2>

                                                        <div class="dropdown">
                                    <h2>
                                        Role of vitamins B and C in the body                                    </h2>
                                    <div class="dropdown_content">
                                        <p dir="ltr">The B group of vitamins are involved in the development and maintenance of the nervous system (brain and nerves) and the formation of blood cells.&nbsp;They help convert food into energy, as well as acting as an antioxidant. Vitamin B6 has an important role in maintaining a healthy immune system.</p>
<p dir="ltr">Vitamin C is an antioxidant and is responsible for maintaining a healthy cell structure and producing an important protein, known as collagen, which is found in the skin, bone, cartilage and tendons. Vitamin C also has an important role in maintaining a healthy immune system.</p>
                                    </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        People at higher risk of developing vitamin B and C deficiency                                  </h2>
                                    <div class="dropdown_content">
                                        <p dir="ltr">Vitamins B and C are only obtained from the diet. Vitamin B and C deficiencies can arise in the following circumstances:-</p>
<ul>
<li dir="ltr">from poor diet, particularly in alcoholism or psychiatric states</li>
<li dir="ltr">following surgery</li>
<li dir="ltr">following infections</li>
<li dir="ltr">in conditions where the body cannot effectively absorb nutrients from the gut (malabsorption)</li>
<li dir="ltr">with regular kidney haemodialysis, which removes these vitamins from the blood</li>
</ul>
                                    </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        Vitamin B and C (Pabrinex) injection                                    </h2>
                                    <div class="dropdown_content">
                                        <p dir="ltr">Pabrinex injection has traditionally been used to treat patients who require rapid replacement of the water-soluble B and C vitamins. This injection contains water soluble vitamin C and&nbsp;vitamin&nbsp;(B1 (thiamine), B2 (riboflavin), B3 (nicotinamide) and B6 (pyridoxine). As the vitamins are water-soluble, the body will excrete any vitamins not required by the body.</p>
<p><strong>Dose: </strong>This would depend on the level of deficiency</p>
<p><strong>Cost: </strong>£70 per dose</p>
<p><strong>Patient information leaflet:</strong></p>
<p><a href="https://www.medicines.org.uk/emc/files/pil.1426.pdf">https://www.medicines.org.uk/emc/files/pil.1426.pdf</a></p>
                                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>                                                                         </div>
                                </div>
                                                </div>
                                    <div class="dropdown_section">
                        <h2>
                            Other vitamin blood tests                       </h2>

                                                        <div class="dropdown">
                                    <h2>
                                        Folate blood test                                   </h2>
                                    <div class="dropdown_content">
                                        <p><strong>Folate&nbsp;</strong></p>
<p>This test will identify if you have low levels of folic acid (vitamin b9)</p>
<p><strong>Cost:&nbsp;</strong>£45</p>
<p><strong>Active B12 and Folate</strong></p>
<p><strong>Cost:&nbsp;</strong>£130</p>
<p>We would recommend levels of both these vitamins are tested to ensure any deficiency is treated.</p>

                <div class="gf_browser_chrome gform_wrapper" id="gform_wrapper_1"><form method="post" enctype="multipart/form-data" id="gform_1" action="/vitamin-deficiencies/">
                        <div class="gform_heading">
                            <h3 class="gform_title">For more information or to book an appointment</h3>
                            <span class="gform_description"></span>
                        </div>
                        <div class="gform_body"><ul id="gform_fields_1" class="gform_fields top_label form_sublabel_below description_below"><li id="field_1_5" class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_1_5">Name<span class="gfield_required">*</span></label><div class="ginput_container ginput_container_text"><input name="input_5" id="input_1_5" type="text" value="" class="large" placeholder="Name" aria-required="true" aria-invalid="false"></div></li><li id="field_1_2" class="gfield field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_1_2">Email</label><div class="ginput_container ginput_container_email">
                            <input name="input_2" id="input_1_2" type="text" value="" class="large" placeholder="Email" aria-invalid="false">
                        </div></li><li id="field_1_3" class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_1_3">Phone<span class="gfield_required">*</span></label><div class="ginput_container ginput_container_phone"><input name="input_3" id="input_1_3" type="text" value="" class="large" placeholder="Phone" aria-required="true" aria-invalid="false"></div></li><li id="field_1_4" class="gfield field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_1_4">Message</label><div class="ginput_container ginput_container_textarea"><textarea name="input_4" id="input_1_4" class="textarea large" placeholder="Message" aria-invalid="false" rows="10" cols="50"></textarea></div></li>
                            </ul></div>
        <div class="gform_footer top_label"> <input type="submit" id="gform_submit_button_1" class="gform_button button" value="Submit" onclick="if(window[&quot;gf_submitting_1&quot;]){return false;}  window[&quot;gf_submitting_1&quot;]=true;  " onkeypress="if( event.keyCode == 13 ){ if(window[&quot;gf_submitting_1&quot;]){return false;} window[&quot;gf_submitting_1&quot;]=true;  jQuery(&quot;#gform_1&quot;).trigger(&quot;submit&quot;,[true]); }"> 
            <input type="hidden" class="gform_hidden" name="is_submit_1" value="1">
            <input type="hidden" class="gform_hidden" name="gform_submit" value="1">
            
            <input type="hidden" class="gform_hidden" name="gform_unique_id" value="">
            <input type="hidden" class="gform_hidden" name="state_1" value="WyJbXSIsIjllNGI4ZWNhOWE5OGNjNDY1NDIwYzNlOTU4NzhkODYyIl0=">
            <input type="hidden" class="gform_hidden" name="gform_target_page_number_1" id="gform_target_page_number_1" value="0">
            <input type="hidden" class="gform_hidden" name="gform_source_page_number_1" id="gform_source_page_number_1" value="1">
            <input type="hidden" name="gform_field_values" value="">
            
        </div>
                        </form>
                        </div><script type="text/javascript"> jQuery(document).bind('gform_post_render', function(event, formId, currentPage){if(formId == 1) {if(typeof Placeholders != 'undefined'){
                        Placeholders.enable();
                    }} } );jQuery(document).bind('gform_post_conditional_logic', function(event, formId, fields, isInit){} );</script><script type="text/javascript"> jQuery(document).ready(function(){jQuery(document).trigger('gform_post_render', [1, 1]) } ); </script>
                                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>                                                                         </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        General vitamin screen                                  </h2>
                                    <div class="dropdown_content">
                                        <p><strong>Vitamin screen includes:-</strong></p>
<p>Vitamin A, B1, B6, B9 (folate), B12, C, D, E</p>
<p><strong>Cost:&nbsp;</strong>£400</p>

                <div class="gf_browser_chrome gform_wrapper" id="gform_wrapper_1"><form method="post" enctype="multipart/form-data" id="gform_1" action="/vitamin-deficiencies/">
                        <div class="gform_heading">
                            <h3 class="gform_title">For more information or to book an appointment</h3>
                            <span class="gform_description"></span>
                        </div>
                        <div class="gform_body"><ul id="gform_fields_1" class="gform_fields top_label form_sublabel_below description_below"><li id="field_1_5" class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_1_5">Name<span class="gfield_required">*</span></label><div class="ginput_container ginput_container_text"><input name="input_5" id="input_1_5" type="text" value="" class="large" placeholder="Name" aria-required="true" aria-invalid="false"></div></li><li id="field_1_2" class="gfield field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_1_2">Email</label><div class="ginput_container ginput_container_email">
                            <input name="input_2" id="input_1_2" type="text" value="" class="large" placeholder="Email" aria-invalid="false">
                        </div></li><li id="field_1_3" class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_1_3">Phone<span class="gfield_required">*</span></label><div class="ginput_container ginput_container_phone"><input name="input_3" id="input_1_3" type="text" value="" class="large" placeholder="Phone" aria-required="true" aria-invalid="false"></div></li><li id="field_1_4" class="gfield field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_1_4">Message</label><div class="ginput_container ginput_container_textarea"><textarea name="input_4" id="input_1_4" class="textarea large" placeholder="Message" aria-invalid="false" rows="10" cols="50"></textarea></div></li>
                            </ul></div>
        <div class="gform_footer top_label"> <input type="submit" id="gform_submit_button_1" class="gform_button button" value="Submit" onclick="if(window[&quot;gf_submitting_1&quot;]){return false;}  window[&quot;gf_submitting_1&quot;]=true;  " onkeypress="if( event.keyCode == 13 ){ if(window[&quot;gf_submitting_1&quot;]){return false;} window[&quot;gf_submitting_1&quot;]=true;  jQuery(&quot;#gform_1&quot;).trigger(&quot;submit&quot;,[true]); }"> 
            <input type="hidden" class="gform_hidden" name="is_submit_1" value="1">
            <input type="hidden" class="gform_hidden" name="gform_submit" value="1">
            
            <input type="hidden" class="gform_hidden" name="gform_unique_id" value="">
            <input type="hidden" class="gform_hidden" name="state_1" value="WyJbXSIsIjllNGI4ZWNhOWE5OGNjNDY1NDIwYzNlOTU4NzhkODYyIl0=">
            <input type="hidden" class="gform_hidden" name="gform_target_page_number_1" id="gform_target_page_number_1" value="0">
            <input type="hidden" class="gform_hidden" name="gform_source_page_number_1" id="gform_source_page_number_1" value="1">
            <input type="hidden" name="gform_field_values" value="">
            
        </div>
                        </form>
                        </div><script type="text/javascript"> jQuery(document).bind('gform_post_render', function(event, formId, currentPage){if(formId == 1) {if(typeof Placeholders != 'undefined'){
                        Placeholders.enable();
                    }} } );jQuery(document).bind('gform_post_conditional_logic', function(event, formId, fields, isInit){} );</script><script type="text/javascript"> jQuery(document).ready(function(){jQuery(document).trigger('gform_post_render', [1, 1]) } ); </script>
                                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>                                                                         </div>
                                </div>
                                                </div>
                
            </div>
        </div>
    </section>
    <section class="newsletter-style-1 section-padding-30 bg-light-blue">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">

                    <div class="newsletter-content-wrapper mb-md-40">
                        <h3 class="text-custom-white fw-600">Book an appointment</h3>
                        <p class="text-custom-white">You can book an appointment by:</p>
                        <p><span class="fa fa-envelope mr-3" style="color: white;"></span> <a href="#" style="color: white;">info@sfvaccinations.co.uk</a></p>
                        <p><span class="fa fa-phone mr-3" style="color: white;"></span> <a href="tel:01132213533" style="color: white;">0113 221 3533 or </a><a href="tel:01138730242" style="color: white;">0113 873 0242</a></p>
                        <a href="#" class="btn btn-warning">Book Online</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-padding-30"></section>
</div>
@endsection