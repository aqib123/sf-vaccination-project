@extends('website.layouts.base')
@section('content')
<input type="hidden" value="PK" id='hiddenchekuae'>	<div id="content" style="width: 100%;">
				<div id="post-1472" class="post-1472 page type-page status-publish hentry">
			<span class="entry-title" style="display: none;">FAQs</span><span class="vcard" style="display: none;"><span class="fn"><a href="../index.html" title="Posts by admin" rel="author">admin</a></span></span><span class="updated" style="display:none;">2017-05-19T09:32:32+00:00</span>																		<div class="post-content">
				<div class="fusion-fullwidth fullwidth-box fusion-fullwidth-1  fusion-parallax-none nonhundred-percent-fullwidth" style="border-color:#eae9e9;border-bottom-width: 0px;border-top-width: 0px;border-bottom-style: solid;border-top-style: solid;padding-bottom:20px;padding-top:50px;padding-left:px;padding-right:px;"><style type="text/css" scoped="scoped">.fusion-fullwidth-1 {
                            padding-left: px !important;
                            padding-right: px !important;
                        }</style><div class="fusion-row"><div class="fusion-title title fusion-title-size-three custom_heading"><h3 class="title-heading-left">FAQs for Students and Parents </h3><div class="title-sep-container"><div class="title-sep sep-single"></div></div></div><div class="fusion-sep-clear"></div><div class="fusion-separator sep-single" style="border-color:#e0dede;border-top-width:1px;margin-left: auto;margin-right: auto;margin-top:px;margin-bottom:50px;width:100%;max-width:50%;"></div><div class="fusion-title title fusion-title-size-three"><h3 class="title-heading-left">How does My Tutor Source (MTS) work?</h3><div class="title-sep-container"><div class="title-sep sep-double"></div></div></div><div class="about-mts">
<ul>
<li><strong>What is My Tutor Source (MTS)?</strong>
<ul>
<li>My Tutor Source is a way for students and parents to seek help from brilliant educators. Our expert tutors provide one on one online help on very affordable prices and make sure that students perform well in their exams. You don’t need to waste any more time, money and energy on travelling and can enjoy high quality tuition from the comfort of your home!</li>
</ul>
</li>
<li><strong>What are the 3 Steps to hire an Online Tutor at MTS?</strong>
<ul>
<li>Step 1: Click on “Online Trial (Free!)” link and submit your requirements to MTS”.</li>
<li>Step 2: Our administrator would schedule a FREE (30 minutes) “Meet the tutor” call.</li>
<li>Step 3: Finalize the tuition duration in the call and start enjoying your sessions!</li>
</ul>
</li>
<li><strong>Who are the tutors?</strong>
<ul>
<li>MTS only hires experienced professionals! Our experts hold Master&#8217;s Degrees (or higher) in their respective subjects and have 10+ years of teaching experience.</li>
</ul>
</li>
<li><strong>For which Grades/Levels does MTS offer tuition?</strong>
<ul>
<li>We offer professional educators for Junior Level subjects, GCSE/IB/GCE subjects, language courses and test preparations (e.g. SAT1/2, TOEFL, IELTS and GRE etc.) Our expert tutors offer individual help so students can safely expect to meet their academic goals and achieve high grades!</li>
</ul>
</li>
<li><strong>How do you ensure quality?</strong>
<ul>
<li>Every tutoring session is closely monitored by our administrators. They’re also available throughout the call for general guidance and feedback.</li>
</ul>
</li>
<li><strong>How does online tuition with MTS differ from In-Person tutoring?</strong>
<ul>
<li>Generally MTS online tutoring is very much alike In-Person tutoring since the students can simultaneously see the tutor’s live video and his virtual notepad on their laptop screens. However, absence of the need to travel, affordable rates and closely monitored tutoring sessions, are some of the key attributes which give MTS online tutoring a considerable edge over In-Person coaching.</li>
</ul>
</li>
<li><strong>What infrastructure do you need for Online tutoring with MTS?</strong><br />
You need the following few items for the tutoring sessions:</li>
</ul>
</div>
<ul>
<li>A Decent Internet connection</li>
<li>Desktop or a Laptop</li>
<li>Latest Skype (You can download it from: http://www.skype.com/en/download-skype/skype-for-computer/ )</li>
<li><strong>How does MTS involve parents?</strong>
<ul>
<li>MTS starts involving parents from the very first “Meet the Tutor” call. Every decision (for example, the number of tutoring sessions per week, tutoring days of the week) has to be approved by the parents. Parents also have the flexibility to provide feedback directly to MTS management on a regular basis.</li>
</ul>
</li>
</ul>
<div class="fusion-title title fusion-title-size-three" style="margin-top:5px;"><h3 class="title-heading-left">How do you make Payments?</h3><div class="title-sep-container"><div class="title-sep sep-double"></div></div></div></div></div><div class="about-mts">
<ul>
<li><strong>How much does the tuition with MTS cost?</strong>
<ul>
<li>To review the pricing for tutoring classes, please click the <a href="http://www.mytutorsource.com/mts-pricing/"><b>Pricing</b></a> link.</li>
</ul>
</li>
<li><strong>How to make the payment?</strong>
<ul>
<li>MTS uses <a href="https://www.skrill.com/en/" target="_blank">skrill.com</a> for online payments. This payment gateway is secure, cost effective and highly credible. MTS also uses Western Union for International Cash transfer.</li>
</ul>
</li>
</ul>
</div>

							</div>
																	</div>
					</div>
@endsection