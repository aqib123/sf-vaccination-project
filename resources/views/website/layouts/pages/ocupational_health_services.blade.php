@extends('website.layouts.base')
@section('content')
<div class="container">
    @include('website.layouts.logo')
    <section>
        <div class="section-header">
            <div class="section-heading">
                <h3 class="text-custom-black fw-700">Occupational Health Services</h3>
            </div>
        </div>
        <div class="row">
            <article class="col-lg-4 col-md-6 post mb-xl-30">
                <div class="post-wrapper">
                    <div class="blog-img animate-img">
                        <a href="{{url('occupation-health-vaccination')}}">
                            <img src="{{ url('sfvaccination/images/occupation-health-vaccination.jpeg') }}" class="full-width" alt="blog" style="width:100%;height: 230px;">
                        </a>
                    </div>
                    <div class="blog-meta padding-20">
                        <div class="post-content">
                            <h4><a href="{{url('occupation-health-vaccination')}}" class="text-light-green fw-600">Occupational Health Vaccinations</a></h4>
                        </div>
                    </div>
                </div>
            </article>
            <article class="col-lg-4 col-md-6 post mb-xl-30">
                <div class="post-wrapper">
                    <div class="blog-img animate-img">
                        <a href="{{ url('medical') }}">
                            <img src="{{ url('sfvaccination/images/medical.jpeg') }}" class="full-width" alt="blog" style="width:100%;height: 230px;">
                        </a>
                    </div>
                    <div class="blog-meta padding-20">
                        <div class="post-content">
                            <h4><a href="{{ url('medical') }}" class="text-light-green fw-600">Medicals</a></h4>
                        </div>
                    </div>
                </div>
            </article>
            <article class="col-lg-4 col-md-6 post mb-xl-30">
                <div class="post-wrapper">
                    <div class="blog-img animate-img">
                        <a href="{{ url('occupational-health-blood-test') }}">
                            <img src="{{ url('sfvaccination/images/blood-testing.jpeg') }}" class="full-width" alt="blog" style="width:100%;height: 230px;">
                        </a>
                    </div>
                    <div class="blog-meta padding-20">
                        <div class="post-content">
                            <h4><a href="{{ url('occupational-health-blood-test') }}" class="text-light-green fw-600">Occupational Health Blood Test</a></h4>
                        </div>
                    </div>
                </div>
            </article>
            <div style="clear:both;"></div>
        </div>
    </section>
</div>
@endsection