@extends('website.layouts.base')
@section('styling')
<style type="text/css">
    .row {
        margin-left: unset !important;
    }
    h2, .h2 {
        font-size: 1.2rem;
    }
</style>
@endsection
@section('content')
<div class="container">
    @include('website.layouts.logo')
    <section class="section-padding div_with_white_back">
        <div class="section-header">
            <div class="section-heading">
                <h3 class="text-custom-black fw-700">Chronic Migraines Treatment</h3>
            </div>
        </div>
        <div class="row">
            <div class="col_12">
            <p>Chronic migraines are usually defined as:-</p>
<ul>
<li>15 or more headache days a month, of which 8 are migraines</li>
<li>Headaches last for 4 hours or more</li>
<li>With at least half of the headaches being migraine</li>
</ul>
<p>Patients that attend our private clinic will require a letter of diagnosis by their GP or specialist.</p>
<p>A direct referral by the GP or specialist can also be made by emailing our secure email address: <a href="mailto:lenoccg.woodhousemedicalpractice@nhs.net">lenoccg.woodhousemedicalpractice@nhs.net</a></p>
<p>Patients can self-refer by emailing <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a> or arranging phoning our appointment line 0113 221 3533.</p>
<p>A consultation will be completed by a healthcare professional prior to any treatment being given.</p>
<h3><strong>Botulinum toxin A injections (Botox®)</strong></h3>
<p>An effective and proven treatment available for preventing or reducing the frequency of chronic migraines is the administration of botulinum toxin (Botox®). This was approved for use within the NHS by the national healthcare body NICE for the prevention of headaches in adults with chronic migraine. However, some patients may wish to obtain this treatment on a private basis.</p>
<p><span style="display: inline !important; float: none; background-color: transparent; color: #333333; cursor: text; font-family: Georgia,'Times New Roman','Bitstream Charter',Times,serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;">Botox®</span>&nbsp;works by blocking the release of neurotransmitters such as substance P, which causes pain. Also, during a migraine, the muscles can become tense. <span style="display: inline !important; float: none; background-color: transparent; color: #333333; cursor: text; font-family: Georgia,'Times New Roman','Bitstream Charter',Times,serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;">Botox®</span> relaxes the muscles in the head, which then helps reduce the severity of migraines.</p>
<p>A full consultation is completed with a healthcare professional to assess the suitability of treatment.</p>
<p>&nbsp;</p>

                                <div class="dropdown_section">
                        <h2>
                            Further information on botulinum toxin (Botox®) injections                      </h2>

                                                        <div class="dropdown">
                                    <h2>
                                        Who can have botulinum toxin (Botox®) injections?                                   </h2>
                                    <div class="dropdown_content">
                                        <ul>
<li><span style="margin: 0px; font-family: 'Arial','sans-serif';"><span style="color: #000000;">Patients with a diagnosis of chronic migraine by their GP or specialist</span></span></li>
<li><span style="margin: 0px; font-family: 'Arial','sans-serif';"><span style="color: #000000;">Patients who have tried other treatments, which have failed or not been tolerated.&nbsp;</span></span></li>
</ul>
                                    </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2 class="">
                                        How effective are botulinum toxin (Botox®) injections?                                  </h2>
                                    <div class="dropdown_content" style="display: none;">
                                        <p>Efficacy should be assessed after 2 treatments given 3 months apart. The trials showed that aftert2 sets of injections, approximately 70% of patients reported an improvement in their symptoms.</p>
<p>After 5 years, approximately 58% of patients suffered from just episodic migraines.</p>
                                    </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        How long will it take to see the effects?                                   </h2>
                                    <div class="dropdown_content">
                                        <p><span style="margin: 0px; font-family: 'Arial','sans-serif';"><span style="color: #000000;">Most patients normally see an improvement within the first week of treatment but it can take up to 2 weeks.</span></span></p>
                                    </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        How long does the treatment last?                                   </h2>
                                    <div class="dropdown_content">
                                        <p><span style="margin: 0px; font-family: 'Arial','sans-serif';"><span style="color: #000000;">This can vary between patients but is most often between 3-4 months. </span></span></p>
                                    </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        Is the treatment painful?                                   </h2>
                                    <div class="dropdown_content">
                                        <p><span style="margin: 0px; font-family: 'Arial','sans-serif';"><span style="color: #000000;">No the treatment causes little discomfort. </span></span></p>
                                    </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        Are there any side effects?                                 </h2>
                                    <div class="dropdown_content">
                                        <p><span style="margin: 0px; font-family: 'Arial','sans-serif';"><span style="color: #000000;">As with all treatment, there can be some adverse effects. Most commonly, these were headaches, muscle aches and pains and injection-site reactions. Most adverse effects resolve within 48 hours and without treatment. The treatment does not restrict you from continuing your usual activities (apart from intensive exercise and the use of excessive heat).</span></span></p>
<p><span style="margin: 0px; font-family: 'Arial','sans-serif';"><span style="color: #000000;">Trials showed that:-</span></span></p>
<ul>
<li><span style="margin: 0px; font-family: 'Arial','sans-serif';"><span style="color: #000000;">3% of patients had an increase in headaches</span></span></li>
<li><span style="margin: 0px; font-family: 'Arial','sans-serif';"><span style="color: #000000;">3% of patients had an eyebrow ptosis (‘droop’)</span></span></li>
<li><span style="margin: 0px; font-family: 'Arial','sans-serif';"><span style="color: #000000;">7% neck stiffness&nbsp;</span></span></li>
</ul>
                                    </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        How much does it cost?                                  </h2>
                                    <div class="dropdown_content">
                                        <p>£500</p>

                <div class="gf_browser_chrome gform_wrapper" id="gform_wrapper_1"><form method="post" enctype="multipart/form-data" id="gform_1" action="/chronic-migraine-treatment/">
                        <div class="gform_heading">
                            <h3 class="gform_title">For more information or to book an appointment</h3>
                            <span class="gform_description"></span>
                        </div>
                        <div class="gform_body"><ul id="gform_fields_1" class="gform_fields top_label form_sublabel_below description_below"><li id="field_1_5" class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_1_5">Name<span class="gfield_required">*</span></label><div class="ginput_container ginput_container_text"><input name="input_5" id="input_1_5" type="text" value="" class="large" placeholder="Name" aria-required="true" aria-invalid="false"></div></li><li id="field_1_2" class="gfield field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_1_2">Email</label><div class="ginput_container ginput_container_email">
                            <input name="input_2" id="input_1_2" type="text" value="" class="large" placeholder="Email" aria-invalid="false">
                        </div></li><li id="field_1_3" class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_1_3">Phone<span class="gfield_required">*</span></label><div class="ginput_container ginput_container_phone"><input name="input_3" id="input_1_3" type="text" value="" class="large" placeholder="Phone" aria-required="true" aria-invalid="false"></div></li><li id="field_1_4" class="gfield field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_1_4">Message</label><div class="ginput_container ginput_container_textarea"><textarea name="input_4" id="input_1_4" class="textarea large" placeholder="Message" aria-invalid="false" rows="10" cols="50"></textarea></div></li>
                            </ul></div>
        <div class="gform_footer top_label"> <input type="submit" id="gform_submit_button_1" class="gform_button button" value="Submit" onclick="if(window[&quot;gf_submitting_1&quot;]){return false;}  window[&quot;gf_submitting_1&quot;]=true;  " onkeypress="if( event.keyCode == 13 ){ if(window[&quot;gf_submitting_1&quot;]){return false;} window[&quot;gf_submitting_1&quot;]=true;  jQuery(&quot;#gform_1&quot;).trigger(&quot;submit&quot;,[true]); }"> 
            <input type="hidden" class="gform_hidden" name="is_submit_1" value="1">
            <input type="hidden" class="gform_hidden" name="gform_submit" value="1">
            
            <input type="hidden" class="gform_hidden" name="gform_unique_id" value="">
            <input type="hidden" class="gform_hidden" name="state_1" value="WyJbXSIsImU5Y2YwOTc3NjI4NjI0NmE5OTI0OTczN2EyZWIxN2EwIl0=">
            <input type="hidden" class="gform_hidden" name="gform_target_page_number_1" id="gform_target_page_number_1" value="0">
            <input type="hidden" class="gform_hidden" name="gform_source_page_number_1" id="gform_source_page_number_1" value="1">
            <input type="hidden" name="gform_field_values" value="">
            
        </div>
                        </form>
                        </div><script type="text/javascript"> jQuery(document).bind('gform_post_render', function(event, formId, currentPage){if(formId == 1) {if(typeof Placeholders != 'undefined'){
                        Placeholders.enable();
                    }} } );jQuery(document).bind('gform_post_conditional_logic', function(event, formId, fields, isInit){} );</script><script type="text/javascript"> jQuery(document).ready(function(){jQuery(document).trigger('gform_post_render', [1, 1]) } ); </script>
                                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>                                                                         </div>
                                </div>
                                                </div>
                
                    </div>
        </div>
    </section>
    <section class="newsletter-style-1 section-padding-30 bg-light-blue">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="newsletter-content-wrapper mb-md-40">
                        <h3 class="text-custom-white fw-600">Booking</h3>
                        <p class="text-custom-white">You can book an appointment by:</p>
                        <p class="text-custom-white">Phoning 0113 2213533 or 0113 8730242</p>
                        <p class="text-custom-white">Emailing info@sfvaccinations.co.uk</p>
                        <!-- <a href="#" class="btn btn-warning">Book Online</a> -->
                    </div>
                </div>
                <!-- <div class="col-lg-6">
                    <div class="newsletter-form-wrapper">
                        <form>
                            <div class="input-group">
                                <input type="email" name="#" class="form-control form-control-custom" placeholder="Email Id">
                                <div class="input-group-append">
                                    <button type="submit">Subscribe</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div> -->
            </div>
        </div>
    </section>
    <section class="section-padding-30"></section>
</div>
@endsection