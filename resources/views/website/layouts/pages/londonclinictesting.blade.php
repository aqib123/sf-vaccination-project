@extends('website.layouts.base')
@section('content')
<div class="container">
    @include('website.layouts.logo')
    <div class="bg-image p-5 text-center shadow-1-strong rounded mb-5 text-white" style="background-image: url('https://mdbcdn.b-cdn.net/img/new/slides/003.jpg'); background-size:cover; width:100%;">
        <h1 class="mb-3 h2" style="color: #ffffff;">COVID-19 LONDON IN-CLINIC TESTING</h1>
    </div>
    <!-- Section One -->
    <div class="uk-panel uk-panel-box uk-width-medium-1-1 uk-container-center">
        <h3 class="pink" style="text-align: center;">MAKE A LONDON IN-CLINIC COVID-19 TEST APPOINTMENT</h3>
        <div style="text-align: center;"><strong>To make an appointment at one of our leading London clinics, select either City of London or The Shard.</strong></div>
        <p>&nbsp;</p>
        <div class="uk-grid data-uk-grid-margin=">
            <div class="uk-width-medium-1-2 uk-container-center uk-text-center">
                <p>
                    <a title="CityDoc City of London Covid-19 Testing Clinic" href="https://www.citydoc.org.uk/moorgate-travel-health/">
                        <img src="https://www.citydoc.org.uk/images/travel-london/city-moorgate.jpg" alt="CityDoc City of London Covid-19 Testing Clinic" class="lazyloaded" data-ll-status="loaded">
                        <noscript>
                            <img src="https://www.citydoc.org.uk/images/travel-london/city-moorgate.jpg" alt="CityDoc City of London Covid-19 Testing Clinic" />
                        </noscript>
                    </a>
                </p>
                <h4>
                    <a title="CityDoc City of London Covid-19 Testing Clinic" href="https://www.citydoc.org.uk/moorgate-travel-health/">CITYDOC CITY OF LONDON</a>
                </h4>
                <p>16 City Road<br>The Square Mile<br>London<br>EC1Y 2AA</p>
                <p>
                    <strong>Opening Hours:</strong><br>Mon-Fri 9am – 6pm<br>Sat 10am – 4pm
                </p>
                <p>
                    <img src="https://www.citydoc.org.uk/images/underground.png" alt="Moorgate Tube" width="16" height="22" class="lazyloaded" data-ll-status="loaded">
                    <noscript>
                        <img src="https://www.citydoc.org.uk/images/underground.png" alt="Moorgate Tube" width="16" height="22" />
                    </noscript>&nbsp;<strong>Tube:&nbsp;</strong>Moorgate
                </p>
                <div class="uk-hidden-large">
                    <img style="width: 1px; height: 40px;" src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%200%200'%3E%3C/svg%3E" alt="spacer" data-lazy-src="https://www.citydoc.org.uk/images/spacer.png">
                    <noscript>
                        <img style="width: 1px; height: 40px;" src="https://www.citydoc.org.uk/images/spacer.png" alt="spacer" />
                    </noscript>
                </div>
            </div>
            <div class="uk-width-medium-1-2 uk-container-center uk-text-center">
                <p><a title="Shard Covid-19 Testing Clinic" href="https://www.citydoc.org.uk/travel-clinic-shard-southwark/"><img src="https://www.citydoc.org.uk/images/travel-london/city-shardf-sq.jpg" alt="CityDoc Shard Covid-19 Testing Clinic" class="lazyloaded" data-ll-status="loaded"><noscript><img src="https://www.citydoc.org.uk/images/travel-london/city-shardf-sq.jpg" alt="CityDoc Shard Covid-19 Testing Clinic" /></noscript></a></p>
                <h4><a title="CityDoc Shard Covid-19 Testing Clinic" href="https://www.citydoc.org.uk/travel-clinic-shard-southwark/">CITYDOC SHARD</a></h4>
                <p>52 Weston St<br>Bermonsey<br>London<br>SE1 3QJ</p>
                <p><strong>Opening Hours:</strong><br>Mon-Fri 9am – 5pm<br>Sat 9am – 4pm</p>
                <p><img src="https://www.citydoc.org.uk/images/icon-train.png" alt="London Bridge Train Station" width="16" height="22" class="lazyloaded" data-ll-status="loaded"><noscript><img src="https://www.citydoc.org.uk/images/icon-train.png" alt="London Bridge Train Station" width="16" height="22" /></noscript>&nbsp;<strong>Train:&nbsp;</strong>London Bridge</p>
                <div class="uk-hidden-large"><img style="width: 1px; height: 40px;" src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%200%200'%3E%3C/svg%3E" alt="spacer" data-lazy-src="https://www.citydoc.org.uk/images/spacer.png"><noscript><img style="width: 1px; height: 40px;" src="https://www.citydoc.org.uk/images/spacer.png" alt="spacer" /></noscript></div>
            </div>
        </div>
        <p>&nbsp;</p>
        <p><img style="width: 1px; height: 25px;" src="https://www.citydoc.org.uk/images/spacer.png" alt="spacer" class="lazyloaded" data-ll-status="loaded"><noscript><img style="width: 1px; height: 25px;" src="https://www.citydoc.org.uk/images/spacer.png" alt="spacer" /></noscript></p>
        <div style="text-align: center;"><span style="font-size: x-large;">For all other enquiries please <a href="https://www.citydoc.org.uk/contact-us/">email us</a></span></div>
        <p>&nbsp;</p>
    </div>
    <!-- Section Two -->
</div>
@endsection