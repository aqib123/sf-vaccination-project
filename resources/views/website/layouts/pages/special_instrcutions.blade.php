@extends('website.layouts.base')

@section('styling')
<style>
    table {
        border: unset !important;
        padding: unset !important; 
        width: 100%;
    }
</style>
@endsection

@section('content')
<div class="container">
    @include('website.layouts.logo')
    <div class="tab-content padding-20 bg-custom-white bx-wrapper">
        <div class="section-header">
            <div class="section-heading">
                <h3 class="text-custom-black fw-700">Special instructions</h3>
                <div class="section-description">
                    <p class="text-custom-black fw-700">Tests displayed on this website may display numbers in square brackets after the sample type required. These numbers relate to special instructions, see below a guide for each instruction.</p>
                </div>
            </div>
        </div>
        <div class="tab-pane fade show active" id="start">
            <div class="tab-inner">
                <div class="row">
                    <div class="col-lg-12 table-responsive">
                        <table class="table borderless">
                            <thead class="thead-blue">
                                <tr>
                                    <th>Code</th>
                                    <th>Instruction</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($specialInstructionsRecord as $key => $value)
                                <tr>
                                    <td>{{$value['code']}}</td>
                                    <td>{{$value['instructions']}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="section-padding-footer">
    </section>
</div>
@endsection