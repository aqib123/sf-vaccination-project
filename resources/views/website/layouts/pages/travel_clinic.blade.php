@extends('website.layouts.base')
@section('content')
<div class="container">
    @include('website.layouts.logo')
    <section class="section-padding div_with_white_back">
        <div class="section-header">
            <div class="section-heading">
                <h3 class="text-custom-black fw-700">Travel clinic services</h3>
            </div>
        </div>
        <div class="row">
            <article class="col-lg-4 col-md-6 post mb-xl-30">
                <div class="post-wrapper">
                    <div class="blog-img animate-img">
                        <a href="https://travelhealthpro.org.uk" target="_blank">
                            <img src="{{ url('img/country-recomendation.jpeg') }}" class="full-width" alt="blog" style="width:100%;height: 230px;">
                        </a>
                    </div>
                    <div class="blog-meta padding-20">
                        <div class="post-content">
                            <h4><a href="https://travelhealthpro.org.uk" target="_blank" class="text-light-green fw-600">Country recommendations</a></h4>
                        </div>
                    </div>
                </div>
            </article>
            <article class="col-lg-4 col-md-6 post mb-xl-30">
                <div class="post-wrapper">
                    <div class="blog-img animate-img">
                        <a href="{{ url('travel/vaccination') }}">
                            <img src="{{ url('img/injectionimage.webp') }}" class="full-width" alt="blog" style="width:100%;height: 230px;">
                        </a>
                    </div>
                    <div class="blog-meta padding-20">
                        <div class="post-content">
                            <h4><a href="{{ url('travel/vaccination') }}" class="text-light-green fw-600">Travel vaccinations</a></h4>
                        </div>
                    </div>
                </div>
            </article>
            <article class="col-lg-4 col-md-6 post mb-xl-30">
                <div class="post-wrapper">
                    <div class="blog-img animate-img">
                        <a href="{{ url('anti/malarial') }}">
                            <img src="{{ url('img/anti-malarial.jpeg') }}" class="full-width" alt="blog" style="width:100%;height: 230px;">
                        </a>
                    </div>
                    <div class="blog-meta padding-20">
                        <div class="post-content">
                            <h4><a href="{{ url('anti/malarial') }}" class="text-light-green fw-600">Anti malarial</a></h4>
                        </div>
                    </div>
                </div>
            </article>
            <div style="clear:both;"></div>
        </div>
    </section>
    <section class="section-padding-30"></section>
</div>
@endsection