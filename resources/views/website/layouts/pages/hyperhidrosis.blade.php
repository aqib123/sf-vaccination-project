@extends('website.layouts.base')
@section('styling')
<style type="text/css">
    .row {
        margin-left: unset !important;
    }
    h2, .h2 {
        font-size: 1.2rem;
    }
</style>
@endsection
@section('content')
<div class="container">
    @include('website.layouts.logo')
    <section class="section-padding div_with_white_back">
        <div class="section-header">
            <div class="section-heading">
                <h3 class="text-custom-black fw-700">Hyperhidrosis</h3>
            </div>
        </div>
        <div class="row">
            <div class="col_12">
            <p><span style="margin: 0px; color: black; font-family: 'Arial','sans-serif';">Hyperhidrosis (excessive sweating) is sweating that is more than required to maintain normal heat regulation within the body. Most cases of excessive sweating are idiopathic i.e. there is no known cause for it. In some cases, it can be caused by other conditions or medication, e.g. menopause or some antidepressants. </span></p>
<p><span style="margin: 0px; color: black; font-family: 'Arial','sans-serif';">If you have started with a sudden onset of excessive sweating, we would recommend you consulting your GP so any underlying cause can be investigated in the first instance. </span></p>
<p><span style="margin: 0px; color: black; font-family: 'Arial','sans-serif';">There are a few treatments recommended by your GP or pharmacist for the treatment of hyperhidrosis. You may be familiar with a few of them as follows:- </span></p>
<p><span style="margin: 0px; color: black; font-family: 'Arial','sans-serif';">1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Topical over-the-counter antiperspirants</span></p>
<p><span style="margin: 0px; color: black; font-family: 'Arial','sans-serif';">2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Aluminium chloride 10%-35% antiperspirant</span></p>
<p><span style="margin: 0px; color: black; font-family: 'Arial','sans-serif';">3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Anticholinergic medication </span></p>
<p><span style="margin: 0px; color: black; font-family: 'Arial','sans-serif';">4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Intradermal injections of botulinum toxin A</span></p>
<p><span style="margin: 0px; color: black; font-family: 'Arial','sans-serif';">5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Surgery </span></p>
<h3><b><span style="margin: 0px; color: #333333; font-family: 'Arial','sans-serif';">Botulinum toxin A or Anti-Sweating Injections </span></b></h3>
<p><span style="margin: 0px; color: black; font-family: 'Arial','sans-serif';">An effective and proven treatment available for excessive sweating is the administration of botulinum toxin (Botox®) in the problem area, most commonly the underarms.&nbsp; </span></p>
<p><span style="margin: 0px; color: black; font-family: 'Arial','sans-serif';">The toxin works by blocking the nerves that supply the eccrine glands, which in turn prevents sweating. There are millions of sweat glands in the body so the ability to sweat will not disappear, something that would not be safe or desirable. Only the area that is treated will be affected.&nbsp; </span></p>
<p><b><span style="margin: 0px; color: black; font-family: 'Arial','sans-serif';">A full consultation with a healthcare professional is completed to assess the suitability of treatment.</span></b></p>

                                <div class="dropdown_section">
                        <h2>
                            Further information on botulinum toxin injections                       </h2>

                                                        <div class="dropdown">
                                    <h2>
                                        Who can have this treatment?                                    </h2>
                                    <div class="dropdown_content">
                                        <ul>
<li>Patients who have <strong>not</strong> had a sudden onset of excessive sweating. If this is the case, we would recommend you seeing your GP to identify any underlying cause prior to starting treatment.</li>
<li>Patients who have tried other treatments, which have failed or not been tolerated.</li>
<li>Patients who want to reduce the embarrassment and inconvenience caused by overactive sweat glands.</li>
</ul>
                                    </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        How long will it take to see the effects?                                   </h2>
                                    <div class="dropdown_content">
                                        <p><span style="margin: 0px; font-family: 'Arial','sans-serif';"><span style="color: #000000;">Most patients normally see an improvement within the first week of treatment but optimum effects are usually seen after 2 weeks.</span></span></p>
                                    </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        How long does the treatment last?                                   </h2>
                                    <div class="dropdown_content">
                                        <p><span style="margin: 0px; font-family: 'Arial','sans-serif';"><span style="color: #000000;">This can vary between patients but is most often between 4-7 months. Sometimes the effects can persist for up to a year.&nbsp;&nbsp; </span></span></p>
                                    </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        Is the treatment painful?                                   </h2>
                                    <div class="dropdown_content">
                                        <p><span style="margin: 0px; font-family: 'Arial','sans-serif';"><span style="color: #000000;">The underarm and forehead areas are not painful. However, the palms of the hands and soles of the feet maybe more painful and an anaesthetic cream can be applied to help reduce the pain. </span></span></p>
                                    </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        Are there any side effects?                                 </h2>
                                    <div class="dropdown_content">
                                        <p><span style="margin: 0px; font-family: 'Arial','sans-serif';"><span style="color: #000000;">As with all treatment, there can be some adverse effects. Most commonly, these were headaches, pins and needles and injection-site reactions. Most adverse effects resolve within 48 hours and without treatment. The treatment does not restrict you from continuing your usual activities (apart from intensive exercise and the use of excessive heat).&nbsp;&nbsp;&nbsp; </span></span></p>
<p>A full list of side effects can be viewed in the <a href="https://www.medicines.org.uk/emc/files/pil.859.pdf">patient information leaflet</a></p>
<p>&nbsp;</p>
                                    </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        How much does it cost?                                  </h2>
                                    <div class="dropdown_content">
                                        <p>Underarms – £450</p>
<p><span style="margin: 0px; font-family: 'Arial','sans-serif';"><span style="color: #000000;">
                </span></span></p><div class="gf_browser_chrome gform_wrapper" id="gform_wrapper_1"><form method="post" enctype="multipart/form-data" id="gform_1" action="/hyperhidrosis-treatment/">
                        <div class="gform_heading">
                            <h3 class="gform_title">For more information or to book an appointment</h3>
                            <span class="gform_description"></span>
                        </div>
                        <div class="gform_body"><ul id="gform_fields_1" class="gform_fields top_label form_sublabel_below description_below"><li id="field_1_5" class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_1_5">Name<span class="gfield_required">*</span></label><div class="ginput_container ginput_container_text"><input name="input_5" id="input_1_5" type="text" value="" class="large" placeholder="Name" aria-required="true" aria-invalid="false"></div></li><li id="field_1_2" class="gfield field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_1_2">Email</label><div class="ginput_container ginput_container_email">
                            <input name="input_2" id="input_1_2" type="text" value="" class="large" placeholder="Email" aria-invalid="false">
                        </div></li><li id="field_1_3" class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_1_3">Phone<span class="gfield_required">*</span></label><div class="ginput_container ginput_container_phone"><input name="input_3" id="input_1_3" type="text" value="" class="large" placeholder="Phone" aria-required="true" aria-invalid="false"></div></li><li id="field_1_4" class="gfield field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_1_4">Message</label><div class="ginput_container ginput_container_textarea"><textarea name="input_4" id="input_1_4" class="textarea large" placeholder="Message" aria-invalid="false" rows="10" cols="50"></textarea></div></li>
                            </ul></div>
        <div class="gform_footer top_label"> <input type="submit" id="gform_submit_button_1" class="gform_button button" value="Submit" onclick="if(window[&quot;gf_submitting_1&quot;]){return false;}  window[&quot;gf_submitting_1&quot;]=true;  " onkeypress="if( event.keyCode == 13 ){ if(window[&quot;gf_submitting_1&quot;]){return false;} window[&quot;gf_submitting_1&quot;]=true;  jQuery(&quot;#gform_1&quot;).trigger(&quot;submit&quot;,[true]); }"> 
            <input type="hidden" class="gform_hidden" name="is_submit_1" value="1">
            <input type="hidden" class="gform_hidden" name="gform_submit" value="1">
            
            <input type="hidden" class="gform_hidden" name="gform_unique_id" value="">
            <input type="hidden" class="gform_hidden" name="state_1" value="WyJbXSIsImU5Y2YwOTc3NjI4NjI0NmE5OTI0OTczN2EyZWIxN2EwIl0=">
            <input type="hidden" class="gform_hidden" name="gform_target_page_number_1" id="gform_target_page_number_1" value="0">
            <input type="hidden" class="gform_hidden" name="gform_source_page_number_1" id="gform_source_page_number_1" value="1">
            <input type="hidden" name="gform_field_values" value="">
            
        </div>
                        </form>
                        </div><script type="text/javascript"> jQuery(document).bind('gform_post_render', function(event, formId, currentPage){if(formId == 1) {if(typeof Placeholders != 'undefined'){
                        Placeholders.enable();
                    }} } );jQuery(document).bind('gform_post_conditional_logic', function(event, formId, fields, isInit){} );</script><script type="text/javascript"> jQuery(document).ready(function(){jQuery(document).trigger('gform_post_render', [1, 1]) } ); </script><p></p>
<p>&nbsp;</p>
                                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>                                                                         </div>
                                </div>
                                                </div>
                
                    </div>
        </div>
    </section>
    <section class="newsletter-style-1 section-padding-30 bg-light-blue">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="newsletter-content-wrapper mb-md-40">
                        <h3 class="text-custom-white fw-600">Booking</h3>
                        <p class="text-custom-white">You can book an appointment by:</p>
                        <p class="text-custom-white">Phoning 0113 2213533 or 0113 8730242</p>
                        <p class="text-custom-white">Emailing info@sfvaccinations.co.uk</p>
                        <!-- <a href="#" class="btn btn-warning">Book Online</a> -->
                    </div>
                </div>
                <!-- <div class="col-lg-6">
                    <div class="newsletter-form-wrapper">
                        <form>
                            <div class="input-group">
                                <input type="email" name="#" class="form-control form-control-custom" placeholder="Email Id">
                                <div class="input-group-append">
                                    <button type="submit">Subscribe</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div> -->
            </div>
        </div>
    </section>
    <section class="section-padding-30"></section>
</div>
@endsection