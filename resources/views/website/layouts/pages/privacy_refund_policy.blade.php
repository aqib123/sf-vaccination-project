@extends('website.layouts.base')
@section('content')
<input type="hidden" value="PK" id='hiddenchekuae'>	<div id="content" style="width: 100%;">
				<div id="post-1997" class="post-1997 page type-page status-publish hentry">
			<span class="entry-title" style="display: none;">Privacy and Refund Policy</span><span class="vcard" style="display: none;"><span class="fn"><a href="{{ url('/') }}" title="Posts by admin" rel="author">admin</a></span></span><span class="updated" style="display:none;">2016-12-04T19:25:07+00:00</span>																		<div class="post-content">
				<div class="fusion-fullwidth fullwidth-box fusion-fullwidth-1  fusion-parallax-none nonhundred-percent-fullwidth remove-spacing" style="border-color:#eae9e9;border-bottom-width: 0px;border-top-width: 0px;border-bottom-style: solid;border-top-style: solid;padding-bottom:20px;padding-top:85px;padding-left:px;padding-right:px;"><style type="text/css" scoped="scoped">.fusion-fullwidth-1 {
                            padding-left: px !important;
                            padding-right: px !important;
                        }</style><div class="fusion-row"><div class="fusion-title title fusion-title-size-three custom_heading"><h3 class="title-heading-left">Privacy and Refund Policy</h3><div class="title-sep-container"><div class="title-sep sep-single"></div></div></div><div class="fusion-sep-clear"></div><div class="fusion-separator sep-single" style="border-color:#e0dede;border-top-width:1px;margin-left: auto;margin-right: auto;margin-top:px;width:100%;max-width:25%;"></div><div class="about-mts">
<ul>
<li><strong>My Tutor Source Privacy Policy</strong>
<ul>
<li>My Tutor Source is very well aware of the security of personal information. Our administrators would need to collect some personal information (e.g Skype ID, Phone #, Grade/Level of education etc) from you to set up your account with us. We need your personal information to be able to do:
<ul>
<li>Make your tutoring sessions personal</li>
<li>Make sure that you’re on the right track</li>
</ul>
</li>
<li>Students/parents will not share their personal information with the tutor/s (such as but not limited to: Skype IDs, email addresses and phone numbers etc.) MTS administrators will handle all logistics of communications.</li>
<li>Students are advised to not engage in personal conversations with the tutor during and after the tutoring sessions. This is to maintain professionalism and a healthy environment where delivering quality education is the primary goal.</li>
</ul>
</li>
<li><strong>Cookies</strong>
<ul>
<li>Cookies are files that your Web browser puts on your computer when you visit a Web site. We use cookies to tell us how often you visit our site, and what pages you go to on our site. The cookie itself does not have personal information in it, so no one except Retlmtutors.com can figure out who you are from our cookie.</li>
<li>Sometimes we use files from other companies (called third-party cookies) to help us see where people go on our website, to improve our services. Third-party cookies do not contain any personally identifying information, and the third party cannot figure out who you are, or contact you.</li>
</ul>
</li>
<li><strong>Refund/Withdrawal Policy</strong>
<ul>
<li>My Tutor Source (MTS) takes full responsibility of the tutoring classes and ensures that these classes are both beneficial and fun for the students.</li>
<li>Payment for the tutoring classes is made before the sessions. This amount is non-refundable. Of course, The MTS team ensures that all sessions are successfully delivered.</li>
<li>If students/parents are not satisfied by the performance of a specific tutor, MTS administration will conduct a thorough review and may change the tutor for the remaining sessions. No refunds shall be made.</li>
</ul>
</li>
</ul>
</div>
</div></div>
							</div>
																	</div>
					</div>
@endsection