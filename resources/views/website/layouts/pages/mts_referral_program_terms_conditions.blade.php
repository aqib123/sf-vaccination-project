@extends('website.layouts.base')
@section('content')
<input type="hidden" value="PK" id='hiddenchekuae'>	<div id="content" style="width: 100%;">
				<div id="post-2661" class="post-2661 page type-page status-publish hentry">
			<span class="entry-title" style="display: none;">Terms &#038; Conditions for Referral Program</span><span class="vcard" style="display: none;"><span class="fn"><a href="{{url('/')}}" title="Posts by admin" rel="author">admin</a></span></span><span class="updated" style="display:none;">2019-12-26T08:40:42+00:00</span>																		<div class="post-content">
				<div class="fusion-fullwidth fullwidth-box fusion-fullwidth-1  fusion-parallax-none nonhundred-percent-fullwidth" style="border-color:#eae9e9;border-bottom-width: 0px;border-top-width: 0px;border-bottom-style: solid;border-top-style: solid;padding-bottom:20px;padding-top:70px;padding-left:px;padding-right:px;"><style type="text/css" scoped="scoped">.fusion-fullwidth-1 {
                            padding-left: px !important;
                            padding-right: px !important;
                        }</style><div class="fusion-row"><div class="fusion-title title fusion-title-size-three custom_heading"><h3 class="title-heading-left">Terms &amp; Conditions for Referral Program</h3><div class="title-sep-container"><div class="title-sep sep-single"></div></div></div><div class="fusion-sep-clear"></div><div class="fusion-separator sep-single" style="border-color:#e0dede;border-top-width:1px;margin-left: auto;margin-right: auto;margin-top:px;margin-bottom:30px;width:100%;max-width:60%;"></div><p>Apart from providing unmatched quality of tutoring classes, My Tutor Source (MTS) also provides amazing monetary benefits (up to <strong>US$5000/month</strong>) to its users/customers. A user/customer can become an Ambassador with MTS and earn revenue share from their referred friends and families directly into their bank accounts.</p>
<ul>
<li><strong>How to earn Up to 15% revenue through your referred families:</strong>
<ul>
<li>As soon as a customer signs up on the website, a “family referral code” is generated. The customer can forward this referral code to their friends and family. When a referred customer, makes the first purchase from MTS (using referee’s referral code), the referee is automatically given a certain percentage (depending on their current Ambassador Status) from the total amount given by the referred customer. So for example if the referee is a Gold Ambassador and the referred customer pays $500 for the tutoring classes, the referee will be given $75 (15% revenue share) for referring the customer.</li>
<li>The referee will be given this revenue share (based on their Ambassador Status) for a period of 6 months since the referred customer joined MTS. So for example if John referred Mike and Mike registered with MTS on 1<sup>st</sup> of January 2020, John will keep getting 10 to 15% revenue share (depending on his Ambassador Status) from Mike’s payments to MTS till 1<sup>st</sup> of June 2020.</li>
</ul>
</li>
<li><strong>List of Ambassador Statuses:</strong>
<ul>
<li>Bronze Ambassador (10% Revenue Share on each referred student)
<ul>
<li>Default Ambassador Status of anyone signing up on MTS for the first time is “Bronze”. Bronze Ambassador gets a revenue share of 10% on each payment made by each one of their referred students.</li>
</ul>
</li>
<li>Silver Ambassador (12% Revenue Share on each referred student)
<ul>
<li>When an Ambassador has successfully referred more than 10 students to MTS, his/her Ambassador status is upgraded to “Silver”. Silver Ambassador gets a revenue share of 12% on each payment made by each one of their referred students (new and existing).</li>
</ul>
</li>
<li>Gold Ambassador (15% Revenue Share on each referred student)
<ul>
<li>When an Ambassador has successfully referred more than 25 students to MTS, his/her Ambassador status is upgraded to “Gold”. Gold Ambassador gets a revenue share of 15% on each payment made by each one of their referred students (new and existing).</li>
</ul>
</li>
</ul>
</li>
<li><strong>Prohibited Use of Referral Code:</strong>
<ul>
<li>Customer (student and the immediate family) can use the Referral Code only to refer other individuals/families to MTS tutoring, this referral code cannot be used in any circumstances by the customer themselves in order to get revenue share. Customers are requested to keep only one account with MTS in order to avoid this activity. If a customer is found guilty of using their own Referral Code to gain monetary benefit, they will not be given any revenue share and in case they’re using MTS services, they will not be allowed to continue their tutoring classes with MTS and no amount shall be refunded.</li>
</ul>
</li>
<li><strong>Termination and Changes:</strong>
<ul>
<li>MTS may suspend, update or terminate the Referral Program or a customer’s ability to participate in the Referral Program at any time for any reason.</li>
<li>We reserve the right to suspend accounts or remove discount/monetary gain if we notice any activity that we believe is abusive, fraudulent, or in violation of the MTS Terms of Service or Payments Terms of Service. We reserve the right to review and investigate all referral activities and to suspend accounts or modify referrals in our sole discretion as deemed fair and appropriate.</li>
</ul>
</li>
</ul>
</div></div>
							</div>
																	</div>
					</div>
@endsection