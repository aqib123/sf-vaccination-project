@extends('website.layouts.base')
@section('styling')
<style type="text/css">
    .row {
        margin-left: unset !important;
    }
    h2, .h2 {
        font-size: 1.2rem;
    }
</style>
@endsection
@section('content')
<div class="container">
    @include('website.layouts.logo')
    <section class="section-padding div_with_white_back">
        <div class="section-header">
            <div class="section-heading">
                <h3 class="text-custom-black fw-700">Private Blood Tests</h3>
            </div>
        </div>
        <div class="row">
            <div class="col_12">
                <h3>Coronavirus (COVID-19) Tests</h3>
                <p>Please visit our <a href="http://www.sfvaccinations.co.uk/covid-19-service-and-tests/">COVID-19 service and tests page</a> for further information on the validated COVID-19 antibody and PCR tests.</p>
                <h3>What private blood tests are available?</h3>
                <p>We can conduct a wide range of private blood tests in our private vaccination clinic at Woodhouse Medical Practice, Leeds. We also provide home-testing kits for a variety of diseases including sexual health, hepatitis B, etc. These may include the following:-</p>
                <ul>
                    <li>Checking immunity levels after a course of vaccinations such as hepatitis B</li>
                    <li>Occupational health screening&nbsp;prior to starting work in a clinical setting e.g. healthcare workers</li>
                    <li>Sexual health diseases&nbsp;such&nbsp;chlamydia, gonorrhoea, HIV.</li>
                    <li>Visa requirements e.g. for the USA</li>
                    <li>General health screening e.g. lipid profile, HbA1c.</li>
                    <li>COVID-19 Antibody Test</li>
                </ul>
                <p>We have listed below some of the blood tests available.</p>
                <p><strong>N.B. The list is not exhaustive and we are happy to advise you on a number of other blood tests available.</strong></p>
                <h3>Do I need a GP referral to use this service?</h3>
                <p>No. Patients are able to self-refer to this service. However, we do require your GP contact details so they are provided with a copy of the test results, and further testing or treatments can be arranged. If you don’t want your GP knowing your test results, please speak to our healthcare professional about this.</p>
                <h3>Home blood test-kits vs clinic testing</h3>
                <p>We provide private blood tests as home-testing kits or you can attend our clinic based in Leeds. We use accredited laboratories for all tests. There is a wider range of blood tests available within the clinic setting. However, if you prefer the convenience and privacy of having the test within your home, then a self-collection test kit may be more suitable. If the test is not listed below, you can email our clinic with your request at: <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>
                <p>&nbsp;</p>
                <div class="dropdown_section">
                        <h2>Coronavirus (COVID-19) Tests</h2>
                            <div class="dropdown">
                                <h2>Coronavirus (COVID-19) Antibody Test</h2>
                                <div class="dropdown_content">
                                    <p>Please visit our <a href="http://www.sfvaccinations.co.uk/covid-19-service-and-tests/">COVID-19 service and tests page</a> for further information on the validated COVID-19 antibody and PCR tests.</p>
                                    <p>&nbsp;</p>
                                    <p>&nbsp;</p>
                                    <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>                                                                
                                </div>
                            </div>
                            <div class="dropdown">
                                <h2>Coronavirus (COVID-19) PCR Test</h2>
                                <div class="dropdown_content">
                                    <p>Please visit our <a href="http://www.sfvaccinations.co.uk/covid-19-service-and-tests/">COVID-19 service and tests page</a> for further information on the validated COVID-19 antibody and PCR tests.</p>
                                    <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>                                                               
                                </div>
                                </div>
                                                </div>
                                    <div class="dropdown_section">
                        <h2>
                            Home-Testing Kits                       </h2>

                                                        <div class="dropdown">
                                    <h2>
                                        General Health Check                                    </h2>
                                    <div class="dropdown_content">
                                        <p>The following parameters will be checked in line with the general NHS Health Check:-</p>
<ul>
<li><strong>HbA1c</strong> – to check your risk of diabetes</li>
<li><strong>Cholesterol</strong> – to assess your risk of a stroke and heart disease</li>
<li><strong>Liver Function Test</strong> – to identify any abnormalities that can increase your risk of some diseases</li>
<li><strong>Renal Function Test </strong>– to help identify any kidney problems</li>
</ul>
<p><strong>Cost:</strong> £80</p>
<p>&nbsp;</p>
                                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>                                                                         </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        Anaemia and General Tiredness Profile                                   </h2>
                                    <div class="dropdown_content">
                                        <p>The following parameters will be checked:-</p>
<ul>
<li><strong>Thyroid Function Test</strong> – to identify any problems with your thyroid that can lead to tiredness and weight gain</li>
<li><strong>Full Blood Count</strong> – to identify any problems with your red blood cells, platelets and white blood cells which may cause problems such as anaemia</li>
<li><strong>Iron Profile</strong> – to identify any deficiency in your iron levels which can lead to anaemia</li>
<li><strong>Vitamin and Minerals Profile&nbsp;</strong>– to identify common deficiencies that can cause tiredness. Vitamin D, calcium, vitamin B12 (active) and folate levels will be checked</li>
</ul>
<p><strong>Cost:</strong> £150</p>
                                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>                                                                         </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        Well Person Check                                   </h2>
                                    <div class="dropdown_content">
                                        <p>The following parameters will be checked:-</p>
<ul>
<li><strong>HbA1c</strong> – to check your risk of diabetes</li>
<li><strong>Cholesterol</strong> – to assess your risk of a stroke and heart disease</li>
<li><strong>Liver Function Test</strong> – to identify any abnormalities that can increase your risk of some diseases</li>
<li><strong>Renal Function Test </strong>– to help identify any kidney problems</li>
<li><strong>Thyroid Function Test</strong> – to identify any problems with your thyroid that can lead to tiredness and weight gain</li>
<li><strong>Full Blood Count</strong> – to identify any problems with your red blood cells, platelets and white blood cells which may cause problems such as anaemia</li>
<li><strong>Iron Profile</strong> – to identify any deficiency in your iron levels which can lead to anaemia</li>
<li><strong>Vitamin and Minerals Profile&nbsp;</strong>– to identify common deficiencies that can cause tiredness. Vitamin D, calcium, vitamin B12 (active) and folate levels will be checked</li>
</ul>
<p><strong>Cost:</strong> £140</p>
                                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>                                                                         </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        Well Man Check                                  </h2>
                                    <div class="dropdown_content">
                                        <p>The following parameters will be checked:-</p>
<ul>
<li><strong>HbA1c</strong> – to check your risk of diabetes</li>
<li><strong>Cholesterol</strong> – to assess your risk of a stroke and heart disease</li>
<li><strong>Liver Function Test</strong> – to identify any abnormalities that can increase your risk of some diseases</li>
<li><strong>Renal Function Test </strong>– to help identify any kidney problems</li>
<li><strong>Thyroid Function Test</strong> – to identify any problems with your thyroid that can lead to tiredness and weight gain</li>
<li><strong>Full Blood Count</strong> – to identify any problems with your red blood cells, platelets and white blood cells which may cause problems such as anaemia</li>
<li><strong>Iron Profile</strong> – to identify any deficiency in your iron levels which can lead to anaemia</li>
<li><strong>Vitamin and Minerals Profile&nbsp;</strong>– to identify common deficiencies that can cause tiredness. Vitamin D, calcium, vitamin B12 and folate levels will be checked</li>
<li><strong>Testosterone Level&nbsp;</strong></li>
</ul>
<p><strong>Cost:</strong> £180</p>
                                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>                                                                         </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        Well Woman Check                                    </h2>
                                    <div class="dropdown_content">
                                        <p>The following parameters will be checked:-</p>
<ul>
<li><strong>HbA1c</strong> – to check your risk of diabetes</li>
<li><strong>Cholesterol</strong> – to assess your risk of a stroke and heart disease</li>
<li><strong>Liver Function Test</strong> – to identify any abnormalities that can increase your risk of some diseases</li>
<li><strong>Renal Function Test </strong>– to help identify any kidney problems</li>
<li><strong>Thyroid Function Test</strong> – to identify any problems with your thyroid that can lead to tiredness and weight gain</li>
<li><strong>Full Blood Count</strong> – to identify any problems with your red blood cells, platelets and white blood cells which may cause problems such as anaemia</li>
<li><strong>Iron Profile</strong> – to identify any deficiency in your iron levels which can lead to anaemia</li>
<li><strong>Vitamin and Minerals Profile&nbsp;</strong>– to identify common deficiencies that can cause tiredness. Vitamin D, calcium, vitamin B12 and folate levels will be checked</li>
<li><strong>Female Hormone Level Profile</strong> -to identify the levels of follicle-stimulating hormone, luteinising hormone and oestradiol</li>
</ul>
<p><strong>Cost:</strong> £180</p>
                                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>                                                                         </div>
                                </div>
                                                </div>
                                    <div class="dropdown_section">
                        <h2>
                            Summary of occupational blood tests                      </h2>

                                                        <div class="dropdown">
                                    <h2 class="">
                                        Blood Type Testing                                  </h2>
                                    <div class="dropdown_content" style="display: none;">
                                        <p>A blood test can be carried out to determine a patients blood type.</p>
<p>Please email our healthcare professional for further advice and information at <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>
<p><strong>Cost</strong></p>
<p>£70</p>
                                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>                                                                         </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        Chickenpox (varicella)                                  </h2>
                                    <div class="dropdown_content">
                                        <p>A blood test can be carried out prior to receiving the chickenpox vaccination if you are unsure if you have had chickenpox disease.</p>
<p>Please email our healthcare professional for further advice and information at <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>
<p><strong>Cost</strong></p>
<p>£60</p>
                                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>                                                                         </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        Diphtheria and Tetanus                                  </h2>
                                    <div class="dropdown_content">
                                        <p>We recommend you having a blood test 3 months after completing the course of vaccinations.</p>
<p>Please email our healthcare professional for further advice and information at <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>
<p><strong>Cost</strong></p>
<p>£50 per antigen</p>
                                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>                                                                         </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        Drugs of Abuse                                  </h2>
                                    <div class="dropdown_content">
                                        <p>Testing can be done using a sample of the patients saliva or urine. A testing kit with a pre-paid envelope can be sent out. Specimens can be tested for a wide variety of substances including the presence of Morphine, Codeine, Dihydrocodeine, Amphetamine, Cannabis, Cocaine metbolite, Nordiazepam, Oxazepam, Methadone. Alcohol testing can also be performed. Please note, this list is not exhaustive. Please contact the clinic if you are requiring specific substances to be tested.</p>
<p>We can also provide drugs and alcohol testing for your workplace.</p>
<p>Please email our healthcare professional for further advice and information at <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>
<p><strong>Cost</strong></p>
<p>Prices start from £150</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
                                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>                                                                         </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        Hepatitis B Immunity/Screen                                 </h2>
                                    <div class="dropdown_content">
                                        <p>We can conduct a hepatitis B screen and provide you with the results within 3-4 working days.</p>
<p>If you only require a test to determine your immunity after a course of hepatitis B vaccinations, we recommend you having a blood test 1-4 months after completing the course. This is strongly recommended for all healthcare workers and high-risk occupations. This is to ensure you have developed the appropriate level of antibodies after completing the course of vaccinations as not all patients respond after the first course is completed.</p>
<p>Please email our healthcare professional for further advice and information at <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>
<p><strong>Cost</strong></p>
<p>£55</p>
                                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>                                                                         </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        Hepatitis C Screen                                  </h2>
                                    <div class="dropdown_content">
                                        <p>We can conduct a hepatitis C screen and provide you with the results within 3 working days.</p>
<p>Please email our healthcare professional for further advice and information at <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>
<p><strong>Cost</strong></p>
<p>£55</p>
                                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>                                                                         </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        HIV Screen                                  </h2>
                                    <div class="dropdown_content">
                                        <p>We can conduct a full HIV screen and provide you with the results within 3 working days.</p>
<p>Please email our healthcare professional for further advice and information at <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>
<p><strong>Cost</strong></p>
<p>£55</p>
                                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>                                                                         </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        Mantoux Test (Tuberculin Skin Test)                                 </h2>
                                    <div class="dropdown_content">
                                        <p>This test helps determine if you’ve previously had the BCG vaccination or if you have active or latent tuberculosis disease.</p>
<p>It is used prior to administering the BCG vaccination to ensure it is safe to do. Results are read 72 hours later.</p>
<p>Please email our healthcare professional for further advice and information at <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>
<p><strong>Cost&nbsp;</strong>£150</p>
<p>Dependent on availability</p>
                                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>                                                                         </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        Measles/mumps/rubella                                   </h2>
                                    <div class="dropdown_content">
                                        <p>We recommend you having a blood test 6 weeks after completing the course of vaccinations if requested by your employer.</p>
<p>Please email our healthcare professional for further advice and information at <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>
<p><strong>Cost</strong></p>
<p>£55 per antigen tested</p>
                                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>                                                                         </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        Rabies Antibody Test                                    </h2>
                                    <div class="dropdown_content">
                                        <p>This test is used to determine antibody levels after a course of rabies vaccines.</p>
<p>This is usually done for occupational health reasons.</p>
<p><strong>Cost&nbsp;</strong></p>
<p>£180</p>
<p>&nbsp;</p>
                                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>                                                                         </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        Sexual Health Screen                                    </h2>
                                    <div class="dropdown_content">
                                        <p>We can conduct a wide range of blood tests for sexually transmitted diseases or infections. The results of these tests are available within 3 working days.</p>
<p>Please email our healthcare professional for further advice and information at <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>
<p><strong>Cost</strong></p>
<p>Will depend on the range of tests conducted</p>
                                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>                                                                         </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        TB Quantiferon Test                                 </h2>
                                    <div class="dropdown_content">
                                        <p>This test is available to assess Tuberculosis (TB) infection. This test can be used to determine both active infection and latent TB infection and is widely used as a screening test for healthcare workers.</p>
<p>The test is not affected by previous BCG vaccination so will not produce false positives unlike the Tuberculin Skin Test.</p>
<p>Please email our healthcare professional for further advice and information at <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>
<p><strong>Cost</strong></p>
<p>£150</p>
                                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>                                                                         </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        T-Spot Test                                     </h2>
                                    <div class="dropdown_content">
                                        <p>This test is primarily for the detection of latent tuberculosis. It is also used for detecting active disease in patients whom the TB Quantiferon Test is unsuitable.</p>
<p><strong>This test is suitable for young children.</strong></p>
<p><span style="display: inline !important; float: none; background-color: transparent; color: #333333; cursor: text; font-family: Georgia,'Times New Roman','Bitstream Charter',Times,serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;">Please email our healthcare professional for further advice and information at:</span></p>
<p><a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>
<p><strong>Cost</strong></p>
<p>£295</p>
                                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>                                                                         </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        Testosterone level                                  </h2>
                                    <div class="dropdown_content">
                                        <p>We can conduct various blood tests to determine any imbalance in male hormonal levels. The results of these tests are available within 3 working days.</p>
<p>Please email our healthcare professional for further advice and information at <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>
<p><strong>Cost:&nbsp;</strong>£50 (if only testosterone levels are required)</p>
<p>Will depend on the range of blood tests.</p>
                                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>                                                                         </div>
                                </div>
                                                            <div class="dropdown">
                                    <h2>
                                        Zika Antibody Test                                  </h2>
                                    <div class="dropdown_content">
                                        <p>Current recommendations state that couples should avoid pregnancy within 3 months of travel to a <a href="https://www.gov.uk/guidance/zika-virus-country-specific-risk#atoz">Zika-risk area</a>. Further information can be found on <a href="https://travelhealthpro.org.uk/news/4/zika-virus--update-and-advice-for-travellers">TravelHealthPro</a>.</p>
<p>If travel to a <a href="https://www.gov.uk/guidance/zika-virus-country-specific-risk#atoz">Zika-risk area</a>&nbsp;is unavoidable, you can have some tests to first determine if you have contracted the disease.</p>
<p>If you are pregnant and suspect you are infected with the Zika virus, please contact your GP in the first instance.</p>
<p>Please email our healthcare professional for further advice and information at <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>
<p><strong>Cost</strong></p>
<p>£195 for Zika antibody test (4-12 weeks after leaving area)</p>
<p>£300 for Zika PCR tests (within 14 days of first potential exposure)</p>
                                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>                                                                         </div>
                                </div>
                                                </div>
                
                    </div>
            <div style="clear:both;"></div>
        </div>
    </section>
    <section class="newsletter-style-1 section-padding-30 bg-light-blue">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="newsletter-content-wrapper mb-md-40">
                        <h3 class="text-custom-white fw-600">Book an appointment</h3>
                        <p class="text-custom-white">You can book an appointment by:</p>
                        <p><span class="fa fa-envelope mr-3" style="color: white;"></span> <a href="#" style="color: white;">info@sfvaccinations.co.uk</a></p>
                        <p><span class="fa fa-phone mr-3" style="color: white;"></span> <a href="tel:01132213533" style="color: white;">0113 221 3533 or </a><a href="tel:01138730242" style="color: white;">0113 873 0242</a></p>
                        <a href="#" class="btn btn-warning">Book Online</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-padding-30"></section>
</div>
@endsection