@extends('website.layouts.base')
@section('styling')
<style type="text/css">
    h2, .h2 {
        font-size: 1.2rem;
    }
</style>
@endsection
@section('content')
<div class="container">
    @include('website.layouts.logo')
    <section class="section-padding div_with_white_back">
        <div class="section-header">
            <div class="section-heading">
                <h3 class="text-custom-black fw-700">Onsite Testing Service</h3>
            </div>
        </div>
        <div class="row" style="margin-left: unset !important;">
            <div class="col_12">
                <h2 class="bg bg-warning">Under Construction</h2>
            </div>
            <div style="clear:both;"></div>
        </div>
    </section>
    <section class="newsletter-style-1 section-padding-30 bg-light-blue">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="newsletter-content-wrapper mb-md-40">
                        <h3 class="text-custom-white fw-600">Booking</h3>
                        <p class="text-custom-white">You can book an appointment by:</p>
                        <p class="text-custom-white">Phoning 0113 2213533 or 0113 8730242</p>
                        <p class="text-custom-white">Emailing info@sfvaccinations.co.uk</p>
                        <a href="#" class="btn btn-warning">Book Online</a>
                    </div>
                </div>
                <!-- <div class="col-lg-6">
                    <div class="newsletter-form-wrapper">
                        <form>
                            <div class="input-group">
                                <input type="email" name="#" class="form-control form-control-custom" placeholder="Email Id">
                                <div class="input-group-append">
                                    <button type="submit">Subscribe</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div> -->
            </div>
        </div>
    </section>
    <section class="section-padding-30"></section>
</div>
@endsection