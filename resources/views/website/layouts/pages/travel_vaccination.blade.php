@extends('website.layouts.base')
@section('styling')
<style type="text/css">
    h2, .h2 {
        font-size: 1.2rem;
    }
</style>
@endsection
@section('content')
<div class="container">
    @include('website.layouts.logo')
    <section class="section-padding div_with_white_back">
        <div class="section-header">
            <div class="section-heading">
                <h3 class="text-custom-black fw-700">Travel vaccination</h3>
            </div>
        </div>
        <div class="row" style="margin-left: unset !important;">
                <table class="table table-responsive-sm">
                    <thead>
                        <tr>
                            <th>Vaccination</th>
                            <th>How many doses?</th>
                            <th>How long does immunity last?</th>
                            <th>Who needs it?</th>
                            <th>Cost</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>BCG vaccination</td>
                            <td>1 dose</td>
                            <td>No further boosters are recommended. However, immunity wanes after approximately 10 years</td>
                            <td>This is rarely needed for travel unless residing for >3 months or working in healthcare in a high-risk country.</td>
                            <td>Dependent on availability</td>
                        </tr>
                        <tr>
                            <td>Cholera (oral vaccination)</td>
                            <td>Children 2-6years: 3 doses given 1 week apart<br>Children and adults>6 years: 2 doses given 1 week apart</td>
                            <td>Children 2-6years: 6 months<br>Children and adults>6 years: 2years</td>
                            <td>Aid workers and those visiting areas of poor sanitation; after natural disasters or cholera outbreak.</td>
                            <td>£30 per dose</td>
                        </tr>
                        <tr>
                            <td>Diphtheria/Tetanus/Polio vaccination</td>
                            <td>1 dose (as long as you have had a full course of immunisations)</td>
                            <td>Tetanus – 10-20 years if adult booster dose received</td>
                            <td>We recommend this for most travellers as tetanus spores exist worldwide.</td>
                            <td>£35</td>
                        </tr>
                        <tr>
                            <td>Japanese encephalitis vaccination</td>
                            <td>2 doses.<br>18 to 65 years: 1 or 4 weeks apart<br><18 years or >65 years: 4 weeks apart</td>
                            <td>2 years<br>A booster dose will provide an additional 10 years protection</td>
                            <td>When visiting risk areas such as rural areas, rice fields and marshland within transmission seasons in Asia and the Pacific rim</td>
                            <td>£90 per dose – not available on the NHS</td>
                        </tr>
                        <tr>
                            <td>Hepatitis A vaccination</td>
                            <td>1 dose</td>
                            <td>25 years if a second dose is given within a few years after the first</td>
                            <td>Those visiting risk areas such as areas with poor sanitation, long-stay-travellers or those visit friends and family</td>
                            <td>£60 per dose</td>
                        </tr>
                         <tr>
                            <td>Hepatitis B vaccination</td>
                            <td>3  doses at 0,1 and 2 months or 0,1 and 6 months or 4 doses at 0,7 and 21 days and 12 months.</td>
                            <td>20-30 years after primary course</td>
                            <td>Those who may be exposed through transfer of contaminated fluid e.g. healthcare workers, those with chronic conditons, those who may have unprotected sex, etc</td>
                            <td>£45 per dose – not available on the NHS for most travellers</td>
                        </tr>
                        <tr>
                            <td>Meningitis ACWY vaccination</td>
                            <td>Babies<1 years: 2 doses given 1 month apart<br>Children and adults>1year: 1 dose</td>
                            <td>3 years for Hajj/Umrah visa<br>Minimum 5 years</td>
                            <td>Travellers visiting risk areas prone to outbreaks or staying with the locals; mandatory for Hajj/Umrah pilgrimage</td>
                            <td>£60 per dose – not available on the NHS for travel</td>
                        </tr>
                        <tr>
                            <td>Rabies vaccination</td>
                            <td>3 doses:<br>Usually given over 21-28 days. A rapid schedule consisting of 3 doses given in 7 days can now be given for patients aged between 18-65 years.</td>
                            <td>One booster dose after a minimum of 5 years is recommended for travellers. No further boosters are required for general travel.If the primary course was completed as a rapid course i.e. over 7 days, then a booster is recommended after 12 months.</td>
                            <td>Those travelling to risk areas where they may not have close access to medical care; those working with animals.</td>
                            <td>£60 per dose – not available on the NHS</td>
                        </tr>
                        <tr>
                            <td>Rotavirus vaccination</td>
                            <td>2 doses:<br>Given at a 4-week interval</td>
                            <td>There is no data on immunity in adults.</td>
                            <td>This vaccine is given as part of the NHS childhood immunisation schedule. Some patients may require this for visa requirements within our clinic. It is used off-license in patients over the age of 24 weeks.</td>
                            <td>£90 per dose – not available on the NHS outside the childhood immunisation schedule</td>
                        </tr>
                        <tr>
                            <td>Tick-borne encephalitis vaccination</td>
                            <td>3 doses:<br>2 doses given 2-4 weeks apart, 3rd dose 5-12 months after second dose</td>
                            <td>3-5 years depending on age and number of vaccinations</td>
                            <td>Those travelling to risk areas especially within forested areas</td>
                            <td>£70 per dose – not available on the NHS</td>
                        </tr>
                        <tr>
                            <td>Typhoid vaccination</td>
                            <td>1 dose</td>
                            <td>3 years</td>
                            <td>Those visiting risk areas such as areas with poor sanitation, young children, long-stay-travellers or those visit friends and family</td>
                            <td>£35 – injectable<br>£45 – oral</td>
                        </tr>
                        <tr>
                            <td>Whooping cough/Pertussis vaccination</td>
                            <td>1 dose</td>
                            <td>There is no current data available on the duration of protection the vaccine will provide against pertussis. The vaccine will provide 10-years protection against diphtheria, tetanus and polio.</td>
                            <td>Due to outbreaks of whooping cough in Australia and the USA, the pertussis vaccine is sometimes requested if a close contact is pregnant or have recently given birth.<br><b>N.B. The vaccine cannot be given if a vaccine containing diphtheria/tetanus/polio has been given within the preceding month.</b></td>
                            <td>£90 – not available on the NHS for travel</td>
                        </tr>
                        <tr>
                            <td>Yellow fever vaccination</td>
                            <td>1 dose</td>
                            <td>Lifelong. UK guidance has stated 10 years for certain groups of patients.</td>
                            <td>Those travelling to risk areas or requiring a yellow fever certificate</td>
                            <td>£65 – not available on the NHS</td>
                        </tr>
                    </tbody>
                </table>  
            <div style="clear:both;"></div>
        </div>
    </section>
    <section class="section-padding-30"></section>
</div>
@endsection