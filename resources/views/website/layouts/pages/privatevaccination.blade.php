@extends('website.layouts.base')
@section('styling')
<style type="text/css">
    .row {
        margin-left: unset !important;
    }
    h2, .h2 {
        font-size: 1.2rem;
    }
</style>
@endsection
@section('content')
<div class="container">
    @include('website.layouts.logo')
    <section class="section-padding div_with_white_back">
        <div class="section-header">
            <div class="section-heading">
                <h3 class="text-custom-black fw-700">Private Vaccination</h3>
            </div>
        </div>
        <div class="row">
            <div class="col_12">
                <p>Some vaccinations are available on the NHS for certain groups of patients. However, these vaccinations can also be provided on a private basis if you fall outside of the NHS criteria. Please make an appointment in our private vaccination clinic at Woodhouse Medical Practice if you wish to have this done.</p>
                <div class="dropdown_section">
                    <h2>Our private vaccination clinic offers the following vaccinations</h2>
                    <div class="dropdown">
                        <h2 class="">Chickenpox vaccination</h2>
                        <div class="dropdown_content" style="display: none;">
                            <p><strong>Dose</strong></p>
                            <p>2 doses:</p>
                            <p>Given at 8-12 week intervals</p>
                            <p><strong>Cost</strong></p>
                            <p>£70 per dose</p>
                            <p><strong>Further information</strong></p>
                            <p>Chickenpox disease:&nbsp;<a href="http://www.nhs.uk/conditions/Chickenpox/Pages/Introduction.aspx">http://www.nhs.uk/conditions/Chickenpox/Pages/Introduction.aspx</a></p>
                            <p>GSK Information:<a href="http://www.spotchickenpox.com/">&nbsp;http://www.spotchickenpox.com</a></p>
                            <p>Chickenpox information from the US Department of Health:</p>
                            <p><a href="https://www.cdc.gov/chickenpox/index.html">https://www.cdc.gov/chickenpox/index.html</a></p>
                            <p>Chickenpox vaccine information:&nbsp;<a href="http://www.nhs.uk/Conditions/vaccinations/Pages/chickenpox-vaccine.aspx">http://www.nhs.uk/Conditions/vaccinations/Pages/chickenpox-vaccine.aspx</a></p>
                            <p>NHS eligibility:&nbsp;<a href="http://www.nhs.uk/Conditions/vaccinations/Pages/vaccination-schedule-age-checklist.aspx">http://www.nhs.uk/Conditions/vaccinations/Pages/vaccination-schedule-age-checklist.aspx</a></p>
                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>
                        </div>
                    </div>
                    <div class="dropdown">
                        <h2>
                        HPV vaccination                                 </h2>
                        <div class="dropdown_content">
                            <p><strong>Dose</strong></p>
                            <p>&gt;14 years:</p>
                            <p>3 doses at month 0, month 2 and month 6</p>
                            <p><strong>Cost</strong></p>
                            <p>Gardasil-4 £145 per dose&nbsp; Gardasil-9 £160 per dose</p>
                            <p><strong>Further information</strong></p>
                            <p>Cervical cancer:&nbsp;<a href="http://www.nhs.uk/Conditions/Cancer-of-the-cervix/Pages/Introduction.aspx">http://www.nhs.uk/Conditions/Cancer-of-the-cervix/Pages/Introduction.aspx</a></p>
                            <p>HPV vaccine information:&nbsp;<a href="http://www.nhs.uk/Conditions/vaccinations/Pages/hpv-human-papillomavirus-vaccine.aspx">http://www.nhs.uk/Conditions/vaccinations/Pages/hpv-human-papillomavirus-vaccine.aspx</a></p>
                            <p>NHS eligibility:&nbsp;<a href="http://www.nhs.uk/Conditions/vaccinations/Pages/vaccination-schedule-age-checklist.aspx">http://www.nhs.uk/Conditions/vaccinations/Pages/vaccination-schedule-age-checklist.aspx</a></p>
                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>
                        </div>
                    </div>
                    <div class="dropdown">
                        <h2>
                        Flu vaccination                                 </h2>
                        <div class="dropdown_content">
                            <p><strong>Dose</strong></p>
                            <p>&nbsp;</p>
                            <p>1 dose</p>
                            <p><strong>Cost</strong></p>
                            <p>£35</p>
                            <p><strong>Further information</strong></p>
                            <p>Flu:&nbsp;<a href="http://www.nhs.uk/conditions/flu/Pages/Introduction.aspx">http://www.nhs.uk/conditions/flu/Pages/Introduction.aspx</a></p>
                            <p>Flu vaccine information:&nbsp;<a href="http://www.nhs.uk/Conditions/vaccinations/Pages/flu-influenza-vaccine.aspx">http://www.nhs.uk/Conditions/vaccinations/Pages/flu-influenza-vaccine.aspx</a></p>
                            <p>NHS eligibility:&nbsp;<a href="http://www.nhs.uk/Conditions/vaccinations/Pages/flu-influenza-vaccine.aspx">http://www.nhs.uk/Conditions/vaccinations/Pages/flu-influenza-vaccine.aspx</a></p>
                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>
                        </div>
                    </div>
                    <div class="dropdown">
                        <h2>
                        Meningitis B vaccination</h2>
                        <div class="dropdown_content">
                            <p><strong>Dose</strong></p>
                            <p>&lt;2 years: 3 doses with 2 doses given at an 8-week interval and the third dose 1-2 years after</p>
                            <p>&gt;2 years: 2 doses with an 8-week interval</p>
                            <p><strong>Cost</strong></p>
                            <p>£135 per dose</p>
                            <p><strong>Further information</strong></p>
                            <p>Meningitis disease:&nbsp;<a href="http://www.nhs.uk/Conditions/Meningitis/Pages/Introduction.aspx">http://www.nhs.uk/Conditions/Meningitis/Pages/Introduction.aspx</a></p>
                            <p>Meningitis B vaccine information:&nbsp;<a href="http://www.nhs.uk/Conditions/vaccinations/Pages/meningitis-B-vaccine.aspx">http://www.nhs.uk/Conditions/vaccinations/Pages/meningitis-B-vaccine.aspx</a></p>
                            <p>NHS eligibility:&nbsp;<a href="http://www.nhs.uk/Conditions/vaccinations/Pages/vaccination-schedule-age-checklist.aspx">http://www.nhs.uk/Conditions/vaccinations/Pages/vaccination-schedule-age-checklist.aspx</a></p>
                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>
                        </div>
                    </div>
                    <div class="dropdown">
                        <h2>
                        Meningitis ACWY vaccination                                 </h2>
                        <div class="dropdown_content">
                            <p><strong>Dose</strong></p>
                            <p>Babies&lt;1 years: 2 doses given 1 month apart</p>
                            <p>Children and adults&gt;1year: 1 dose</p>
                            <p><strong>How long does immunity last?</strong></p>
                            <p>Minimum 5 years</p>
                            <p><strong>Who needs it?</strong></p>
                            <p>Those requiring the meningitis C vaccine for their baby. Two doses of the meningitis ACWY vaccine provides the same coverage as the single meningitis C vaccine previously&nbsp;given on the NHS.</p>
                            <p><strong>Cost</strong></p>
                            <p>£60 per dose&nbsp;– not available on the NHS outside the routine immunisation schedule</p>
                            <p><strong>Further information</strong></p>
                            <p>Meningitis disease:&nbsp;<a href="http://www.nhs.uk/Conditions/Meningitis/Pages/Introduction.aspx">http://www.nhs.uk/Conditions/Meningitis/Pages/Introduction.aspx</a></p>
                            <p>Meningitis ACWY vaccine information:&nbsp;<a href="http://www.nhs.uk/Conditions/vaccinations/Pages/men-acwy-vaccine.aspx">http://www.nhs.uk/Conditions/vaccinations/Pages/men-acwy-vaccine.aspx</a></p>
                            <p>NHS eligibility:&nbsp;<a href="http://www.nhs.uk/Conditions/vaccinations/Pages/vaccination-schedule-age-checklist.aspx">http://www.nhs.uk/Conditions/vaccinations/Pages/vaccination-schedule-age-checklist.aspx</a></p>
                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>
                        </div>
                    </div>
                    <div class="dropdown">
                        <h2>
                        Pneumonia vaccination                                   </h2>
                        <div class="dropdown_content">
                            <p><strong>Dose</strong></p>
                            <p>1 dose</p>
                            <p><strong>Cost</strong></p>
                            <p>£50 polysaccharide/£120 conjugate</p>
                            <p><strong>Further information</strong></p>
                            <p>Pneumonia disease:&nbsp;<a href="http://www.nhs.uk/conditions/pneumonia/Pages/Introduction.aspx">http://www.nhs.uk/conditions/pneumonia/Pages/Introduction.aspx</a></p>
                            <p>Pneumococcal vaccine information:&nbsp;<a href="http://www.medicines.org.uk/emc/PIL.17518.latest.pdf">http://www.medicines.org.uk/emc/PIL.17518.latest.pdf</a></p>
                            <p>Prevenar: http://www.medicines.org.uk/emc/medicine/22694</p>
                            <p>NHS eligibility:&nbsp;<a href="http://www.nhs.uk/Conditions/vaccinations/Pages/vaccination-schedule-age-checklist.aspx">http://www.nhs.uk/Conditions/vaccinations/Pages/vaccination-schedule-age-checklist.aspx</a></p>
                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>
                        </div>
                    </div>
                    <div class="dropdown">
                        <h2>
                        Shingles vaccination                                    </h2>
                        <div class="dropdown_content">
                            <p><strong>Dose</strong></p>
                            <p>1 dose</p>
                            <p><strong>Cost</strong></p>
                            <p>£175</p>
                            <p><strong>Further information</strong></p>
                            <p>Shingles disease:&nbsp;<a href="http://www.nhs.uk/Conditions/Shingles/Pages/Symptoms.aspx">http://www.nhs.uk/Conditions/Shingles/Pages/Symptoms.aspx</a></p>
                            <p>Shingles vaccine information:&nbsp;<a href="http://www.nhs.uk/Conditions/vaccinations/Pages/shingles-vaccination.aspx">http://www.nhs.uk/Conditions/vaccinations/Pages/shingles-vaccination.aspx</a></p>
                            <p>NHS eligibility:&nbsp;<a href="http://www.nhs.uk/Conditions/vaccinations/Pages/vaccination-schedule-age-checklist.aspx">http://www.nhs.uk/Conditions/vaccinations/Pages/vaccination-schedule-age-checklist.aspx</a></p>
                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="col_4 last sidebar">
                <div class="blue-box">
                    <img src="https://www.sfvaccinations.co.uk/wp-content/uploads/2016/05/download.png" style="float:right;margin-top:10px;">

                    <h2>
                        <a href="https://www.sfvaccinations.co.uk/wp-content/uploads/2021/02/Pricelist-May-2020-WV.pdf">
                        Download full service and price list                                        </a>
                    </h2>
                </div>
                <div class="blue-box">
                    <img src="https://www.sfvaccinations.co.uk/wp-content/uploads/2016/05/location.png" style="float:right;margin-top:10px;">

                    <h2>
                        <a href="http://www.sfvaccinations.co.uk/#opening">
                        Opening times                                       </a>
                    </h2>
                </div>
                <div class="blue-box">
                    <img src="https://www.sfvaccinations.co.uk/wp-content/uploads/2016/05/location.png" style="float:right;margin-top:10px;">

                    <h2>
                        <a href="http://www.sfvaccinations.co.uk/#contact">
                        How to find us                                      </a>
                    </h2>
                </div>
                <div class="blue-box grey-box">
                    <h2>
                        <a href="">
                        How our vaccination clinic works</a>
                    </h2>
                    <ol>
                        <li>Consultation and risk assessment</li>
                        <li>Vaccinations provided</li>
                        <li>Follow-up appointment arranged&nbsp;if required</li>
                    </ol>
                </div>
                <div class="blue-box grey-box">
                    <h2>
                        <a href="">
                       You can book an appointment by</a>
                    </h2>
                    <ul>
                        <li>Phoning 0113 2213533 or 0113 8730242</li>
                        <li>Emailing info@sfvaccinations.co.uk</li>
                        <li>Booking online</li>
                    </ul>
                </div>
                <div class="blue-box grey-box">

                    <h2>
                        <a href="">
                        Useful websites                                     </a>
                    </h2>
                    <p>NHS Choices</p>
                    <p><a href="http://www.nhs.uk" target="_blank" rel="noopener noreferrer">www.nhs.uk</a></p>
                </div>
            </div> -->
            <div style="clear:both;"></div>
        </div>
    </section>
    <section class="section-padding-30"></section>
</div>
@endsection