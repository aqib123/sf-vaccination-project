@extends('website.layouts.base')

@section('styling')
<style type="text/css">.tdldots,.dot {font-family:'Aileron TDL Dot', Arial,Helvetica, sans-serif;font-weight: bold;font-style: normal;margin:0 2px;} .dota,.dotanew,.lavender {color:#993399;} .dotb,.dotbnew,.gold {color:#ff9900;} .dotc,.dotcnew,.light-blue {color:#0066ff;} .dotf,.dotfnew,.red {color:#ff0000;} .dotg,.dotgnew,.grey {color:#999999;} .doth,.dothnew,.green {color:#009900;} .dotk,.dotknew,.dark-blue {color:#003399;}
</style>
@endsection
@section('content')
<div class="container">
    @include('website.layouts.logo')
    <div class="tab-content padding-20 bg-custom-white bx-wrapper">
        <div class="section-header">
            <div class="section-heading">
                <!-- <h3 class="text-custom-black fw-700">Sample Requirements</h3>
                <div class="section-description">
                    <h5 class="text-custom-black fw-700">Vacutainer</h5>
                </div> -->
            </div>
        </div>
        <div class="tab-pane fade show active" id="start">
            <div class="tab-inner">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- Starting -->
                        <div class="padded-box simpleTextBox w-match-height">
                            <div class="w-rte simpleTextBoxContent">
                                <h2>{{$detail->test->names}}</h2>
                                <div class="row feature-sections content1Column" style="border-top: solid 1px #dddddd; padding-top:15px;">
                                    <div class="col-md-3">
                                        <p><span class="sonicDarkBlue"><strong>Code</strong></span></p>
                                    </div>
                                    <div class="col-md-8">
                                        {{ $detail->codes }}
                                    </div>
                                </div>
                                <div class="row feature-sections content1Column">
                                    <div class="col-md-3">
                                        <p><span class="sonicDarkBlue"><strong>Sample Reqs</strong></span></p>
                                    </div>
                                    <div class="col-md-8">
                                        {!! $detail->sample_requirements !!}
                                    </div>
                                </div>
                                <div class="row feature-sections content1Column">
                                    <div class="col-md-3">
                                        <p><span class="sonicDarkBlue"><strong>Turnaround</strong></span></p>
                                    </div>
                                    <div class="col-md-8">
                                        <p>{{ $detail->turnaround }}</p>
                                    </div>
                                </div>
                                <div class="row feature-sections content1Column">
                                    <div class="col-md-3">
                                        <p><span class="sonicDarkBlue"><strong>Special instructions</strong></span></p>
                                    </div>
                                    <div class="col-md-8">
                                        <p>{{ $detail->special_instructions }}</p>
                                    </div>
                                </div>
                                <p style="color:#666666;"><strong>Sample type guide</strong></p>
                                <div class="row feature-sections content1Column" style="border-top:solid 1px #cccccc;padding:8px 0 0 0;">
                                    <div class="col-xs-1">
                                        <p><span class="dot lavender">A</span></p>
                                    </div>
                                    <div class="col-xs-11">
                                        <p>Lavender Vacutainer, EDTA anticoagulant, 4ml/10ml (10ml EDTA tubes are used for specific PCR assays)</p>
                                    </div>
                                </div>
                                <div class="row feature-sections content1Column" style="border-top: solid 1px #dddddd; padding-top:30px;">
                                    <div class="col-md-12">
                                        <p>See the <a data-id="23118" href="/tests/sample-requirements/" target="_self" title="Sample requirements"><b>Sample Requirements</b></a> page for an explanation of all the sample requirements.</p>
                                        <p>See the <a data-id="23119" href="/tests/special-instructions/" target="_self" title="Special instructions"><b>Special Instructions</b></a> Legend page for a full list of special instructions.</p>
                                        <p>Last-updated: </p>
                                        <p>&nbsp;</p>
                                        <p><span class="btn-custom-3"><a class="btn btn-warning" data-id="25579" href="/tests/tests-a-z/tests-a/" target="_self" title="Test library">Back to test list</a></span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Enfing here -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="section-padding-30">
    </section>
</div>
@endsection