@extends('website.layouts.base')
@section('content')
<input type="hidden" value="PK" id='hiddenchekuae'>	<div id="content" style="width: 100%;">
				<div id="post-1441" class="post-1441 page type-page status-publish hentry">
			<span class="entry-title" style="display: none;">Classroom Setup</span><span class="vcard" style="display: none;"><span class="fn"><a href="../index.html" title="Posts by admin" rel="author">admin</a></span></span><span class="updated" style="display:none;">2018-07-30T12:25:18+00:00</span>																		<div class="post-content">
				<div class="fusion-fullwidth fullwidth-box fusion-fullwidth-1  fusion-parallax-none nonhundred-percent-fullwidth remove-spacing" style="border-color:#eae9e9;border-bottom-width: 0px;border-top-width: 0px;border-bottom-style: solid;border-top-style: solid;padding-bottom:20px;padding-top:65px;padding-left:px;padding-right:px;"><style type="text/css" scoped="scoped">.fusion-fullwidth-1 {
                            padding-left: px !important;
                            padding-right: px !important;
                        }</style><div class="fusion-row"><div class="fusion-title title fusion-title-size-three custom_heading"><h3 class="title-heading-left">Online Classroom Setup</h3><div class="title-sep-container"><div class="title-sep sep-single"></div></div></div><div class="fusion-sep-clear"></div><div class="fusion-separator sep-single" style="border-color:#e0dede;border-top-width:1px;margin-left: auto;margin-right: auto;margin-top:px;width:100%;max-width:25%;"></div><div class="about-mts">
<ul>
<li><strong>MTS online classes have proved to be better than one on one tutoring</strong>
<ul>
<li>Every online tutoring class with MTS is of 1 hour and we use latest subscribed Skype with whiteboards for delivering high quality sessions.</li>
<li>Our tutors are professionally trained online lecturers who use specialized Software and hardware (laptops and tablets) to conduct sessions.</li>
<li>For students, MTS’s online learning environment is just like in-person tutoring where they can simultaneously see the tutor’s live video and his virtual notepad on their laptop screens.</li>
<li>The flexibility of preparing and sending documents/notes/presentations within a few minutes, gives MTS’s online learning environment a considerable edge over in-person tutoring.</li>
</ul>
</li>
<li><strong>Infrastructure that you need for online classes</strong>
<ul>
<li>You just need the following few items:
<li>A Decent Internet connection</li>
<li>Desktop or a Laptop</li>
<li>Latest Skype (You can download it from: <a href="http://www.skype.com/en/download-skype/skype-for-computer/">http://www.skype.com/en/download-skype/skype-for-computer/ )</a></li>
</ul>
</li>
</ul>
</div>
</div></div>
							</div>
																	</div>
					</div>
@endsection