@extends('website.layouts.base')
@section('content')
<input type="hidden" value="PK" id='hiddenchekuae'>	<div id="content" style="width: 100%;">
				<div id="post-1250" class="post-1250 page type-page status-publish hentry">
			<span class="entry-title" style="display: none;">Grade/Levels we offer</span><span class="vcard" style="display: none;"><span class="fn"><a href="../index.html" title="Posts by admin" rel="author">admin</a></span></span><span class="updated" style="display:none;">2018-07-30T12:24:36+00:00</span>																		<div class="post-content">
				<div class="fusion-fullwidth fullwidth-box fusion-fullwidth-1  fusion-parallax-none nonhundred-percent-fullwidth grade-main-image" style="border-color:#eae9e9;border-bottom-width: 0px;border-top-width: 0px;border-bottom-style: solid;border-top-style: solid;padding-bottom:0px;padding-top:40%;padding-left:0px;padding-right:0px;background-color:#f2f2f2;background-position:center bottom;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;-ms-filter: &quot;progid:DXImageTransform.Microsoft.AlphaImageLoader(src=&#039;https://www.mytutorsource.com/wp-content/uploads/2015/03/MTS_Grade_03.png&#039;, sizingMethod=&#039;scale&#039;)&quot;;background-image: url(https://www.mytutorsource.com/wp-content/uploads/2015/03/MTS_Grade_03.png);"><style type="text/css" scoped="scoped">.fusion-fullwidth-1 {
                            padding-left: 0px !important;
                            padding-right: 0px !important;
                        }</style><div class="fusion-row"></div></div><div class="fusion-fullwidth fullwidth-box fusion-fullwidth-2  fusion-parallax-none nonhundred-percent-fullwidth grade-level-steps" style="border-color:#eae9e9;border-bottom-width: 0px;border-top-width: 0px;border-bottom-style: solid;border-top-style: solid;padding-bottom:0px;padding-top:20px;padding-left:px;padding-right:px;"><style type="text/css" scoped="scoped">.fusion-fullwidth-2 {
                            padding-left: px !important;
                            padding-right: px !important;
                        }</style><div class="fusion-row"><div class="fusion-title title fusion-title-size-three custom_heading"><h3 class="title-heading-left">Expert Tutors for Junior Level, O/A Level, IB, Language Courses and Test Preparations</h3><div class="title-sep-container"><div class="title-sep sep-single"></div></div></div><div class="fusion-sep-clear"></div><div class="fusion-separator sep-single" style="border-color:#e0dede;border-top-width:1px;margin-left: auto;margin-right: auto;margin-top:px;margin-bottom:45px;width:100%;max-width:80%;"></div><div class="fusion-content-boxes content-boxes columns fusion-columns-1 fusion-content-boxes-1 content-boxes-icon-on-side row content-left grade-levels" style="margin-top:0px;margin-bottom:40px;"><style type="text/css" scoped="scoped">
			.fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .heading h2,
			.fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .heading .heading-link h2,
			.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .heading h2,
			.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .heading .heading-link h2,
			.fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more,
			.fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more::after,
			.fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more::before,
			.fusion-content-boxes-1 .fusion-content-box-hover .fusion-read-more:hover:after,
			.fusion-content-boxes-1 .fusion-content-box-hover .fusion-read-more:hover:before,
			.fusion-content-boxes-1 .fusion-content-box-hover .fusion-read-more:hover,
			.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more,
			.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more::after,
			.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more::before,			
			.fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .icon .circle-no,
			.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .icon .circle-no {
				color: #61bd98;
			}
			.fusion-content-boxes-1 .fusion-content-box-hover .heading-link:hover .icon i.circle-yes,
			.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box:hover .heading-link .icon i.circle-yes,
			.fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .heading .icon i.circle-yes,
			.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .heading .icon i.circle-yes {
				background-color: #61bd98 !important;
				border-color: #61bd98 !important;
			}</style><div class="fusion-column content-box-column content-box-column-1 col-lg-12 col-md-12 col-sm-12 fusion-content-box-hover content-box-column-last content-box-column-last-in-row"><div class="col content-wrapper link-area-link-icon icon-hover-animation-slide" style="background-color:transparent;"><div class="heading heading-with-icon icon-left"><div class="image"><img src="../../www.mytutorsource.com/wp-content/uploads/2015/11/1.html" width="110" height="110" alt="" /></div><h2 class="content-box-heading" style="font-size: 16px;line-height:18px;padding-left:130px;">Junior Level (3rd Grade and above):</h2></div><div class="fusion-clearfix"></div><div class="content-container" style="padding-left:130px;">  We know coping up with school curriculum can be tough for children. Not only for children, but even for their busy parents who need to deal with all their hard assignments and regular homework. If you’re in the same situation, don’t worry as MTS would come for rescue! MTS provides highly qualified professional educators for almost all the junior level subjects. Whether you need help in sciences, languages or even arts, we’ve got you covered!</div></div></div><div class="fusion-clearfix"></div><div class="fusion-clearfix"></div></div><div class="fusion-content-boxes content-boxes columns fusion-columns-1 fusion-content-boxes-2 content-boxes-icon-on-side row content-left grade-levels" style="margin-top:0px;margin-bottom:40px;"><style type="text/css" scoped="scoped">
			.fusion-content-boxes-2 .fusion-content-box-hover .link-area-link-icon-hover .heading h2,
			.fusion-content-boxes-2 .fusion-content-box-hover .link-area-link-icon-hover .heading .heading-link h2,
			.fusion-content-boxes-2 .fusion-content-box-hover .link-area-box-hover .heading h2,
			.fusion-content-boxes-2 .fusion-content-box-hover .link-area-box-hover .heading .heading-link h2,
			.fusion-content-boxes-2 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more,
			.fusion-content-boxes-2 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more::after,
			.fusion-content-boxes-2 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more::before,
			.fusion-content-boxes-2 .fusion-content-box-hover .fusion-read-more:hover:after,
			.fusion-content-boxes-2 .fusion-content-box-hover .fusion-read-more:hover:before,
			.fusion-content-boxes-2 .fusion-content-box-hover .fusion-read-more:hover,
			.fusion-content-boxes-2 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more,
			.fusion-content-boxes-2 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more::after,
			.fusion-content-boxes-2 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more::before,			
			.fusion-content-boxes-2 .fusion-content-box-hover .link-area-link-icon-hover .icon .circle-no,
			.fusion-content-boxes-2 .fusion-content-box-hover .link-area-box-hover .icon .circle-no {
				color: #61bd98;
			}
			.fusion-content-boxes-2 .fusion-content-box-hover .heading-link:hover .icon i.circle-yes,
			.fusion-content-boxes-2 .fusion-content-box-hover .link-area-box:hover .heading-link .icon i.circle-yes,
			.fusion-content-boxes-2 .fusion-content-box-hover .link-area-link-icon-hover .heading .icon i.circle-yes,
			.fusion-content-boxes-2 .fusion-content-box-hover .link-area-box-hover .heading .icon i.circle-yes {
				background-color: #61bd98 !important;
				border-color: #61bd98 !important;
			}</style><div class="fusion-column content-box-column content-box-column-1 col-lg-12 col-md-12 col-sm-12 fusion-content-box-hover content-box-column-last content-box-column-last-in-row"><div class="col content-wrapper link-area-link-icon icon-hover-animation-slide" style="background-color:transparent;"><div class="heading heading-with-icon icon-left"><div class="image"><img src="../../www.mytutorsource.com/wp-content/uploads/2015/11/3.html" width="110" height="110" alt="" /></div><h2 class="content-box-heading" style="font-size: 16px;line-height:18px;padding-left:130px;">GCSE / IGCSE / GCE (O-Level and A-Level):</h2></div><div class="fusion-clearfix"></div><div class="content-container" style="padding-left:130px;">  These are the building up years for students and you would be giving more exams than ever before and let’s face it, it can actually get stressful and tiring. But not to worry, as MTS’s professionally trained lecturers would make your life easy! You name any subject (of all the major academic boards e.g. Edexcel, Cambridge and AQA etc.) and MTS would track down the best teacher for you.</div></div></div><div class="fusion-clearfix"></div><div class="fusion-clearfix"></div></div><div class="fusion-content-boxes content-boxes columns fusion-columns-1 fusion-content-boxes-3 content-boxes-icon-on-side row content-left grade-levels" style="margin-top:0px;margin-bottom:40px;"><style type="text/css" scoped="scoped">
			.fusion-content-boxes-3 .fusion-content-box-hover .link-area-link-icon-hover .heading h2,
			.fusion-content-boxes-3 .fusion-content-box-hover .link-area-link-icon-hover .heading .heading-link h2,
			.fusion-content-boxes-3 .fusion-content-box-hover .link-area-box-hover .heading h2,
			.fusion-content-boxes-3 .fusion-content-box-hover .link-area-box-hover .heading .heading-link h2,
			.fusion-content-boxes-3 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more,
			.fusion-content-boxes-3 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more::after,
			.fusion-content-boxes-3 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more::before,
			.fusion-content-boxes-3 .fusion-content-box-hover .fusion-read-more:hover:after,
			.fusion-content-boxes-3 .fusion-content-box-hover .fusion-read-more:hover:before,
			.fusion-content-boxes-3 .fusion-content-box-hover .fusion-read-more:hover,
			.fusion-content-boxes-3 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more,
			.fusion-content-boxes-3 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more::after,
			.fusion-content-boxes-3 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more::before,			
			.fusion-content-boxes-3 .fusion-content-box-hover .link-area-link-icon-hover .icon .circle-no,
			.fusion-content-boxes-3 .fusion-content-box-hover .link-area-box-hover .icon .circle-no {
				color: #61bd98;
			}
			.fusion-content-boxes-3 .fusion-content-box-hover .heading-link:hover .icon i.circle-yes,
			.fusion-content-boxes-3 .fusion-content-box-hover .link-area-box:hover .heading-link .icon i.circle-yes,
			.fusion-content-boxes-3 .fusion-content-box-hover .link-area-link-icon-hover .heading .icon i.circle-yes,
			.fusion-content-boxes-3 .fusion-content-box-hover .link-area-box-hover .heading .icon i.circle-yes {
				background-color: #61bd98 !important;
				border-color: #61bd98 !important;
			}</style><div class="fusion-column content-box-column content-box-column-1 col-lg-12 col-md-12 col-sm-12 fusion-content-box-hover content-box-column-last content-box-column-last-in-row"><div class="col content-wrapper link-area-link-icon icon-hover-animation-slide" style="background-color:transparent;"><div class="heading heading-with-icon icon-left"><div class="image"><img src="../../www.mytutorsource.com/wp-content/uploads/2015/11/21.html" width="110" height="110" alt="" /></div><h2 class="content-box-heading" style="font-size: 16px;line-height:18px;padding-left:130px;">Language Courses:</h2></div><div class="fusion-clearfix"></div><div class="content-container" style="padding-left:130px;">  MTS provides comprehensive language learning courses to students and even adults. Whether you want to improve your speaking skills, want to be good at understanding a language or simply want to start from scratch, MTS is the best solution for you. We have professional educators for all the major languages e.g English, German, French, Urdu and Chinese etc.</div></div></div><div class="fusion-clearfix"></div><div class="fusion-clearfix"></div></div><div class="fusion-content-boxes content-boxes columns fusion-columns-1 fusion-content-boxes-4 content-boxes-icon-on-side row content-left grade-levels" style="margin-top:0px;margin-bottom:40px;"><style type="text/css" scoped="scoped">
			.fusion-content-boxes-4 .fusion-content-box-hover .link-area-link-icon-hover .heading h2,
			.fusion-content-boxes-4 .fusion-content-box-hover .link-area-link-icon-hover .heading .heading-link h2,
			.fusion-content-boxes-4 .fusion-content-box-hover .link-area-box-hover .heading h2,
			.fusion-content-boxes-4 .fusion-content-box-hover .link-area-box-hover .heading .heading-link h2,
			.fusion-content-boxes-4 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more,
			.fusion-content-boxes-4 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more::after,
			.fusion-content-boxes-4 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more::before,
			.fusion-content-boxes-4 .fusion-content-box-hover .fusion-read-more:hover:after,
			.fusion-content-boxes-4 .fusion-content-box-hover .fusion-read-more:hover:before,
			.fusion-content-boxes-4 .fusion-content-box-hover .fusion-read-more:hover,
			.fusion-content-boxes-4 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more,
			.fusion-content-boxes-4 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more::after,
			.fusion-content-boxes-4 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more::before,			
			.fusion-content-boxes-4 .fusion-content-box-hover .link-area-link-icon-hover .icon .circle-no,
			.fusion-content-boxes-4 .fusion-content-box-hover .link-area-box-hover .icon .circle-no {
				color: #61bd98;
			}
			.fusion-content-boxes-4 .fusion-content-box-hover .heading-link:hover .icon i.circle-yes,
			.fusion-content-boxes-4 .fusion-content-box-hover .link-area-box:hover .heading-link .icon i.circle-yes,
			.fusion-content-boxes-4 .fusion-content-box-hover .link-area-link-icon-hover .heading .icon i.circle-yes,
			.fusion-content-boxes-4 .fusion-content-box-hover .link-area-box-hover .heading .icon i.circle-yes {
				background-color: #61bd98 !important;
				border-color: #61bd98 !important;
			}</style><div class="fusion-column content-box-column content-box-column-1 col-lg-12 col-md-12 col-sm-12 fusion-content-box-hover content-box-column-last content-box-column-last-in-row"><div class="col content-wrapper link-area-link-icon icon-hover-animation-slide" style="background-color:transparent;"><div class="heading heading-with-icon icon-left"><div class="image"><img src="../../www.mytutorsource.com/wp-content/uploads/2015/11/4.html" width="110" height="110" alt="" /></div><h2 class="content-box-heading" style="font-size: 16px;line-height:18px;padding-left:130px;">Test Preparation:</h2></div><div class="fusion-clearfix"></div><div class="content-container" style="padding-left:130px;"> </p>
<p>Do you want to ace your SAT 1/2, TOEFL, IELTS or even GRE? We know that preparing alone for these tests can be hard. Our teachers will go through the entire course with you, they will tell you the best strategies to attempt the test and they will also make sure that you go through at least 5-6 complete sample tests before you appear for the exam. So don’t worry, it’s our responsibility!</p>
<p>Click on <a href="https://www.mytutorsource.com/get-started-for-free/">“Online Trial (Free!)“</a><span class="uae_cust_private_link"> or <a href="https://www.mytutorsource.com/high-quality-private-online-tutoring/">“Private Tutoring”</a></span> link and get introduced to your personal tutor for FREE!</p>
<p style="font-weight: 400;" class='custom_home_tutor_adword'>If you’re looking for a home tutor, please click on <a href="../high-quality-home-tutoring/index.html">&#8220;Home Tutoring&#8221;</a></p>
</div></div></div><div class="fusion-clearfix"></div><div class="fusion-clearfix"></div></div></div></div>
							</div>
																	</div>
					</div>
@endsection