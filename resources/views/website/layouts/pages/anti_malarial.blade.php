@extends('website.layouts.base')
@section('styling')
<style type="text/css">
    h2, .h2 {
        font-size: 1.2rem;
    }
</style>
@endsection
@section('content')
<div class="container">
    @include('website.layouts.logo')
    <section class="section-padding div_with_white_back">
        <div class="section-header">
            <div class="section-heading">
                <h3 class="text-custom-black fw-700">Anti malarial</h3>
            </div>
        </div>
        <div class="row" style="margin-left: unset !important;">
                <table class="table table-responsive-sm">
                    <thead>
                        <tr>
                            <th>Anti malarials</th>
                            <th>Dose</th>
                            <th>How long before travelling to the malaria area do I start taking it?</th>
                            <th>How long after leaving the malaria area do I stop taking it?</th>
                            <th>Key features</th>
                            <th>Cost</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Atovaquone/Proguanil</td>
                            <td>Once daily</td>
                            <td>1 day</td>
                            <td>1 week</td>
                            <td>
                                <ul>
                                    <li>Side effects are usually mild</li>
                                    <li>Taken for the shortest period</li>
                                </ul>
                            </td>
                            <td>£30 for 12 tablets</td>
                        </tr>
                        <tr>
                            <td>Doxycycline</td>
                            <td>Once daily</td>
                            <td>1 day</td>
                            <td>4 weeks</td>
                            <td>
                                <ul>
                                    <li>Side effects are usually mild</li>
                                    <li>Cheap</li>
                                </ul>
                            </td>
                            <td>£25 for 50 capsules</td>
                        </tr>
                        <tr>
                            <td>Mefloquine</td>
                            <td>Once weekly</td>
                            <td>2 weeks</td>
                            <td>4 weeks</td>
                            <td>
                                <ul>
                                    <li>Weekly dose</li>
                                    <li>Not suitable for those with mental health conditions</li>
                                </ul>
                            </td>
                            <td>£35 for 8 tablets</td>
                        </tr>
                        <tr>
                            <td>Chloroquine (can be taken in combination with proguanil for some areas)</td>
                            <td>Once weekly</td>
                            <td>2 weeks</td>
                            <td>4 weeks</td>
                            <td>
                                <ul>
                                    <li>Cheap</li>
                                    <li>Weekly dose</li>
                                    <li>Widespread resistance</li>
                                </ul>
                            </td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>  
            <div style="clear:both;"></div>
        </div>
    </section>
    <section class="section-padding-30"></section>
</div>
@endsection