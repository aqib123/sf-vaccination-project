@extends('website.layouts.base')
@section('content')
<div class="container">
    @include('website.layouts.logo')
    <section class="section-padding bg-light-white contact-us">
        <div class="row">
            <div class="col-xl-5 col-lg-4 mb-md-80">
                <div class="contact-form full-height align-self-center bx-wrapper bg-custom-white">
                    <h4 class="text-custom-black fw-600">Contact Us</h4>
                    <!-- <p class="text-light-white no-margin">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p> -->
                    <div class="contact-details">
                                <ul class="custom">
                                    <li>
                                        <i class="fas fa-phone-alt"></i>: 
                                        <a href="tel:">0113 221 3533</a>
                                    </li>
                                    <li>
                                        <i class="fas fa-phone-alt"></i>: 
                                        <a href="tel:">0113 873 0242</a>
                                    </li>
                                    <li>
                                        <i class="fas fa-mobile-alt"></i>: 
                                        <a href="tel:">07597028245</a>
                                    </li>
                                    <li>
                                        <i class="fas fa-envelope"></i> :
                                        <a href="mailto:">info@sfvaccinations.co.uk</a>
                                    </li>
                                    <li>
                                        <i class="fas fa-map-marker-alt"></i> : 
                                        <a href="#">Woodhouse medical practice<br>Cambridge road<br>Leeds<br>LS6 2SF</a>
                                    </li>
                                    <li>
                                        <i class="far fa-clock"></i> : 
                                        <a href="#"> Mon - Fri 08:00 - 18:00</a>
                                    </li>
                                </ul>
                            </div>
                </div>
            </div>
            <div class="col-xl-7 col-lg-8"><iframe width="100%" height="600" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=en&amp;q=Woodhouse%20medical%20practice%20Cambridge%20road%20Leeds%20LS6%202SF+(SF%20Vaccination%20Center)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"><a href="http://www.gps.ie/">vehicle gps</a></iframe>
            </div>
        </div>
    </section>
</div>
@endsection