@extends('website.layouts.base')
@section('content')
<div class="container">
    @include('website.layouts.logo')
    <section class="section-padding div_with_white_back">
        <div class="section-header">
            <div class="section-heading">
                <h3 class="text-custom-black fw-700">Corporate Services</h3>
            </div>
        </div>
        <div class="row">
            <article class="col-lg-4 col-md-6 post mb-xl-30">
                <div class="post-wrapper">
                    <div class="blog-img animate-img">
                        <a href="{{url('onsite_vaccination_clinic')}}">
                            <img src="{{ url('sfvaccination/images/onsite_vaccination_clinic.jpeg') }}" class="full-width" alt="blog" style="width:100%;height: 230px;">
                        </a>
                    </div>
                    <div class="blog-meta padding-20">
                        <div class="post-content">
                            <h4><a href="{{url('onsite_vaccination_clinic')}}" class="text-light-green fw-600">On-site vaccination clinic</a></h4>
                        </div>
                    </div>
                </div>
            </article>
            <article class="col-lg-4 col-md-6 post mb-xl-30">
                <div class="post-wrapper">
                    <div class="blog-img animate-img">
                        <a href="{{ url('onsite_testing_service') }}">
                            <img src="{{ url('sfvaccination/images/onsite_testing_services.jpeg') }}" class="full-width" alt="blog" style="width:100%;height: 230px;">
                        </a>
                    </div>
                    <div class="blog-meta padding-20">
                        <div class="post-content">
                            <h4><a href="{{ url('onsite_testing_service') }}" class="text-light-green fw-600">On-site testing services</a></h4>
                        </div>
                    </div>
                </div>
            </article>
            <article class="col-lg-4 col-md-6 post mb-xl-30">
                <div class="post-wrapper">
                    <div class="blog-img animate-img">
                        <a href="{{ url('employee_health') }}">
                            <img src="{{ url('sfvaccination/images/employee_health.jpeg') }}" class="full-width" alt="blog" style="width:100%;height: 230px;">
                        </a>
                    </div>
                    <div class="blog-meta padding-20">
                        <div class="post-content">
                            <h4><a href="{{ url('employee_health') }}" class="text-light-green fw-600">Employee health and wellbeing checks</a></h4>
                        </div>
                    </div>
                </div>
            </article>
            <div style="clear:both;"></div>
        </div>
    </section>
    <section class="section-padding-30"></section>
</div>
@endsection