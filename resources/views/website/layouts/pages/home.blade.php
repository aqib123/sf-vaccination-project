@extends('website.layouts.base')
@section('content')
<!-- <section class="genmed-intro"> -->
<!-- Section One -->
<section>
    <div class="container">
        @include('website.layouts.logo')
        <div class="section-header">
                <!-- <div class="section-heading"> -->
                    <!-- <h5 class="text-custom-black fw-700"><i class="fa fa-info-circle" aria-hidden="true"></i> At SF Screening and Vaccination, we offer a range of private services including</h5> -->
                    <!-- <img src="{{ asset('sfvaccination/assets/images/title-icon.png') }}" class="mb-xl-20" alt="img"> -->
                <!-- </div> -->
        </div>
        <div class="row justify-content-center">
            <div class="col-md-4 no-padding">
                <div class="intro-box bg-light-blue full-height section-three">
                    <div class="intro-wrapper text-center">
                        <i class="flaticon-flag"></i>
                        <h4 class="text-custom-white fw-900">COVID-19 tests and services</h4>
                        <!-- <p class="text-custom-white fd">SF Screening and Vaccinations is a government-approved private provider of COVID-19 PCR swab and antibody testing</p> -->
                        <a href="{{ url('covid-19-service-and-tests') }}" class="btn-first btn-submit-black">Book your COVID test here</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 no-padding">
                <div class="intro-box bg-light-blue full-height section-one img-responsive">
                    <div class="intro-wrapper text-center">
                        <i class="flaticon-flag"></i>
                        <h4 class="text-custom-white fw-900">Travel clinic services</h4>
                        <!-- <p class="text-custom-white fd">In our private travel clinic, we provide private prescriptions and medication for your trip abroad.Please see our price list for a full list of services and prices</p> -->
                        <a href="{{url('travel/clinic')}}" class="btn-first btn-submit-black">Book your travel vaccines here</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 no-padding">
                <div class="intro-box bg-light-blue full-height section-two">
                    <div class="intro-wrapper text-center">
                        <i class="flaticon-flag"></i>
                        <h4 class="text-custom-white fw-900">Corporate services</h4>
                        <!-- <p class="text-custom-white fd">We can provide onsite vaccination clinics for travel, flu prevention and occupational health purposes and issues.</p> -->
                        <a href="{{url('corporate_service')}}" class="btn-first btn-submit-black">Visit our corporate page</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 no-padding">
                <div class="intro-box bg-light-blue full-height section-five">
                    <div class="intro-wrapper text-center">
                        <i class="flaticon-flag"></i>
                        <h4 class="text-custom-white fw-900">Private vaccination</h4>
                        <!-- <p class="text-custom-white fd">Some vaccinations are available on the NHS for certain groups of patients. However, these vaccinations can also be provided on a private ...</p> -->
                        <a href="{{ url('private-vaccinations') }}" class="btn-first btn-submit-black">Book your private vaccination</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 no-padding">
                <div class="intro-box bg-light-blue full-height section-six">
                    <div class="intro-wrapper text-center">
                        <i class="flaticon-flag"></i>
                        <h4 class="text-custom-white fw-900">Private blood tests</h4>
                        <!-- <p class="text-custom-white fd">Some vaccinations are available on the NHS for certain groups of patients. However, these vaccinations can also be provided on a private ...</p> -->
                        <a href="{{ url('tests/tests-a-z/A') }}" class="btn-first btn-submit-black">Book your private blood</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 no-padding">
                <div class="intro-box bg-light-blue full-height section-seven">
                    <div class="intro-wrapper text-center">
                        <i class="flaticon-flag"></i>
                        <h4 class="text-custom-white fw-900">Occupational health services</h4>
                        <!-- <p class="text-custom-white fd">Hyperhidrosis (excessive sweating) is sweating that is more than required to maintain normal heat regulation within the body.</p> -->
                        <a href="{{url('occupational-health-services')}}" class="btn-first btn-submit-black">Book appointment</a>
                    </div>
                </div>
            </div>
        </div>
        <section class="section-padding">
            <div class="container">
                <div class="row">
                    <div class="col_6 blue-box grey-box"><a name="opening"></a>
                        <h2>Opening hours</h2>

                        <table border="0" width="94%">
                            <tbody>
                                    <tr>
                                        <td>Monday to Friday</td>
                                        <td> 8:30 am - 6:00 pm</td>
                                    </tr>

                                    <tr>
                                        <td>Saturday</td>
                                        <td> Emergency appointments only</td>
                                    </tr>

                                    <tr>
                                        <td>Sunday</td>
                                        <td> Emergency appointments only</td>
                                    </tr>
                                </tbody>
                        </table>
                    </div>
                    <div class="col_6 last">
                        <div class="blue-box grey-box">
                            <a name="contact"></a><h2>Location</h2>
                            <div class="col_12 last">
                               <!--  <img src="https://www.sfvaccinations.co.uk/wp-content/themes/sfsv/images/location-grey.png"> -->
                                <h5>SF screening and vaccination service</h5>
                                <p>Woodhouse medical practice<br>
                                Cambridge road<br>
                                Leeds<br> 
                                LS6 2SF</p>
                                <!-- <img src="https://www.sfvaccinations.co.uk/wp-content/themes/sfsv/images/phone-grey.png"> -->
                                <p><span class="fa fa-phone mr-3"></span> <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a></p>
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                        <!-- <div class="blue-box">
                            <h2><a href="https://www.sfvaccinations.co.uk/wp-content/uploads/2021/02/Pricelist-May-2020-WV.pdf" target="_blank">Download full service and price list <img src="https://www.sfvaccinations.co.uk/wp-content/themes/sfsv/images/download.png" style="vertical-align:text-bottom;margin-left:30px;"></a></h2>
                        </div> -->
                    </div>
                </div>
            </div>
        </section>
        <section class="section-padding appoint-testi-style-8 style-3 p-relative">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="testimonials-sec">
                            <div class="section-header-style-2">
                                <div class="section-heading text-center">
                                    <!-- <h6 class="text-light-white fw-600">Our Testimonials</h6> -->
                                    <h3 class="fw-700">Our testimonials</h3>
                                    <!-- <div class="section-description">
                                        <p class="text-light-white">
                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.
                                        </p>
                                    </div> -->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 no-padding">
                                    <div class="testimonials-slider-2 slider-arrow-layout-2">
                                        <div class="slide-item col-12">
                                            <div class="testimonials-inner">
                                                <div class="testimonials-img animate-img">
                                                    <img src="{{ url('img/background-imag-200_200x200.jpeg') }}" class="image-fit" alt="img">
                                                </div>
                                                <div class="testimonials-text bg-custom-white padding-20">
                                                    <h5><a href="#" class="text-custom-black fw-600">Jhon Anderson</a></h5>
                                                    <div class="ratings mb-xl-20">
                                                        <span class="fs-14 text-custom-yellow"><i class="fas fa-star"></i></span>
                                                        <span class="fs-14 text-custom-yellow"><i class="fas fa-star"></i></span>
                                                        <span class="fs-14 text-custom-yellow"><i class="fas fa-star"></i></span>
                                                        <span class="fs-14 text-light-white"><i class="fas fa-star"></i></span>
                                                        <span class="fs-14 text-light-white"><i class="fas fa-star"></i></span>
                                                    </div>
                                                    <p class="text-light-white no-margin">
                                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slide-item col-12">
                                            <div class="testimonials-inner">
                                                <div class="testimonials-img animate-img">
                                                    <img src="{{ url('img/background-imag-200_200x200.jpeg') }}" class="image-fit" alt="img">
                                                </div>
                                                <div class="testimonials-text bg-custom-white padding-20">
                                                    <h5><a href="#" class="text-custom-black fw-600">Jhon Anderson</a></h5>
                                                    <div class="ratings mb-xl-20">
                                                        <span class="fs-14 text-custom-yellow"><i class="fas fa-star"></i></span>
                                                        <span class="fs-14 text-custom-yellow"><i class="fas fa-star"></i></span>
                                                        <span class="fs-14 text-custom-yellow"><i class="fas fa-star"></i></span>
                                                        <span class="fs-14 text-light-white"><i class="fas fa-star"></i></span>
                                                        <span class="fs-14 text-light-white"><i class="fas fa-star"></i></span>
                                                    </div>
                                                    <p class="text-light-white no-margin">
                                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slide-item col-12">
                                            <div class="testimonials-inner">
                                                <div class="testimonials-img animate-img">
                                                    <img src="{{ url('img/background-imag-200_200x200.jpeg') }}" class="image-fit" alt="img">
                                                </div>
                                                <div class="testimonials-text bg-custom-white padding-20">
                                                    <h5><a href="#" class="text-custom-black fw-600">Jhon Anderson</a></h5>
                                                    <div class="ratings mb-xl-20">
                                                        <span class="fs-14 text-custom-yellow"><i class="fas fa-star"></i></span>
                                                        <span class="fs-14 text-custom-yellow"><i class="fas fa-star"></i></span>
                                                        <span class="fs-14 text-custom-yellow"><i class="fas fa-star"></i></span>
                                                        <span class="fs-14 text-light-white"><i class="fas fa-star"></i></span>
                                                        <span class="fs-14 text-light-white"><i class="fas fa-star"></i></span>
                                                    </div>
                                                    <p class="text-light-white no-margin">
                                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</section>
<!-- Section Two -->

<!-- Section Three -->

<!-- Section Four -->
@endsection