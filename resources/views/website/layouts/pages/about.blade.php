@extends('website.layouts.base')
@section('content')
<input type="hidden" value="PK" id='hiddenchekuae'>	<div id="content" style="width: 100%;">
				<div id="post-1653" class="post-1653 page type-page status-publish hentry">
			<span class="entry-title" style="display: none;">About</span><span class="vcard" style="display: none;"><span class="fn"><a href="../index.html" title="Posts by admin" rel="author">admin</a></span></span><span class="updated" style="display:none;">2017-12-16T22:30:26+00:00</span>																		<div class="post-content">
				<div class="fusion-fullwidth fullwidth-box fusion-fullwidth-1  fusion-parallax-none nonhundred-percent-fullwidth remove-spacing" style="border-color:#eae9e9;border-bottom-width: 0px;border-top-width: 0px;border-bottom-style: solid;border-top-style: solid;padding-bottom:20px;padding-top:50px;padding-left:px;padding-right:px;"><style type="text/css" scoped="scoped">.fusion-fullwidth-1 {
                            padding-left: px !important;
                            padding-right: px !important;
                        }</style><div class="fusion-row"><div class="fusion-title title fusion-title-size-three custom_heading"><h3 class="title-heading-left">High quality and very Affordable Online Tutoring</h3><div class="title-sep-container"><div class="title-sep sep-single"></div></div></div><div class="fusion-sep-clear"></div><div class="fusion-separator sep-single" style="border-color:#e0dede;border-top-width:1px;margin-left: auto;margin-right: auto;margin-top:px;margin-bottom:30px;width:100%;max-width:60%;"></div><div class="about-mts">
<ul>
<li><strong>Introduction:</strong><br />
My Tutor Source (MTS) is a formidable international teaching platform providing online educational services in a number of regions, like England, Singapore, UAE, India and Pakistan etc. MTS provides highly qualified and experienced tutors for one on one online tutoring. The tutors are not some freelancers but expert professionals with tons of teaching experience; therefore MTS takes full responsibility of the tutoring sessions and makes sure that the delivery is highly beneficial and fun for the students.<br />
So, if you’re a parent, trying to find quality tutoring to take your child’s knowledge and concepts to the next level or you’re a student trying to ace your Grades, MTS is the best solution for you!</li>
<li><strong>Grades/Levels we offer:</strong><br />
We offer professional educators for Junior Level subjects, GCSE/IB/GCE subjects, language courses and test preparations (e.g. SAT1/2, TOEFL, IELTS and GRE etc.) Our expert tutors offer individual help so students can safely expect to meet their academic goals and achieve high grades!</li>
</ul>
</div>
</div></div>
							</div>
																	</div>
					</div>
@endsection