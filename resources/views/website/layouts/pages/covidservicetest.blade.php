@extends('website.layouts.base')
@section('content')
<div class="container">
    @include('website.layouts.logo')
    <div class="bg-image p-5 text-center shadow-1-strong rounded mb-5 text-white" style="background-image: url('https://mdbcdn.b-cdn.net/img/new/slides/003.jpg'); background-size:cover; width:100%;">
        <h1 class="mb-3 h2" style="color: #ffffff;">COVID 19 tests and services</h1>
    </div>
    <section class="tm-top-b uk-grid section-padding" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin="">
        <div class="uk-width-1-1 uk-width-medium-1-3 uk-row-first">
            <div class="uk-panel uk-panel-box widget_text" style="min-height: 735.062px;">
                <div style="margin-top: -20px; margin-left: -20px; margin-right: -20px;">
                    <a href="{{ url('covid-19-fit-to-fly') }}">
                        <img title="COVID19 PCR Testing for UK Departures" src="https://www.citydoc.org.uk/images/banners/covid-19-departure-testing1.jpg" alt="COVID19 Testing for Travellers Leaving the UK" class="lazyloaded" data-ll-status="loaded">
                        <noscript>
                        <img title="COVID19 PCR Testing for UK Departures" src="https://www.citydoc.org.uk/images/banners/covid-19-departure-testing1.jpg" alt="COVID19 Testing for Travellers Leaving the UK" /></noscript>
                    </a>
                </div>
                <h3 style="font-weight: 500;">UK Departures<br>
                    <span class="pink">COVID 19 PCR Testing</span>
                </h3>
                <ul class="uk-list uk-list-striped">
                    <li><i class="uk-icon-check-square-o"></i> The most widely accepted PCR testing option for international travel &amp; UK flight departures</li>
                    <li><i class="uk-icon-check-square-o"></i> Results available as <strong>“Border Entry Report”</strong></li>
                    <li><i class="uk-icon-check-square-o"></i> Tests for active cases of Coronavirus</li>
                    <li><i class="uk-icon-check-square-o"></i> ISO and CQC accredited testing</li>
                    <li><i class="uk-icon-check-square-o"></i> 99.9% accuracy with correct technique using our accredited UK laboratories</li>
                    <li>
                        <div class="pink" style="font-size: 17px;">
                            <i class="uk-icon-check-square-o"></i> <strong>In-Clinic Testing only £175</strong>
                        </div>
                        <p>Testing available in Central London.</p>
                    </li>
                </ul>
                <p><a class="uk-button uk-button-default uk-button-large uk-align-center blue-button" href="https://www.citydoc.org.uk/covid-19-fit-to-fly/"><strong>Book an appointment online</strong></a></p>
                <div class="col_12 last covid">
                    <p><span class="fa fa-envelope mr-3"></span> <a href="#">info@sfvaccinations.co.uk</a></p>
                    <p><span class="fa fa-phone mr-3"></span> <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a></p>
                </div>
            </div>
        </div>
        <div class="uk-width-1-1 uk-width-medium-1-3">
            <div class="uk-panel uk-panel-box widget_text" style="min-height: 735.062px;">
                <div style="margin-top: -20px; margin-left: -20px; margin-right: -20px;">
                    <a href="{{ url('covid-19-international-arrivals-testing-day-2-and-8') }}">
                        <img title="COVID19 Testing for Arrivals into the UK" src="https://www.citydoc.org.uk/images/banners/covid-19-arrivals-testing1.jpg" alt="COVID19 Testing for Arrivals into the UK" class="lazyloaded" data-ll-status="loaded">
                        <noscript>
                            <img title="COVID19 Testing for Arrivals into the UK" src="https://www.citydoc.org.uk/images/banners/covid-19-arrivals-testing1.jpg" alt="COVID19 Testing for Arrivals into the UK" />
                        </noscript>
                    </a>
                </div>
                <h3 style="font-weight: 500;">UK International Arrivals<br>
                    <span class="pink">COVID19 2 &amp; 8 Day Tests</span>
                </h3>
                <ul class="uk-list uk-list-striped">
                    <li><i class="uk-icon-check-square-o"></i> Recognised and trusted Provider appointed by the UK governments Departments of Health and Social Care</li>
                    <li><i class="uk-icon-check-square-o"></i> Essential <strong>“Day 2 &amp; Day 8”</strong> testing for international arrivals</li>
                    <li><i class="uk-icon-check-square-o"></i> Tests for active cases of Coronavirus</li>
                    <li>
                        <div class="pink" style="font-size: 17px;"><i class="uk-icon-check-square-o"></i> <strong>Buy Online £160</strong>
                        </div>
                        <p>Nationwide Home-Testing service available.</p>
                    </li>
                    <li>
                        <div class="pink" style="font-size: 17px;"><i class="uk-icon-check-square-o"></i> <strong>In-Clinic Testing only £300</strong></div>
                        <p>Only available two working days before UK arrival.</p>
                    </li>
                </ul>
                <p><a class="uk-button uk-button-default uk-button-large uk-align-center blue-button" href="{{ url('covid-19-international-arrivals-testing-day-2-and-8') }}"><strong>Book an appointment online</strong></a></p>
                <div class="col_12 last covid">
                    <p><span class="fa fa-envelope mr-3"></span> <a href="#">info@sfvaccinations.co.uk</a></p>
                    <p><span class="fa fa-phone mr-3"></span> <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a></p>
                </div>
            </div>
        </div>
        <div class="uk-width-1-1 uk-width-medium-1-3">
            <div class="uk-panel uk-panel-box widget_text" style="min-height: 735.062px;">
                <div style="margin-top: -20px; margin-left: -20px; margin-right: -20px;">
                    <a href="https://www.citydoc.org.uk/test-to-release-covid-19-testing/">
                        <img title="COVID19 Testing for Arrivals into the UK" src="https://www.citydoc.org.uk/images/banners/covid-19-arrivals-testing1.jpg" alt="COVID19 Testing for Arrivals into the UK" class="lazyloaded" data-ll-status="loaded">
                        <noscript>
                            <img title="COVID19 Testing for Arrivals into the UK" src="https://www.citydoc.org.uk/images/banners/covid-19-arrivals-testing1.jpg" alt="COVID19 Testing for Arrivals into the UK" />
                        </noscript>
                    </a>
                </div>
                <h3 style="font-weight: 500;">UK International Arrivals<br>
                    <span class="pink">COVID19 Test-to-Release</span>
                </h3>
                <ul class="uk-list uk-list-striped">
                    <li><i class="uk-icon-check-square-o"></i> Recognised and trusted Test-to-Release Provider appointed by the UK governments Departments of Health and Social Care</li>
                    <li><i class="uk-icon-check-square-o"></i> Optional <strong>“Test-to-Release”</strong> testing available to reduce self-isolation period</li>
                    <li><i class="uk-icon-check-square-o"></i> Tests for active cases of Coronavirus</li>
                    <li><i class="uk-icon-check-square-o"></i> ISO and CQC accredited testing</li>
                    <li><i class="uk-icon-check-square-o"></i> 99.9% accuracy with correct technique using our accredited UK laboratories</li>
                    <li>
                        <div class="pink" style="font-size: 17px;"><i class="uk-icon-check-square-o"></i> <strong>In-Clinic Testing only £175</strong>
                        </div>
                        <p>Testing available in Central London.</p>
                    </li>
                </ul>
                <p><a class="uk-button uk-button-default uk-button-large uk-align-center blue-button" href="https://www.citydoc.org.uk/test-to-release-covid-19-testing/"><strong>Book an appointment online</strong></a></p>
                <div class="col_12 last covid">
                    <p><span class="fa fa-envelope mr-3"></span> <a href="#">info@sfvaccinations.co.uk</a></p>
                    <p><span class="fa fa-phone mr-3"></span> <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a></p>
                </div>
            </div>
        </div>
    </section>
    <!-- Section Two -->
    <div class="tm-middle uk-grid" data-uk-grid-match="" data-uk-grid-margin="">
        <div class="tm-main uk-width-medium-1-1 uk-row-first">
            <section class="tm-main-top uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin="">
                <div class="uk-width-1-1 uk-width-medium-1-3 uk-row-first">
                    <div class="uk-panel uk-panel-box widget_text" style="min-height: 735.062px;">
                        <div style="margin-top: -20px; margin-left: -20px; margin-right: -20px;">
                            <a href="https://www.citydoc.org.uk/test-to-release-covid-19-testing/">
                                <img title="COVID19 Testing for Arrivals into the UK" src="https://www.citydoc.org.uk/images/banners/covid-19-arrivals-testing1.jpg" alt="COVID19 Testing for Arrivals into the UK" class="lazyloaded" data-ll-status="loaded">
                                <noscript>
                                    <img title="COVID19 Testing for Arrivals into the UK" src="https://www.citydoc.org.uk/images/banners/covid-19-arrivals-testing1.jpg" alt="COVID19 Testing for Arrivals into the UK" />
                                </noscript>
                            </a>
                        </div>
                        <h3 style="font-weight: 500;">UK International Arrivals<br>
                            <span class="pink">COVID19 Test-to-Release</span>
                        </h3>
                        <ul class="uk-list uk-list-striped">
                            <li><i class="uk-icon-check-square-o"></i> Recognised and trusted Test-to-Release Provider appointed by the UK governments Departments of Health and Social Care</li>
                            <li><i class="uk-icon-check-square-o"></i> Optional <strong>“Test-to-Release”</strong> testing available to reduce self-isolation period</li>
                            <li><i class="uk-icon-check-square-o"></i> Tests for active cases of Coronavirus</li>
                            <li><i class="uk-icon-check-square-o"></i> ISO and CQC accredited testing</li>
                            <li><i class="uk-icon-check-square-o"></i> 99.9% accuracy with correct technique using our accredited UK laboratories</li>
                            <li>
                                <div class="pink" style="font-size: 17px;">
                                    <i class="uk-icon-check-square-o"></i> 
                                    <strong>In-Clinic Testing only £175</strong>
                                </div>
                                <p>Testing available in Central London.</p>
                            </li>
                        </ul>
                        <p>
                            <a class="uk-button uk-button-default uk-button-large uk-align-center blue-button" href="https://www.citydoc.org.uk/test-to-release-covid-19-testing/">
                                <strong>Book an appointment online</strong>
                            </a>
                        </p>
                        <div class="col_12 last covid">
                            <p><span class="fa fa-envelope mr-3"></span> <a href="#">info@sfvaccinations.co.uk</a></p>
                            <p><span class="fa fa-phone mr-3"></span> <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a></p>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-1 uk-width-medium-1-3">
                    <div class="uk-panel uk-panel-box widget_text" style="min-height: 735.062px;">
                        <div style="margin-top: -20px; margin-left: -20px; margin-right: -20px;">
                            <a href="https://www.citydoc.org.uk/moorgate-travel-health/">
                                <img title="COVID-19 In-Clinic CityDoc Testing In London" src="https://www.citydoc.org.uk/images/banners/in-clinic-testing.jpg" alt="COVID-19 In-Clinic CityDoc Testing" class="lazyloaded" data-ll-status="loaded">
                                <noscript>
                                    <img title="COVID-19 In-Clinic CityDoc Testing In London" src="https://www.citydoc.org.uk/images/banners/in-clinic-testing.jpg" alt="COVID-19 In-Clinic CityDoc Testing" />
                                </noscript>
                            </a>
                        </div>
                        <h3 style="font-weight: 500;">In-Clinic COVID 19 Vaccination<br>
                            <!-- <span class="pink">CityDoc London</span> -->
                        </h3>
                        <ul class="uk-list uk-list-striped">
                            <li><i class="uk-icon-check-square-o"></i> <strong>Whether you’re flying out of, or into the UK,</strong> you have the option of ordering a home testing kit delivered via courier or getting tested in one of CityDoc’s London Clinics.</li>
                            <li><i class="uk-icon-check-square-o"></i> The COVID-19 test is taken by one of our qualified medical technicians at one of our CityDoc Clinics.</li>
                            <li>
                                <i class="uk-icon-check-square-o"></i> Completed tests are taken directly to the lab by the dedicated medical courier Monday to Saturday.
                            </li>
                            <li>
                                <div class="pink" style="font-size: 17px;"><i class="uk-icon-check-square-o"></i> 
                                    <strong>In-Clinic Testing</strong>
                                </div>
                                <p>Testing available in Central London.</p>
                            </li>
                        </ul>
                        <p>
                            <a class="uk-button uk-button-default uk-button-large uk-align-center blue-button" href="https://www.citydoc.org.uk/moorgate-travel-health/">
                                <strong>Book an appointment online</strong>
                            </a>
                        </p>
                        <div class="col_12 last covid">
                            <p><span class="fa fa-envelope mr-3"></span> <a href="#">info@sfvaccinations.co.uk</a></p>
                            <p><span class="fa fa-phone mr-3"></span> <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a></p>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-1 uk-width-medium-1-3">
                    <div class="uk-panel uk-panel-box widget_text" style="min-height: 735.062px;">
                        <div style="margin-top: -20px; margin-left: -20px; margin-right: -20px;">
                            <img title="British Airways Customers Advice &amp; Support" src="https://www.citydoc.org.uk/images/banners/info.jpg" alt="British Airways Customers Advice &amp; Support" class="lazyloaded" data-ll-status="loaded">
                            <noscript>
                                <img title="British Airways Customers Advice &amp; Support" src="https://www.citydoc.org.uk/images/banners/info.jpg" alt="British Airways Customers Advice &amp; Support" />
                            </noscript>
                        </div>
                        <h3 style="font-weight: 500;">Corporate COVID 19 Service<br>
                            <!-- <span class="pink">Support &amp; Bookings</span> -->
                        </h3>
                        <ul class="uk-list uk-list-striped">
                            <li><i class="uk-icon-check-square-o"></i> 
                                <strong>For help and advice booking your test</strong> or for any support regarding your appointment, please contact us via our dedicated COVID19 email address and one of our support advisors will reply promptly.<strong><br>
                                Email: <i class="uk-icon-envelope-o pink"></i> 
                                <a href="mailto:covid19@citydoc.org.uk">covid19@citydoc.org.uk</a></strong><p></p>
                                <h3>Further Information</h3>
                                <p>For further information and before purchasing a test, see the government’s travel advice or read through our frequently asked questions.</p>
                                <p>
                                    <a href="https://www.gov.uk/guidance/travel-advice-novel-coronavirus">
                                        <i class="uk-icon-info-circle"></i> https://www.gov.uk/guidance/travel-advice-novel-coronavirus
                                    </a><br>
                                    <img style="width: 1px; height: 10px;" src="https://www.citydoc.org.uk/images/spacer.png" alt="spacer" class="lazyloaded" data-ll-status="loaded"><noscript><img style="width: 1px; height: 10px;" src="https://www.citydoc.org.uk/images/spacer.png" alt="spacer" /></noscript>
                                </p>
                            </li>
                        </ul>

                        <p>
                            <a class="uk-button uk-button-default uk-button-large uk-align-center blue-button" href="https://www.citydoc.org.uk/moorgate-travel-health/">
                                <strong>Book an appointment online</strong>
                            </a>
                        </p>
                        <div class="col_12 last covid">
                            <p><span class="fa fa-envelope mr-3"></span> <a href="#">info@sfvaccinations.co.uk</a></p>
                            <p><span class="fa fa-phone mr-3"></span> <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a></p>
                        </div>
                    </div>
                </div>
            </section>
            <main class="tm-content">
                <article class="uk-article">
                    <h1 class="uk-article-title"></h1>
                </article>
            </main>
        </div>
    </div>
</div>
@endsection