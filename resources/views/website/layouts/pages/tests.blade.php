@extends('website.layouts.base')

@section('styling')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
<style>
    table {
        border: unset !important;
        padding: unset !important; 
        width: 100%;
    }
    #content ol li, .blue-box ol li {
        list-style: unset !important; 
        /*padding-left: unset !important; 
        padding-top: unset !important; 
        padding-bottom: unset !important; */
    }
    #content ol, .blue-box ol {
        padding-left: unset !important; 
    }
</style>
@endsection

@section('content')
<div class="container">
    @include('website.layouts.logo')
    <div class="tab-content padding-20 bg-custom-white bx-wrapper">
        <div class="tab-inner">
            <div class="row">
                <h2>Private Blood Tests</h2>
            </div>
            <div class="row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Tests</a></li>
                    <li class="breadcrumb-item"><a href="#">Test A - Z</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Test {{isset($tests[0]) ? $tests[0]['code']: 'No Code Found'}}</li>
                  </ol>
                </nav>
            </div>
        </div>
    </div>
    
    <section class="section-padding-20">
    </section>
    
    <div class="tab-content padding-20 bg-custom-white bx-wrapper">
        <p><a data-id="25579" href="{{ url('tests/tests-a-z/A') }}" target="_self" title="Tests: A">A</a>&nbsp; |&nbsp; <a data-id="25580" href="{{ url('tests/tests-a-z/B') }}" target="_self" title="Tests: B">B</a>&nbsp; |&nbsp; <a data-id="25581" href="{{ url('tests/tests-a-z/C') }}" target="_self" title="Tests: C">C</a>&nbsp; |&nbsp; <a data-id="25582" href="{{ url('tests/tests-a-z/D') }}" target="_self" title="Tests: D">D</a>&nbsp; |&nbsp; <a data-id="25583" href="{{ url('tests/tests-a-z/E') }}" target="_self" title="Tests: E">E</a>&nbsp; |&nbsp; <a data-id="25584" href="{{ url('tests/tests-a-z/F') }}" target="_self" title="Tests: F">F</a>&nbsp; |&nbsp; <a data-id="25585" href="{{ url('tests/tests-a-z/G') }}" target="_self" title="Tests: G">G</a>&nbsp; |&nbsp; <a data-id="25586" href="{{ url('tests/tests-a-z/H') }}" target="_self" title="Tests: H">H</a>&nbsp; |&nbsp; <a data-id="25587" href="{{ url('tests/tests-a-z/I') }}" target="_self" title="Tests: I">I</a>&nbsp; |&nbsp; <a data-id="25589" href="{{ url('tests/tests-a-z/J') }}" target="_self" title="Tests: J">J</a>&nbsp; |&nbsp; <a data-id="25590" href="{{ url('tests/tests-a-z/K') }}" target="_self" title="Tests: K">K</a>&nbsp; |&nbsp; <a data-id="25590" href="{{ url('tests/tests-a-z/L') }}" target="_self" title="Tests: L">L</a>&nbsp; |&nbsp; <a data-id="25591" href="{{ url('tests/tests-a-z/M') }}" target="_self" title="Tests: M">M</a>&nbsp; |&nbsp; <a data-id="25592" href="{{ url('tests/tests-a-z/N') }}" target="_self" title="Tests: N">N</a>&nbsp; |&nbsp; <a data-id="25593" href="{{ url('tests/tests-a-z/O') }}" target="_self" title="Tests: O">O</a>&nbsp; |&nbsp; <a data-id="25594" href="{{ url('tests/tests-a-z/P') }}" target="_self" title="Tests: P">P</a>&nbsp; |&nbsp; <a data-id="25595" href="{{ url('tests/tests-a-z/Q') }}" target="_self" title="Tests: Q">Q</a>&nbsp; |&nbsp; <a data-id="25596" href="{{ url('tests/tests-a-z/R') }}" target="_self" title="Tests: R">R</a>&nbsp; |&nbsp; <a data-id="25597" href="{{ url('tests/tests-a-z/S') }}" target="_self" title="Tests: S">S</a>&nbsp; |&nbsp; <a data-id="25598" href="{{ url('tests/tests-a-z/T') }}" target="_self" title="Tests: T">T</a>&nbsp; |&nbsp; <a data-id="25599" href="{{ url('tests/tests-a-z/U') }}" target="_self" title="Tests: U">U</a>&nbsp; |&nbsp; <a data-id="25600" href="{{ url('tests/tests-a-z/V') }}" target="_self" title="Tests: V">V</a>&nbsp; |&nbsp; <a data-id="25601" href="{{ url('tests/tests-a-z/W') }}" target="_self" title="Tests: W">W</a>&nbsp; |&nbsp; <a data-id="25602" href="{{ url('tests/tests-a-z/X') }}" target="_self" title="Tests: X">X</a>&nbsp; |&nbsp; <a data-id="25603" href="{{ url('tests/tests-a-z/Y') }}" target="_self" title="Tests: Y">Y</a>&nbsp; |&nbsp; <a data-id="25604" href="{{ url('tests/tests-a-z/Z') }}" target="_self" title="Tests: Z">Z</a></p>
        <div class="section-header">
            <div class="section-heading">
                <h3 class="text-custom-black fw-700">Tests: {{isset($tests[0]) ? $tests[0]['code']: 'No Code Found'}}</h3>
                <div class="section-description">
                    <p class="text-custom-black fw-700">Test codes, sample requirements and turnaround times for our most requested tests.Please use the search box below to filter this list</p>
                </div>
            </div>
        </div>
        <div class="tab-pane fade show active" id="start">
            <div class="tab-inner">
                <div class="row">
                    <div class="col-lg-12 table-responsive">
                        <table class="table borderless">
                            <thead class="thead-blue text-left">
                                <tr>
                                    <th>Test Name</th>
                                </tr>
                            </thead>
                            <tbody class="text-left">
                                @foreach($tests as $key => $value)
                                <tr>
                                    <td><a href="{{url('')}}">{{$value['names']}}</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="newsletter-style-1 section-padding-30 bg-light-blue">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="newsletter-content-wrapper mb-md-40">
                        <h3 class="text-custom-white fw-600">Booking</h3>
                        <p class="text-custom-white">You can book an appointment by:</p>
                        <p class="text-custom-white">Phoning 0113 2213533 or 0113 8730242</p>
                        <p class="text-custom-white">Emailing info@sfvaccinations.co.uk</p>
                        <a href="#" class="btn btn-warning">Book Online</a>
                    </div>
                </div>
                <!-- <div class="col-lg-6">
                    <div class="newsletter-form-wrapper">
                        <form>
                            <div class="input-group">
                                <input type="email" name="#" class="form-control form-control-custom" placeholder="Email Id">
                                <div class="input-group-append">
                                    <button type="submit">Subscribe</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div> -->
            </div>
        </div>
    </section>
    <section class="section-padding-30">
    </section>
</div>
@endsection

@section('script')
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.table').DataTable();
        } );
    </script>
@endsection