@extends('website.layouts.base')
@section('content')
<div class="container">
    @include('website.layouts.logo')
    <div class="bg-image p-5 text-center shadow-1-strong rounded mb-5 text-white" style="background-image: url('https://mdbcdn.b-cdn.net/img/new/slides/003.jpg'); background-size:cover; width:100%;">
        <h1 class="mb-3 h2" style="color: #ffffff;">International Arrivals 2 & 8 Day Covid-19 Testing</h1>
    </div>
    <!-- Section One -->
    <div class="tm-middle uk-grid" data-uk-grid-match="" data-uk-grid-margin="">
		<div class="tm-main uk-width-medium-2-3 uk-row-first" style="min-height: 755.781px;">
			<main class="tm-content">
				<div id="primary" class="content-area">
					<main id="main" class="site-main" role="main">
						<nav class="woocommerce-breadcrumb">
							<a href="https://www.citydoc.org.uk">Home</a>&nbsp;/&nbsp;
							<a href="https://www.citydoc.org.uk/product-category/covid-19/">Covid-19</a>
							&nbsp;/&nbsp;Covid-19 Day 2 Only Test
						</nav>
						<div class="woocommerce-notices-wrapper"></div>
						<div id="product-67728" class="product type-product post-67728 status-publish first instock product_cat-covid-19 product_cat-day2 has-post-thumbnail shipping-taxable purchasable product-type-simple">
							<div class="woocommerce-product-gallery woocommerce-product-gallery--with-images woocommerce-product-gallery--columns-4 images" data-columns="4" style="opacity: 1; transition: opacity 0.25s ease-in-out 0s;">
								<figure class="woocommerce-product-gallery__wrapper">
									<div data-thumb="https://www.citydoc.org.uk/wp-content/uploads/2021/04/day2-covid-test-1-100x100.jpg" data-thumb-alt="" class="woocommerce-product-gallery__image"><a href="https://www.citydoc.org.uk/wp-content/uploads/2021/04/day2-covid-test-1.jpg"><img width="600" height="600" src="https://www.citydoc.org.uk/wp-content/uploads/2021/04/day2-covid-test-1.jpg" class="wp-post-image" alt="" title="day2-covid-test" data-caption="" data-src="https://www.citydoc.org.uk/wp-content/uploads/2021/04/day2-covid-test-1.jpg" data-large_image="https://www.citydoc.org.uk/wp-content/uploads/2021/04/day2-covid-test-1.jpg" data-large_image_width="600" data-large_image_height="600" srcset="https://www.citydoc.org.uk/wp-content/uploads/2021/04/day2-covid-test-1.jpg 600w, https://www.citydoc.org.uk/wp-content/uploads/2021/04/day2-covid-test-1-300x300.jpg 300w, https://www.citydoc.org.uk/wp-content/uploads/2021/04/day2-covid-test-1-150x150.jpg 150w, https://www.citydoc.org.uk/wp-content/uploads/2021/04/day2-covid-test-1-100x100.jpg 100w" sizes="(max-width: 600px) 100vw, 600px"></a></div> 
								</figure>
							</div>
							<div class="summary entry-summary">
								<h1 class="product_title entry-title">Covid-19 Day 2 Only Test</h1>
								<p class="price"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">£</span>80.00</bdi></span>
								</p>
								<form class="cart" action="https://www.citydoc.org.uk/product/covid-19-day-2-international-arrivals/" method="post" enctype="multipart/form-data" id="CB_FORM_ID_1">
									<div class="clear"></div>
									<div class="qib-container">
										<button type="button" class="minus qib-button">-</button>
										<div class="quantity buttons_added">
											<label class="screen-reader-text" for="quantity_60ae36da443b5">Covid-19 Day 2 Only Test quantity</label> <input type="number" id="quantity_60ae36da443b5" class="input-text qty text" step="1" min="1" max="" name="quantity" value="1" title="Qty" size="4" placeholder="" inputmode="numeric">
										</div>
										<button type="button" class="plus qib-button">+</button>
									</div>
									<button type="submit" name="add-to-cart" value="67728" class="single_add_to_cart_button button alt">Add to basket</button>
								</form>
								<div class="product_meta">
									<span class="posted_in">Categories: <a href="https://www.citydoc.org.uk/product-category/covid-19/" rel="tag">Covid-19</a>, <a href="https://www.citydoc.org.uk/product-category/day2/" rel="tag">Day2</a></span>
								</div>
							</div>
							<div class="woocommerce-tabs wc-tabs-wrapper">
								<ul class="tabs wc-tabs" role="tablist">
									<li class="description_tab active" id="tab-title-description" role="tab" aria-controls="tab-description">
										<a href="#tab-description">Description </a>
									</li>
								</ul>
								<div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--description panel entry-content wc-tab" id="tab-description" role="tabpanel" aria-labelledby="tab-title-description" style="">
									<p><strong>Day 2 Orders will be shipped from Monday 17th May.</strong></p>
									<p>CityDoc and its laboratory partners have met the legal compliance requirements set out by the Government to provide day 2 testing</p>
									<p><i class="uk-icon-check-square-o"></i> Under government guidance you are required to purchase your tests pre-arrival in the UK</p>
									<p><i class="uk-icon-check-square-o"></i> When you purchase these tests you are required to enter the arrival date into the UK on the checkout page</p>
									<p><i class="uk-icon-check-square-o"></i> Once purchased you will then receive a unique reference number</p>
									<p><i class="uk-icon-check-square-o"></i> You must enter this reference number on the passenger locator form before you arrive in the UK</p>
								</div>
							</div>
						</div>
					</main>
				</div>
			</main>
		</div>
		<aside class="tm-sidebar-a uk-width-medium-1-3" style="min-height: 755.781px;">
			<div class="uk-panel uk-panel-box woocommerce widget_shopping_cart"><h3 class="uk-panel-title"><i class="uk-icon-cart-plus"></i> Basket</h3>
				<div class="widget_shopping_cart_content">
		    		<p class="woocommerce-mini-cart__empty-message">No products in the basket.</p>
				</div>
			</div>
			<div class="uk-panel uk-panel-box widget_nav_menu"><h3 class="uk-panel-title"><i class="uk-icon-plane"></i> Covid-19 Testing</h3><ul class="uk-nav uk-nav-parent-icon uk-nav-side" data-uk-nav="{}"><li><a href="https://www.citydoc.org.uk/covid-19-international-arrivals-testing-day-2-and-8/" class="">UK Arrivals: C19 Day 2 or Day 2 &amp; 8 Tests</a></li><li><a href="https://www.citydoc.org.uk/test-to-release-covid-19-testing/" class="">UK Arrivals: C19 Test To Release</a></li><li><a href="https://www.citydoc.org.uk/covid-19-fit-to-fly/" class="">UK Departures: C19 Border Entry</a></li><li><a href="https://www.citydoc.org.uk/product/covid-19-antibody-home-test/" class="">C19 Antibody Test</a></li><li><a href="https://www.citydoc.org.uk/covid-19-return-to-work/" class="">C19 Return to work</a></li><li><a href="https://www.citydoc.org.uk/faqs-covid-19-testing/" class="">C19 FAQ’s</a></li></ul></div>
		</aside>
	</div>
    <!-- Section Two -->
</div>
@endsection