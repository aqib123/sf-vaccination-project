@extends('website.layouts.base')
@section('styling')
<style type="text/css">
    h2, .h2 {
        font-size: 1.2rem;
    }
</style>
@endsection
@section('content')
<div class="container">
    @include('website.layouts.logo')
    <section class="section-padding div_with_white_back">
        <div class="section-header">
            <div class="section-heading">
                <h3 class="text-custom-black fw-700">Onsite Vaccination Service</h3>
            </div>
        </div>
        <div class="row" style="margin-left: unset !important;">
            <div class="col_12">
                <h2>Why SF?</h2>
                <p>
                    As well as our extensive clinic network, we can provide onsite vaccination clinics for travel, public health (including flu) and occupational health purposes. We understand that your employees may not have time in the working day to attend a SF clinic in person, so we can bring the clinic to your place of work instead.
                </p>
                <br>
                <h2>How does it work?</h2>
                <div class="row">
                    <article class="col-lg-4 col-md-6 post mb-xl-30">
                        <div class="post-wrapper">
                            <div class="blog-img animate-img">
                                <a href="{{url('occupation-health-vaccination')}}">
                                    <img src="{{ url('sfvaccination/images/contact_us.png') }}" class="full-width" alt="blog" style="width:100%;height: 230px;">
                                </a>
                            </div>
                            <div class="blog-meta padding-20">
                                <div class="post-content">
                                    <h4><a href="#" class="text-light-green fw-600">Contact Us</a></h4>
                                    <p class="text-light-white">Contact our sales team at</p>
                                    <p class="text-light-white"><span class="fa fa-envelope mr-3"></span><a href="#">info@sfvaccinations.co.uk</a></p>
                                    <p class="text-light-white"><span class="fa fa-phone mr-3"></span> <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a></p>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="col-lg-4 col-md-6 post mb-xl-30">
                        <div class="post-wrapper">
                            <div class="blog-img animate-img">
                                <a href="{{ url('medical') }}">
                                    <img src="{{ url('sfvaccination/images/arrange_date.png') }}" class="full-width" alt="blog" style="width:100%;height: 230px;">
                                </a>
                            </div>
                            <div class="blog-meta padding-20">
                                <div class="post-content">
                                    <h4><a href="#" class="text-light-green fw-600">Arrange a Date</a></h4>
                                   <p class="text-light-white">We will arrange a date and time convenient for yourselves for our visit.</p> 
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="col-lg-4 col-md-6 post mb-xl-30">
                        <div class="post-wrapper">
                            <div class="blog-img animate-img">
                                <a href="{{ url('occupational-health-blood-test') }}">
                                    <img src="{{ url('sfvaccination/images/clinic_day.png') }}" class="full-width" alt="blog" style="width:100%;height: 230px;">
                                </a>
                            </div>
                            <div class="blog-meta padding-20">
                                <div class="post-content">
                                    <h4><a href="#" class="text-light-green fw-600">Clinic Day</a></h4>
                                   <p class="text-light-white">The SF nurse will attend, set up and provide any services needed on the day.</p> 
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
            <div style="clear:both;"></div>
        </div>
    </section>
    <!-- <section class="newsletter-style-1 section-padding-30 bg-light-blue">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="newsletter-content-wrapper mb-md-40">
                        <h3 class="text-custom-white fw-600">Booking</h3>
                        <p class="text-custom-white">You can book an appointment by:</p>
                        <p class="text-custom-white">Phoning 0113 2213533 or 0113 8730242</p>
                        <p class="text-custom-white">Emailing info@sfvaccinations.co.uk</p>
                        <a href="#" class="btn btn-warning">Book Online</a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="newsletter-form-wrapper">
                        <form>
                            <div class="input-group">
                                <input type="email" name="#" class="form-control form-control-custom" placeholder="Email Id">
                                <div class="input-group-append">
                                    <button type="submit">Subscribe</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div> 
            </div>
        </div>
    </section> -->
    <section class="section-padding-30"></section>
</div>
@endsection