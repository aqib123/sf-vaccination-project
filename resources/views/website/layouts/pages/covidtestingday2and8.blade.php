@extends('website.layouts.base')
@section('content')
<div class="container">
    @include('website.layouts.logo')
    <div class="bg-image p-5 text-center shadow-1-strong rounded mb-5 text-white" style="background-image: url('https://mdbcdn.b-cdn.net/img/new/slides/003.jpg'); background-size:cover; width:100%;">
        <h1 class="mb-3 h2" style="color: #ffffff;">International Arrivals 2 & 8 Day Covid-19 Testing</h1>
    </div>
    <!-- Section One -->
    <div class="uk-panel uk-panel-box uk-width-medium-4-6 uk-container-center">
        <h3 class="pink">Covid-19 UK International Arrivals Testing Day 2 and 8</h3>
        <p>CityDoc and its laboratory partners have met the legal compliance requirements set out by the Government to provide day 2 &amp; 8 testing.</p>
        <p>Under government guidance you are required to purchase your tests pre-arrival to the UK. You are required to fill in your passage locator form no later than 48 hrs before travelling. So, we advise you buy your kits at least 2 working days before your departure.</p>
        <p>When you order your tests, we will ask you to provide details about yourself and your journey. You will then receive a unique reference number, which you must enter on your passenger locator form.</p>
        <p><a href="https://www.gov.uk/uk-border-control?priority-taxon=774cee22-d896-44c1-a611-e3109cce8eae" target="_blank" rel="noopener noreferrer">https://www.gov.uk/uk-border-control</a></p>
        <div class="uk-grid data-uk-grid-margin=">
            <div class="uk-panel uk-width-medium-1-2">
                <h4 class="pink" style="text-align: center;">OPTION A</h4>
                <h3 class="pink" style="text-align: center;">Day 2 Only Home Testing £80</h3>
                <p style="text-align: center;">Purchase your home test kit here:</p>
                <p><a class="uk-button uk-button-large uk-width-medium-5-6 uk-align-center uk-container-center pink-button" href="{{ url('product/covid-19-day-2-international-arrivals') }}">BUY DAY 2 ONLY ONLINE</a></p>
                <p style="text-align: center; font-weight: bold;">We advise you order no later than 2 working days before UK arrival</p>
                <p><img style="width: 1px; height: 15px;" src="https://www.citydoc.org.uk/images/spacer.png" alt="spacer" class="lazyloaded" data-ll-status="loaded"><noscript><img style="width: 1px; height: 15px;" src="https://www.citydoc.org.uk/images/spacer.png" alt="spacer" /></noscript></p>
            </div>
            <div class="uk-panel uk-width-medium-1-2">
                <h4 class="pink" style="text-align: center;">OPTION B</h4>
                <h3 class="pink" style="text-align: center;">Day 2 &amp; 8 Home Testing £160</h3>
                <p style="text-align: center;">Purchase your home test kits here:</p>
                <p><a class="uk-button uk-button-large uk-width-medium-5-6 uk-align-center uk-container-center pink-button" href="{{ url('product/covid-19-2-and-8-day-international-arrivals') }}">BUY DAY 2 &amp; 8 ONLINE</a></p>
                <p style="text-align: center; font-weight: bold;">We advise you order no later than 2 working days before UK arrival</p>
                <p><img style="width: 1px; height: 15px;" src="https://www.citydoc.org.uk/images/spacer.png" alt="spacer" class="lazyloaded" data-ll-status="loaded"><noscript><img style="width: 1px; height: 15px;" src="https://www.citydoc.org.uk/images/spacer.png" alt="spacer" /></noscript></p>
            </div>
        </div>
        <div class="uk-grid data-uk-grid-margin=">
            <div class="uk-panel uk-align-center uk-width-medium-1-2">
                <h4 class="pink" style="text-align: center;">OPTION C</h4>
                <h3 class="pink" style="text-align: center;">London In-clinic Testing</h3>
                <h4 class="pink" style="text-align: center;">Day 2 &amp; 8 Testing From £300</h4>
                <h4 class="pink" style="text-align: center;">Day 2 Only Test From £175</h4>
                <p style="text-align: center;">Book your in-clinic appointment here:</p>
                <p><a class="uk-button uk-button-large uk-width-medium-5-6 uk-align-center uk-container-center pink-button" href="https://www.citydoc.org.uk/covid-19-london-clinic-testing/">BOOK APPOINTMENT ONLINE</a></p>
                <p style="text-align: center; font-weight: bold;">We advise you order no later than 2 working days before UK arrival</p>
                <p><img style="width: 1px; height: 15px;" src="https://www.citydoc.org.uk/images/spacer.png" alt="spacer" class="lazyloaded" data-ll-status="loaded"><noscript><img style="width: 1px; height: 15px;" src="https://www.citydoc.org.uk/images/spacer.png" alt="spacer" /></noscript></p>
            </div>
        </div>
        <h3 style="text-align: center;">Please see our FAQ’s for UK International arrival tests</h3>
        <p><a class="uk-button uk-button-large uk-width-medium-1-6 uk-align-center uk-container-center blk-button" href="https://www.citydoc.org.uk/faqs-covid-19-testing#international-arrivals-covid-testing">FAQ’s</a></p>
    </div>
    <!-- Section Two -->
</div>
@endsection