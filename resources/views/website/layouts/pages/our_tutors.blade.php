@extends('website.layouts.base')
@section('content')
<input type="hidden" value="PK" id='hiddenchekuae'>	<div id="content" style="width: 100%;">
				<div id="post-1287" class="post-1287 page type-page status-publish hentry">
			<span class="entry-title" style="display: none;">Our Tutors</span><span class="vcard" style="display: none;"><span class="fn"><a href="../index.html" title="Posts by admin" rel="author">admin</a></span></span><span class="updated" style="display:none;">2018-07-30T12:21:55+00:00</span>																		<div class="post-content">
				<div class="fusion-fullwidth fullwidth-box fusion-fullwidth-1  fusion-parallax-none nonhundred-percent-fullwidth main-image" style="border-color:#eae9e9;border-bottom-width: 0px;border-top-width: 0px;border-bottom-style: solid;border-top-style: solid;padding-bottom:415px;padding-top:0px;padding-left:0px;padding-right:0px;background-color:#efefef;background-position:right top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;-ms-filter: &quot;progid:DXImageTransform.Microsoft.AlphaImageLoader(src=&#039;https://www.mytutorsource.com/wp-content/uploads/2016/04/our-tutor.jpg&#039;, sizingMethod=&#039;scale&#039;)&quot;;background-image: url(https://www.mytutorsource.com/wp-content/uploads/2016/04/our-tutor.jpg);"><style type="text/css" scoped="scoped">.fusion-fullwidth-1 {
                            padding-left: 0px !important;
                            padding-right: 0px !important;
                        }</style><div class="fusion-row"><div class="slideourtutor">
<ul>
<li><span id="yo">Expert Tutors, personally shortlisted from 100s of applicants:</span></li>
<li><i class="fa fa-check"></i> <span id="yo">10+ years of teaching experience</span></li>
<li><i class="fa fa-check"></i> <span id="yo">Hold Master&#8217;s or Ph.D. degrees</span></li>
</ul>
</div>
</div></div><div class="fusion-fullwidth fullwidth-box fusion-fullwidth-2  fusion-parallax-none nonhundred-percent-fullwidth" style="border-color:#eae9e9;border-bottom-width: 0px;border-top-width: 0px;border-bottom-style: solid;border-top-style: solid;padding-bottom:20px;padding-top:20px;padding-left:px;padding-right:px;"><style type="text/css" scoped="scoped">.fusion-fullwidth-2 {
                            padding-left: px !important;
                            padding-right: px !important;
                        }</style><div class="fusion-row"></div></div><div class="fusion-fullwidth fullwidth-box fusion-fullwidth-3  fusion-parallax-none nonhundred-percent-fullwidth" style="border-color:#eae9e9;border-bottom-width: 0px;border-top-width: 0px;border-bottom-style: solid;border-top-style: solid;padding-bottom:20px;padding-top:20px;padding-left:px;padding-right:px;" id="hideforHK_tutors_page"><style type="text/css" scoped="scoped">.fusion-fullwidth-3 {
                            padding-left: px !important;
                            padding-right: px !important;
                        }</style><div class="fusion-row"><div class="fusion-title title fusion-title-size-three custom_heading"><h3 class="title-heading-left">Some of our Highly Experienced Tutors</h3><div class="title-sep-container"><div class="title-sep sep-single"></div></div></div><div class="fusion-sep-clear"></div><div class="fusion-separator sep-single" style="border-color:#e0dede;border-top-width:1px;margin-left: auto;margin-right: auto;margin-top:px;margin-bottom:30px;width:100%;max-width:60%;"></div><div class='white' style='background:rgba(0,0,0,0); border:solid 0px rgba(0,0,0,0); border-radius:0px; padding:0px 0px 0px 0px;'>
<div id='slider_1907' class='owl-carousel sa_owl_theme owl-pagination-true' data-slider-id='slider_1907' style='visibility:hidden;'>
<div id='slider_1907_slide01' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-image:url(_https_/dev.mytutorsource.com/wp-content/uploads/2015/02/MTS_Main-page_03.html); background-position:left top; background-size:contain; background-repeat:no-repeat; background-color:rgb(238, 238, 238); '><div><img class="aligncenter wp-image-1922 size-thumbnail" src="../../www.mytutorsource.com/wp-content/uploads/2017/05/1.html" alt="" width="150" height="150" /></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Ms. Sarah Chan </strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../../www.mytutorsource.com/wp-content/uploads/2016/05/verify6.html" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Master's in Economics</strong><br /><strong>Dept. of Economics at MTS</strong></p>
</div></div>
<div id='slider_1907_slide02' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-image:url(_https_/dev.mytutorsource.com/wp-content/uploads/2015/02/MTS_Main-page_03.html); background-position:left top; background-size:contain; background-repeat:no-repeat; background-color:rgb(255, 255, 255); '><div><img class="aligncenter wp-image-1922 size-thumbnail" src="../../www.mytutorsource.com/wp-content/uploads/2016/04/Anderson-150x150.html" alt="" width="150" height="150" /></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Mr. Anderson Collins </strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../../www.mytutorsource.com/wp-content/uploads/2016/05/verify6.html" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Master's in English Language</strong><br /><strong>Dept. of English at MTS</strong></p>
</div></div>
<div id='slider_1907_slide03' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-image:url(_https_/dev.mytutorsource.com/wp-content/uploads/2015/02/MTS_Main-page_03.html); background-position:left top; background-size:contain; background-repeat:no-repeat; background-color:rgb(238, 238, 238); '><div><img class="wp-image-1925 size-thumbnail aligncenter" src="../../www.mytutorsource.com/wp-content/uploads/2016/04/Kashif-150x150.html" alt="" width="150" height="150" /></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Mr. Kashif Jibran</strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../../www.mytutorsource.com/wp-content/uploads/2016/05/verify6.html" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Ph.D. in Mathematics</strong><br /><strong>Dept. of Mathematics at MTS</strong></p>
</div></div>
<div id='slider_1907_slide04' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-image:url(_https_/dev.mytutorsource.com/wp-content/uploads/2015/02/MTS_Main-page_03.html); background-position:left top; background-size:contain; background-repeat:no-repeat; background-color:rgb(255, 255, 255); '><div><img class="wp-image-1926 size-thumbnail aligncenter" src="../../www.mytutorsource.com/wp-content/uploads/2017/05/3.html" alt="" width="150" height="150" /></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Ms. Wendy Chilong</strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../../www.mytutorsource.com/wp-content/uploads/2016/05/verify6.html" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Master's in Bio Chemistry</strong><br /><strong>Dept. of Biology at MTS</strong></p>
</div></div>
<div id='slider_1907_slide05' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-image:url(_https_/dev.mytutorsource.com/wp-content/uploads/2015/02/MTS_Main-page_03.html); background-position:left top; background-size:contain; background-repeat:no-repeat; background-color:rgb(238, 238, 238); '><div><img class="wp-image-1926 size-thumbnail aligncenter" src="../../www.mytutorsource.com/wp-content/uploads/2016/04/katie-150x150.html" alt="" width="150" height="150" /></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Ms. Katie Leslove</strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../../www.mytutorsource.com/wp-content/uploads/2016/05/verify6.html" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Master's in Law</strong><br /><strong>Dept. of Law at MTS</strong></p>
</div></div>
<div id='slider_1907_slide06' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-image:url(_https_/dev.mytutorsource.com/wp-content/uploads/2015/02/MTS_Main-page_03.html); background-position:left top; background-size:contain; background-repeat:no-repeat; background-color:rgb(255, 255, 255); '><div><img class="wp-image-1924 size-thumbnail aligncenter" src="../../www.mytutorsource.com/wp-content/uploads/2016/04/fahad-150x150.html" alt="" width="150" height="150" /></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Mr. Fahad Rafiq</strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../../www.mytutorsource.com/wp-content/uploads/2016/05/verify6.html" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Master's in Biology</strong><br /><strong>Dept. of Biology at MTS</strong></p>
</div></div>
<div id='slider_1907_slide07' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-image:url(_https_/dev.mytutorsource.com/wp-content/uploads/2015/02/MTS_Main-page_03.html); background-position:left top; background-size:contain; background-repeat:no-repeat; background-color:rgb(238, 238, 238); '><div><a href="../../www.mytutorsource.com/wp-content/uploads/2016/04/rabia.html"><img class="aligncenter wp-image-1945 size-thumbnail" src="../../www.mytutorsource.com/wp-content/uploads/2016/04/rabia-150x150.html" alt="" width="150" height="150" /></a></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Ms. Rabia Ather</strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../../www.mytutorsource.com/wp-content/uploads/2016/05/verify6.html" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Master's in English Literature</strong><br /><strong>Dept. of English at MTS</strong></p>
</div></div>
<div id='slider_1907_slide08' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-image:url(_https_/dev.mytutorsource.com/wp-content/uploads/2015/02/MTS_Main-page_03.html); background-position:left top; background-size:contain; background-repeat:no-repeat; background-color:rgb(255, 255, 255); '><div><img class="wp-image-1927 size-thumbnail aligncenter" src="../../www.mytutorsource.com/wp-content/uploads/2016/04/shahid-150x150.html" alt="" width="150" height="150" /></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Mr. Shahid Manzoor</strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../../www.mytutorsource.com/wp-content/uploads/2016/05/verify6.html" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Master's in Physics</strong><br /><strong>Dept. of Physics at MTS</strong></p>
</div></div>
<div id='slider_1907_slide09' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-image:url(_https_/dev.mytutorsource.com/wp-content/uploads/2015/02/MTS_Main-page_03.html); background-position:left top; background-size:contain; background-repeat:no-repeat; background-color:rgb(238, 238, 238); '><div><a href="../../www.mytutorsource.com/wp-content/uploads/2016/04/cara.html"><img class="aligncenter wp-image-1923 size-thumbnail" src="../../www.mytutorsource.com/wp-content/uploads/2016/04/cara-150x150.html" alt="" width="150" height="150" /></a></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Ms. Cara Lee</strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../../www.mytutorsource.com/wp-content/uploads/2016/05/verify6.html" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Master's in Sociology Sciences</strong><br /><strong>Dept. of Sociology at MTS</strong></p>
</div></div>
<div id='slider_1907_slide10' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-image:url(_https_/dev.mytutorsource.com/wp-content/uploads/2015/02/MTS_Main-page_03.html); background-position:left top; background-size:contain; background-repeat:no-repeat; background-color:rgb(255, 255, 255); '><div><a href="../../www.mytutorsource.com/wp-content/uploads/2016/04/abdul.html"><img class="aligncenter wp-image-1946 size-thumbnail" src="../../www.mytutorsource.com/wp-content/uploads/2016/04/abdul-150x150.html" alt="" width="150" height="150" /></a></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Mr. Abdul Rehman</strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../../www.mytutorsource.com/wp-content/uploads/2016/05/verify6.html" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Master's in Physics</strong><br /><strong>Dept. of Physics at MTS</strong></p>
</div></div>
<div id='slider_1907_slide11' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-image:url(_https_/dev.mytutorsource.com/wp-content/uploads/2015/02/MTS_Main-page_03.html); background-position:left top; background-size:contain; background-repeat:no-repeat; background-color:rgb(238, 238, 238); '><div><a href="../../www.mytutorsource.com/wp-content/uploads/2016/04/sofia.html"><img class="aligncenter wp-image-1947 size-thumbnail" src="../../www.mytutorsource.com/wp-content/uploads/2016/04/sofia-150x150.html" alt="" width="150" height="150" /></a></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Ms. Sofia Babar</strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../../www.mytutorsource.com/wp-content/uploads/2016/05/verify6.html" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Bachelor's in Psychology</strong><br /><strong>Dept. of Psychology at MTS</strong></p>
</div></div>
<div id='slider_1907_slide12' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-image:url(_https_/dev.mytutorsource.com/wp-content/uploads/2015/02/MTS_Main-page_03.html); background-position:left top; background-size:contain; background-repeat:no-repeat; background-color:rgb(255, 255, 255); '><div><a href="../../www.mytutorsource.com/wp-content/uploads/2017/05/22.html"><img class="aligncenter wp-image-1947 size-thumbnail" src="../../www.mytutorsource.com/wp-content/uploads/2017/05/22.html" alt="" width="150" height="150" /></a></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Mr. Asad Ali</strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../../www.mytutorsource.com/wp-content/uploads/2016/05/verify6.html" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Master's in Electrical Engineering</strong><br /><strong>Dept. of Mathematics at MTS</strong></p>
</div></div>
</div>
</div>
<script type='text/javascript'>
	jQuery(document).ready(function() {
		jQuery('#slider_1907').owlCarousel({
			responsive:{
				0:{ items:1 },
				480:{ items:2 },
				768:{ items:2 },
				980:{ items:3 },
				1200:{ items:3 },
				1500:{ items:3 }
			},
			autoplay : false,
			autoplayHoverPause : false,
			smartSpeed : 1000,
			fluidSpeed : 1000,
			autoplaySpeed : 1000,
			navSpeed : 1000,
			dotsSpeed : 1000,
			loop : true,
			nav : true,
			navText : ['',''],
			dots : true,
			responsiveRefreshRate : 200,
			slideBy : 1,
			mergeFit : true,
			autoHeight : false,
			mouseDrag : true,
			touchDrag : true
		});
		jQuery('#slider_1907').css('visibility', 'visible');
	});
</script>
</div></div><div class="fusion-fullwidth fullwidth-box fusion-fullwidth-4  fusion-parallax-none nonhundred-percent-fullwidth" style="border-color:#eae9e9;border-bottom-width: 0px;border-top-width: 0px;border-bottom-style: solid;border-top-style: solid;padding-bottom:20px;padding-top:20px;padding-left:px;padding-right:px;" id="hideforHK_tutors_page_onlyuae"><style type="text/css" scoped="scoped">.fusion-fullwidth-4 {
                            padding-left: px !important;
                            padding-right: px !important;
                        }</style><div class="fusion-row"><div class="fusion-title title fusion-title-size-three custom_heading"><h3 class="title-heading-left">Some of our Highly Experienced Tutors <span style='color:#20D71D'>(3600+ tutors)</span></h3><div class="title-sep-container"><div class="title-sep sep-single"></div></div></div><div class="fusion-sep-clear"></div><div class="fusion-separator sep-single" style="border-color:#e0dede;border-top-width:1px;margin-left: auto;margin-right: auto;margin-top:px;margin-bottom:30px;width:100%;max-width:60%;"></div><div class='white' style='background:rgba(0,0,0,0); border:solid 0px rgba(0,0,0,0); border-radius:0px; padding:0px 0px 0px 0px;'>
<div id='slider_2252' class='owl-carousel sa_owl_theme owl-pagination-true' data-slider-id='slider_2252' style='visibility:hidden;'>
<div id='slider_2252_slide01' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-color:rgb(238, 238, 238); '><div><img class="aligncenter wp-image-1922 size-thumbnail" src="../wp-content/uploads/2017/07/1-2.png" alt="" width="150" height="150" /></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Mr. Conall Harvey </strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../wp-content/uploads/2016/05/verify6.png" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Master's in Business Administration</strong><br /><strong>Dept. of Business at MTS</strong></p>
</div></div>
<div id='slider_2252_slide02' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-color:rgb(255, 255, 255); '><div><img class="aligncenter wp-image-1922 size-thumbnail" src="../wp-content/uploads/2017/07/2-2.png" alt="" width="150" height="150" /></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Mr. Hassan Shehryar </strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../wp-content/uploads/2016/05/verify6.png" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>M Phil in Mathematics </strong><br /><strong>Dept. of Mathematics at MTS</strong></p>
</div></div>
<div id='slider_2252_slide03' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-color:rgb(238, 238, 238); '><div><img class="aligncenter wp-image-1922 size-thumbnail" src="../wp-content/uploads/2017/07/3-2.png" alt="" width="150" height="150" /></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Ms. Alexis Aaron </strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../wp-content/uploads/2016/05/verify6.png" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Master's in Accounting</strong><br /><strong>Dept of Accounting at MTS</strong></p>
</div></div>
<div id='slider_2252_slide04' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-color:rgb(255, 255, 255); '><div><img class="aligncenter wp-image-1922 size-thumbnail" src="../wp-content/uploads/2017/07/4-2.png" alt="" width="150" height="150" /></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Mr. Rahul Basra </strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../wp-content/uploads/2016/05/verify6.png" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Bachelor's in Chemistry</strong><br /><strong> Dept. of Chemistry at MTS</strong></p>
</div></div>
<div id='slider_2252_slide05' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-color:rgb(238, 238, 238); '><div><img class="aligncenter wp-image-1922 size-thumbnail" src="../wp-content/uploads/2017/07/5-1.png" alt="" width="150" height="150" /></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Ms. Priya Bhuvan </strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../wp-content/uploads/2016/05/verify6.png" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong> Bachelor's in Bio Chemistry </strong><br /><strong>Dept. of Biology at MTS</strong></p>
</div></div>
<div id='slider_2252_slide06' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-color:rgb(255, 255, 255); '><div><img class="aligncenter wp-image-1922 size-thumbnail" src="../wp-content/uploads/2017/07/6-1.png" alt="" width="150" height="150" /></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Ms. Iris Jasper</strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../wp-content/uploads/2016/05/verify6.png" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Master's in Electrical Engineering</strong><br /><strong>Dept. of Physics at MTS</strong></p>
</div></div>
<div id='slider_2252_slide07' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-color:rgb(238, 238, 238); '><div><img class="aligncenter wp-image-1922 size-thumbnail" src="../wp-content/uploads/2017/07/7-1.png" alt="" width="150" height="150" /></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Mr. Carl Abbott </strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../wp-content/uploads/2016/05/verify6.png" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Master's in English Language</strong><br /><strong> Dept. of English at MTS</strong></p>
</div></div>
<div id='slider_2252_slide08' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-color:rgb(255, 255, 255); '><div><img class="aligncenter wp-image-1922 size-thumbnail" src="../wp-content/uploads/2017/07/8-1.png" alt="" width="150" height="150" /></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Ms. Diya Manav</strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../wp-content/uploads/2016/05/verify6.png" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Bachelor's in Physics</strong><br /><strong>Dept. of Physics at MTS</strong></p>
</div></div>
<div id='slider_2252_slide09' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-color:rgb(238, 238, 238); '><div><img class="aligncenter wp-image-1922 size-thumbnail" src="../wp-content/uploads/2017/07/9-1.png" alt="" width="150" height="150" /></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Ms. Aliyah Ahmer </strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../wp-content/uploads/2016/05/verify6.png" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Bachelor's in Psychology </strong><br /><strong>Dept. of Psychology at MTS</strong></p>
</div></div>
<div id='slider_2252_slide10' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-color:rgb(255, 255, 255); '><div><img class="aligncenter wp-image-1922 size-thumbnail" src="../wp-content/uploads/2017/05/4.png" alt="" width="150" height="150" /></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Ms. Sarah Chan </strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../wp-content/uploads/2016/05/verify6.png" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Master's in Economics</strong><br /><strong>Dept. of Economics at MTS</strong></p>
</div></div>
<div id='slider_2252_slide11' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-color:rgb(238, 238, 238); '><div><img class="aligncenter wp-image-1922 size-thumbnail" src="../wp-content/uploads/2016/04/Anderson-150x150.png" alt="" width="150" height="150" /></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Mr. Anderson Collins </strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../wp-content/uploads/2016/05/verify6.png" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Master's in English Language</strong><br /><strong>Dept. of English at MTS</strong></p>
</div></div>
<div id='slider_2252_slide12' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-color:rgb(255, 255, 255); '><div><img class="wp-image-1925 size-thumbnail aligncenter" src="../wp-content/uploads/2016/04/Kashif-150x150.png" alt="" width="150" height="150" /></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Mr. Kashif Jibran</strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../wp-content/uploads/2016/05/verify6.png" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Ph.D. in Mathematics</strong><br /><strong>Dept. of Mathematics at MTS</strong></p>
</div></div>
<div id='slider_2252_slide13' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-color:rgb(238, 238, 238); '><div><img class="wp-image-1926 size-thumbnail aligncenter" src="../wp-content/uploads/2017/05/3.png" alt="" width="150" height="150" /></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Ms. Wendy Chilong</strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../wp-content/uploads/2016/05/verify6.png" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Master's in Bio Chemistry</strong><br /><strong>Dept. of Biology at MTS</strong></p>
</div></div>
<div id='slider_2252_slide14' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-color:rgb(255, 255, 255); '><div><img class="wp-image-1926 size-thumbnail aligncenter" src="../wp-content/uploads/2016/04/katie-150x150.png" alt="" width="150" height="150" /></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Ms. Katie Leslove</strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../wp-content/uploads/2016/05/verify6.png" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Master's in Law</strong><br /><strong>Dept. of Law at MTS</strong></p>
</div></div>
<div id='slider_2252_slide15' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-color:rgb(238, 238, 238); '><div><img class="wp-image-1924 size-thumbnail aligncenter" src="../wp-content/uploads/2016/04/fahad-150x150.png" alt="" width="150" height="150" /></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Mr. Fahad Rafiq</strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../wp-content/uploads/2016/05/verify6.png" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Master's in Biology</strong><br /><strong>Dept. of Biology at MTS</strong></p>
</div></div>
<div id='slider_2252_slide16' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-color:rgb(255, 255, 255); '><div><img class="aligncenter wp-image-1945 size-thumbnail" src="../wp-content/uploads/2016/04/rabia-150x150.png" alt="" width="150" height="150" /></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Ms. Rabia Ather</strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../wp-content/uploads/2016/05/verify6.png" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Master's in English Literature</strong><br /><strong>Dept. of English at MTS</strong></p>
</div></div>
<div id='slider_2252_slide17' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-color:rgb(238, 238, 238); '><div><img class="wp-image-1927 size-thumbnail aligncenter" src="../wp-content/uploads/2016/04/shahid-150x150.png" alt="" width="150" height="150" /></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Mr. Shahid Manzoor</strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../wp-content/uploads/2016/05/verify6.png" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Master's in Physics</strong><br /><strong>Dept. of Physics at MTS</strong></p>
</div></div>
<div id='slider_2252_slide18' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-color:rgb(255, 255, 255); '><div><a href="../wp-content/uploads/2016/04/cara.png"><img class="aligncenter wp-image-1923 size-thumbnail" src="../wp-content/uploads/2016/04/cara-150x150.png" alt="" width="150" height="150" /></a></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Ms. Cara Lee</strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../wp-content/uploads/2016/05/verify6.png" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Master's in Sociology Sciences</strong><br /><strong>Dept. of Sociology at MTS</strong></p>
</div></div>
<div id='slider_2252_slide19' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-color:rgb(238, 238, 238); '><div><a href="../wp-content/uploads/2016/04/abdul.png"><img class="aligncenter wp-image-1946 size-thumbnail" src="../wp-content/uploads/2016/04/abdul-150x150.png" alt="" width="150" height="150" /></a></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Mr. Abdul Rehman</strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../wp-content/uploads/2016/05/verify6.png" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Master's in Physics</strong><br /><strong>Dept. of Physics at MTS</strong></p>
</div></div>
<div id='slider_2252_slide20' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-color:rgb(255, 255, 255); '><div><a href="../wp-content/uploads/2016/04/sofia.png"><img class="aligncenter wp-image-1947 size-thumbnail" src="../wp-content/uploads/2016/04/sofia-150x150.png" alt="" width="150" height="150" /></a></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Ms. Sofia Babar</strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../wp-content/uploads/2016/05/verify6.png" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Bachelor's in Psychology</strong><br /><strong>Dept. of Psychology at MTS</strong></p>
</div></div>
<div id='slider_2252_slide21' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-color:rgb(238, 238, 238); '><div><a href="../wp-content/uploads/2017/05/22.png"><img class="aligncenter wp-image-1947 size-thumbnail" src="../wp-content/uploads/2017/05/22.png" alt="" width="150" height="150" /></a></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Mr. Asad Ali</strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../wp-content/uploads/2016/05/verify6.png" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Master's in Electrical Engineering</strong><br /><strong>Dept. of Mathematics at MTS</strong></p>
</div></div>
</div>
</div>
<script type='text/javascript'>
	jQuery(document).ready(function() {
		jQuery('#slider_2252').owlCarousel({
			responsive:{
				0:{ items:1 },
				480:{ items:2 },
				768:{ items:2 },
				980:{ items:3 },
				1200:{ items:3 },
				1500:{ items:3 }
			},
			autoplay : false,
			autoplayHoverPause : false,
			smartSpeed : 1000,
			fluidSpeed : 1000,
			autoplaySpeed : 1000,
			navSpeed : 1000,
			dotsSpeed : 1000,
			loop : true,
			nav : true,
			navText : ['',''],
			dots : true,
			responsiveRefreshRate : 200,
			slideBy : 1,
			mergeFit : true,
			autoHeight : false,
			mouseDrag : true,
			touchDrag : true
		});
		jQuery('#slider_2252').css('visibility', 'visible');
	});
</script>
</div></div><div class="fusion-fullwidth fullwidth-box fusion-fullwidth-5  fusion-parallax-none nonhundred-percent-fullwidth" style="border-color:#eae9e9;border-bottom-width: 0px;border-top-width: 0px;border-bottom-style: solid;border-top-style: solid;padding-bottom:20px;padding-top:20px;padding-left:px;padding-right:px;" id="for_hk_tut_page_12"><style type="text/css" scoped="scoped">.fusion-fullwidth-5 {
                            padding-left: px !important;
                            padding-right: px !important;
                        }</style><div class="fusion-row"><div class="fusion-title title fusion-title-size-three custom_heading"><h3 class="title-heading-left">Some of our Certified Tutors</h3><div class="title-sep-container"><div class="title-sep sep-single"></div></div></div><div class="fusion-sep-clear"></div><div class="fusion-separator sep-single" style="border-color:#e0dede;border-top-width:1px;margin-left: auto;margin-right: auto;margin-top:px;margin-bottom:30px;width:100%;max-width:60%;"></div><div class='white' style='background:rgba(0,0,0,0); border:solid 0px rgba(0,0,0,0); border-radius:0px; padding:0px 0px 0px 0px;'>
<div id='slider_2287' class='owl-carousel sa_owl_theme owl-pagination-true' data-slider-id='slider_2287' style='visibility:hidden;'>
<div id='slider_2287_slide01' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-image:url(_https_/dev.mytutorsource.com/wp-content/uploads/2015/02/MTS_Main-page_03.html); background-position:left top; background-size:contain; background-repeat:no-repeat; background-color:rgb(238, 238, 238); '><div><img class="aligncenter wp-image-1922 size-thumbnail" src="../../www.mytutorsource.com/wp-content/uploads/2016/04/Anderson-150x150.html" alt="" width="150" height="150" /></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Mr. Anderson Collins </strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../../www.mytutorsource.com/wp-content/uploads/2016/05/verify6.html" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Master's in English Language</strong><br /><strong>Dept. of English at MTS</strong></p>
</div></div>
<div id='slider_2287_slide02' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-image:url(_https_/dev.mytutorsource.com/wp-content/uploads/2015/02/MTS_Main-page_03.html); background-position:left top; background-size:contain; background-repeat:no-repeat; background-color:rgb(255, 255, 255); '><div><img class="aligncenter wp-image-1922 size-thumbnail" src="../../www.mytutorsource.com/wp-content/uploads/2017/05/1-1.html" alt="" width="150" height="150" /></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong> Ms. Lucy Wong </strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../../www.mytutorsource.com/wp-content/uploads/2016/05/verify6.html" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>M. Phil in Mathematics </strong><br /><strong>Dept. of Mathematics at MTS</strong></p>
</div></div>
<div id='slider_2287_slide03' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-image:url(_https_/dev.mytutorsource.com/wp-content/uploads/2015/02/MTS_Main-page_03.html); background-position:left top; background-size:contain; background-repeat:no-repeat; background-color:rgb(238, 238, 238); '><div><img class="aligncenter wp-image-1922 size-thumbnail" src="../../www.mytutorsource.com/wp-content/uploads/2017/05/2.html" alt="" width="150" height="150" /></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Ms. Wendy Chilong </strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../../www.mytutorsource.com/wp-content/uploads/2016/05/verify6.html" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Master's in Bio Chemistry</strong><br /><strong>Dept. of Biology at MTS</strong></p>
</div></div>
<div id='slider_2287_slide04' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-image:url(_https_/dev.mytutorsource.com/wp-content/uploads/2015/02/MTS_Main-page_03.html); background-position:left top; background-size:contain; background-repeat:no-repeat; background-color:rgb(255, 255, 255); '><div><img class="wp-image-1925 size-thumbnail aligncenter" src="../../www.mytutorsource.com/wp-content/uploads/2017/05/3-1.html" alt="" width="150" height="150" /></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Mr. Oscar Simon </strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../../www.mytutorsource.com/wp-content/uploads/2016/05/verify6.html" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Master's in Business Administration</strong><br /><strong>Dept. of Business at MTS</strong></p>
</div></div>
<div id='slider_2287_slide05' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-image:url(_https_/dev.mytutorsource.com/wp-content/uploads/2015/02/MTS_Main-page_03.html); background-position:left top; background-size:contain; background-repeat:no-repeat; background-color:rgb(238, 238, 238); '><div><img class="wp-image-1926 size-thumbnail aligncenter" src="../../www.mytutorsource.com/wp-content/uploads/2017/05/4.html" alt="" width="150" height="150" /></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Ms. Sarah Chan</strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../../www.mytutorsource.com/wp-content/uploads/2016/05/verify6.html" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Master's in Economics</strong><br /><strong>Dept. of Economics at MTS</strong></p>
</div></div>
<div id='slider_2287_slide06' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-image:url(_https_/dev.mytutorsource.com/wp-content/uploads/2015/02/MTS_Main-page_03.html); background-position:left top; background-size:contain; background-repeat:no-repeat; background-color:rgb(255, 255, 255); '><div><img class="wp-image-1926 size-thumbnail aligncenter" src="../../www.mytutorsource.com/wp-content/uploads/2017/05/5.html" alt="" width="150" height="150" /></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong> Ms. Diya Manav</strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../../www.mytutorsource.com/wp-content/uploads/2016/05/verify6.html" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Bachelor's in Physics</strong><br /><strong>Dept. of Physics at MTS</strong></p>
</div></div>
<div id='slider_2287_slide07' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-image:url(_https_/dev.mytutorsource.com/wp-content/uploads/2015/02/MTS_Main-page_03.html); background-position:left top; background-size:contain; background-repeat:no-repeat; background-color:rgb(238, 238, 238); '><div><img class="wp-image-1924 size-thumbnail aligncenter" src="../../www.mytutorsource.com/wp-content/uploads/2017/05/6.html" alt="" width="150" height="150" /></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Mr. Ryan Yip</strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../../www.mytutorsource.com/wp-content/uploads/2016/05/verify6.html" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Master's in Accounting</strong><br /><strong>Dept of Accounting at MTS</strong></p>
</div></div>
<div id='slider_2287_slide08' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-image:url(_https_/dev.mytutorsource.com/wp-content/uploads/2015/02/MTS_Main-page_03.html); background-position:left top; background-size:contain; background-repeat:no-repeat; background-color:rgb(255, 255, 255); '><div><img class="wp-image-1924 size-thumbnail aligncenter" src="../../www.mytutorsource.com/wp-content/uploads/2017/05/7.html" alt="" width="150" height="150" /></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Ms. Cleo Wai </strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../../www.mytutorsource.com/wp-content/uploads/2016/05/verify6.html" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Master's in english Language </strong><br /><strong> Dept. of English at MTS</strong></p>
</div></div>
<div id='slider_2287_slide09' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-image:url(_https_/dev.mytutorsource.com/wp-content/uploads/2015/02/MTS_Main-page_03.html); background-position:left top; background-size:contain; background-repeat:no-repeat; background-color:rgb(238, 238, 238); '><div><img class="wp-image-1924 size-thumbnail aligncenter" src="../../www.mytutorsource.com/wp-content/uploads/2017/05/8.html" alt="" width="150" height="150" /></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Ms. Alice Harvey</strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../../www.mytutorsource.com/wp-content/uploads/2016/05/verify6.html" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Bachelor's in Chemical Engineering</strong><br /><strong>Dept. of Chemistry at MTS</strong></p>
</div></div>
<div id='slider_2287_slide10' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-image:url(_https_/dev.mytutorsource.com/wp-content/uploads/2015/02/MTS_Main-page_03.html); background-position:left top; background-size:contain; background-repeat:no-repeat; background-color:rgb(255, 255, 255); '><div><img class="wp-image-1925 size-thumbnail aligncenter" src="../../www.mytutorsource.com/wp-content/uploads/2016/04/Kashif-150x150.html" alt="" width="150" height="150" /></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Mr. Kashif Jibran</strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../../www.mytutorsource.com/wp-content/uploads/2016/05/verify6.html" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Ph.D. in Mathematics</strong><br /><strong>Dept. of Mathematics at MTS</strong></p>
</div></div>
<div id='slider_2287_slide11' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-image:url(_https_/dev.mytutorsource.com/wp-content/uploads/2015/02/MTS_Main-page_03.html); background-position:left top; background-size:contain; background-repeat:no-repeat; background-color:rgb(238, 238, 238); '><div><img class="wp-image-1926 size-thumbnail aligncenter" src="../../www.mytutorsource.com/wp-content/uploads/2016/04/katie-150x150.html" alt="" width="150" height="150" /></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Ms. Katie Leslove</strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../../www.mytutorsource.com/wp-content/uploads/2016/05/verify6.html" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Master's in Law</strong><br /><strong>Dept. of Law at MTS</strong></p>
</div></div>
<div id='slider_2287_slide12' class='sa_hover_container' style='padding:5% 5%; margin:0px 0%; background-image:url(_https_/dev.mytutorsource.com/wp-content/uploads/2015/02/MTS_Main-page_03.html); background-position:left top; background-size:contain; background-repeat:no-repeat; background-color:rgb(255, 255, 255); '><div><a href="../../www.mytutorsource.com/wp-content/uploads/2016/04/cara.html"><img class="aligncenter wp-image-1923 size-thumbnail" src="../../www.mytutorsource.com/wp-content/uploads/2016/04/cara-150x150.html" alt="" width="150" height="150" /></a></div>
<div>
<h3 style="text-align: center;"><span style="color: #33cccc;"><strong>Ms. Cara Lee</strong></span></h3>
<p><span style="color: #33cccc;"><strong><img class="aligncenter wp-image-1938" src="../../www.mytutorsource.com/wp-content/uploads/2016/05/verify6.html" alt="" width="226" height="30" /></strong></span></p>
</div>
<div>
<p style="text-align: center;"><strong>Master's in Sociology Sciences</strong><br /><strong>Dept. of Sociology at MTS</strong></p>
</div></div>
</div>
</div>
<script type='text/javascript'>
	jQuery(document).ready(function() {
		jQuery('#slider_2287').owlCarousel({
			responsive:{
				0:{ items:1 },
				480:{ items:2 },
				768:{ items:2 },
				980:{ items:3 },
				1200:{ items:3 },
				1500:{ items:3 }
			},
			autoplay : false,
			autoplayHoverPause : false,
			smartSpeed : 1000,
			fluidSpeed : 1000,
			autoplaySpeed : 1000,
			navSpeed : 1000,
			dotsSpeed : 1000,
			loop : true,
			nav : true,
			navText : ['',''],
			dots : true,
			responsiveRefreshRate : 200,
			slideBy : 1,
			mergeFit : true,
			autoHeight : false,
			mouseDrag : true,
			touchDrag : true
		});
		jQuery('#slider_2287').css('visibility', 'visible');
	});
</script>
</div></div><div class="fusion-fullwidth fullwidth-box fusion-fullwidth-6  fusion-parallax-none nonhundred-percent-fullwidth" style="border-color:#eae9e9;border-bottom-width: 0px;border-top-width: 0px;border-bottom-style: solid;border-top-style: solid;padding-bottom:20px;padding-top:20px;padding-left:px;padding-right:px;"><style type="text/css" scoped="scoped">.fusion-fullwidth-6 {
                            padding-left: px !important;
                            padding-right: px !important;
                        }</style><div class="fusion-row"></div></div><div class="fusion-fullwidth fullwidth-box fusion-fullwidth-7  fusion-parallax-none nonhundred-percent-fullwidth our-tutors" style="border-color:#eae9e9;border-bottom-width: 0px;border-top-width: 0px;border-bottom-style: solid;border-top-style: solid;padding-bottom:0px;padding-top:20px;padding-left:px;padding-right:px;"><style type="text/css" scoped="scoped">.fusion-fullwidth-7 {
                            padding-left: px !important;
                            padding-right: px !important;
                        }</style><div class="fusion-row"><div class="fusion-title title fusion-title-size-three custom_heading"><h3 class="title-heading-left">Get personalized classes from our Expert Tutors</h3><div class="title-sep-container"><div class="title-sep sep-single"></div></div></div><div class="fusion-sep-clear"></div><div class="fusion-separator sep-single" style="border-color:#e0dede;border-top-width:1px;margin-left: auto;margin-right: auto;margin-top:px;margin-bottom:30px;width:100%;max-width:60%;"></div><div class="fusion-one-third one_third fusion-layout-column fusion-spacing-yes" style="margin-top:0px;margin-bottom:20px;"><div class="fusion-column-wrapper"><div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-1 hover-type-none"> <img alt="" src="../../www.mytutorsource.com/wp-content/uploads/2015/11/MTS_Our-tutors_FINAL2_05.html" class="img-responsive"/></span></div><h3 style="text-align: center; color: #10adb3;">It’s Our Responsibility</h3>
<p style="text-align: center;">No need to go through hundreds of tutor profiles and then choose the right teacher. MTS selects the right tutor from its experts panel, who best suit your needs.</p>
<div class="fusion-clearfix"></div></div></div><div class="fusion-one-third one_third fusion-layout-column fusion-spacing-yes" style="margin-top:0px;margin-bottom:20px;"><div class="fusion-column-wrapper"><div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-2 hover-type-none"> <img alt="" src="../../www.mytutorsource.com/wp-content/uploads/2015/11/MTS_Our-tutors_FINAL2_08.html" class="img-responsive"/></span></div><h3 style="text-align: center; color: #10adb3;">Learn with the Best</h3>
<p style="text-align: center;">Our experts hold Master&#8217;s Degrees (or higher) in their respective subjects and have 10+ years of teaching experience.</p>
<div class="fusion-clearfix"></div></div></div><div class="fusion-one-third one_third fusion-layout-column fusion-column-last fusion-spacing-yes" style="margin-top:0px;margin-bottom:20px;"><div class="fusion-column-wrapper"><div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-3 hover-type-none"> <img alt="" src="../../www.mytutorsource.com/wp-content/uploads/2015/11/MTS_Our-tutors_FINAL2_031.html" class="img-responsive"/></span></div><h3 style="text-align: center; color: #10adb3;">We only Hire Professionals</h3>
<p style="text-align: center;">MTS educators are not some High School graduates but are expert professionals with proven track records. Our tutors have tons of teaching experience in their relevant subjects.</p>
<div class="fusion-clearfix"></div></div></div><div class="fusion-clearfix"></div><div class="fusion-one-sixth fusion-layout-column fusion-spacing-yes" style="margin-top:0px;margin-bottom:20px;"><div class="fusion-column-wrapper"><div class="fusion-clearfix"></div></div></div><div class="fusion-one-third one_third fusion-layout-column fusion-spacing-yes" style="margin-top:0px;margin-bottom:20px;" id="colourtutor"><div class="fusion-column-wrapper" style="padding:20px;"><div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-4 hover-type-none"> <img alt="" src="../../www.mytutorsource.com/wp-content/uploads/2015/11/MTS_Our-tutors_FINAL2_14.html" class="img-responsive"/></span></div><h3 style="text-align: center; color: #10adb3;">Worldwide Hiring</h3>
<p style="text-align: center;">We hire teachers from all across the globe, with the majority of them coming in from Asia region.</p>
<div class="fusion-clearfix"></div></div></div><div class="fusion-one-third one_third fusion-layout-column fusion-spacing-yes" style="margin-top:0px;margin-bottom:20px;" id="colourtutor"><div class="fusion-column-wrapper" style="padding:20px;"><div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-5 hover-type-none"> <img alt="" src="../../www.mytutorsource.com/wp-content/uploads/2015/11/MTS_Our-tutors_FINAL2_17.html" class="img-responsive"/></span></div><h3 style="text-align: center; color: #10adb3;">Up-to-Date Teaching Methodologies</h3>
<p style="text-align: center;">Our teachers are professionally trained lecturers. Whether you&#8217;re taking GCSE Edexcel Chemistry or IB Standard Level Mathematics, they are well aware of the techniques and tactics required to ace an exam.</p>
<div class="fusion-clearfix"></div></div></div><div class="fusion-one-sixth fusion-layout-column fusion-column-last fusion-spacing-yes" style="margin-top:0px;margin-bottom:20px;"><div class="fusion-column-wrapper"><div class="fusion-clearfix"></div></div></div><div class="fusion-clearfix"></div></div></div>
							</div>
																	</div>
					</div>
@endsection