@extends('website.layouts.base')
@section('content')
<input type="hidden" value="PK" id='hiddenchekuae'>	<div id="content" style="width: 100%;">
				<div id="post-1457" class="post-1457 page type-page status-publish hentry">
			<span class="entry-title" style="display: none;">Math Tuition</span><span class="vcard" style="display: none;"><span class="fn"><a href="../index.html" title="Posts by admin" rel="author">admin</a></span></span><span class="updated" style="display:none;">2018-07-30T12:05:20+00:00</span>																		<div class="post-content">
				<div class="fusion-fullwidth fullwidth-box fusion-fullwidth-1  fusion-parallax-none nonhundred-percent-fullwidth remove-spacing" style="border-color:#eae9e9;border-bottom-width: 0px;border-top-width: 0px;border-bottom-style: solid;border-top-style: solid;padding-bottom:0px;padding-top:60px;padding-left:px;padding-right:px;"><style type="text/css" scoped="scoped">.fusion-fullwidth-1 {
                            padding-left: px !important;
                            padding-right: px !important;
                        }</style><div class="fusion-row"><div class="fusion-title title fusion-title-size-three custom_heading"><h3 class="title-heading-left">Trusted Maths tutors for Junior Level, IGCSE, GCSE, A Level and IB</h3><div class="title-sep-container"><div class="title-sep sep-single"></div></div></div><div class="fusion-sep-clear"></div><div class="fusion-separator sep-single" style="border-color:#e0dede;border-top-width:1px;margin-left: auto;margin-right: auto;margin-top:px;margin-bottom:30px;width:100%;max-width:55%;"></div><p style="font-weight: 400;">Mathematics can be tough and we know about all the complications you face on a regular basis. From establishing your basic concepts to working on inefficient time management and careless mistakes during the exams, our tutors will help you ace your Maths papers! MTS provides professional educators for Mathematics of all levels. May that be additional Maths (GCSE, IGCSE), IB Standard or Higher Level Maths or Further Maths (A Level), we’ve got you covered!</p>
<p style="font-weight: 400;">For Maths tutoring, click on <a href="https://www.mytutorsource.com/get-started-for-free/">&#8220;Online Trial (Free!)&#8221;</a><span class="uae_cust_private_link"> or <a href="https://www.mytutorsource.com/high-quality-private-online-tutoring/">&#8220;Private Tutoring&#8221;</a>.</span></p>
<p class="custom_home_tutor_adword" style="font-weight: 400;">If you&#8217;re looking for a Maths home tutor, please click on <a href="../high-quality-home-tutoring/index.html">&#8220;Home Tutoring&#8221;</a></p>
<p class="custom_home_tutor_adword2" style="font-weight: 400;">If you&#8217;re looking for a Maths home tutor, please click on <a href="../high-quality-home-tutoring-3/index.html">&#8220;Home Tutoring&#8221;</a></p>
</div></div>
							</div>
																	</div>
					</div>
@endsection