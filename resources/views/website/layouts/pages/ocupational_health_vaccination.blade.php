@extends('website.layouts.base')
@section('styling')
<style type="text/css">
    .row {
        margin-left: unset !important;
    }
    h2, .h2 {
        font-size: 1.2rem;
    }
</style>
@endsection
@section('content')
<div class="container">
    @include('website.layouts.logo')
    <section class="section-padding div_with_white_back">
        <div class="section-header">
            <div class="section-heading">
                <h3 class="text-custom-black fw-700">Occupational Health Vaccination</h3>
            </div>
        </div>
        <div class="row">
            <div class="col_12">
                <p>There are some jobs that may require you to have occupational health vaccinations, to help protect you against certain diseases during the course of your work duties, e.g. hepatitis B. Your occupational health department should be able to advise you if you require any vaccinations. Alternatively, we can provide a risk assessment and advise what vaccinations are required for occupational health reasons. We are happy to discuss this with you at your consultation in our private vaccination clinic at Woodhouse Medical Practice Leeds.</p>
                <div class="dropdown_section">
                    <h2>What workplace vaccinations do we offer?</h2>
                    <div class="dropdown">
                        <h2>BCG vaccination</h2>
                        <div class="dropdown_content">
                            <p><strong>How many doses? </strong></p>
                            <p>1 dose</p>
                            <p><strong>How long does immunity last? </strong></p>
                            <p>No further boosters are recommended. Current studies sow immunity can last a minimum of 15 years and some have suggested up to 60 years.</p>
                            <p><strong>Who needs it? </strong></p>
                            <p>People at risk of exposure through work e.g. healthcare workers, veterinary workers, laboratory workers.</p>
                            <p><strong>Cost </strong></p>
                            <p>Dependent on availability</p>
                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>
                        </div>
                    </div>
                    <div class="dropdown">
                        <h2>Chickenpox vaccination</h2>
                        <div class="dropdown_content">
                            <p><strong>Dose</strong></p>
                            <p>2 doses</p>
                            <p>4-8 week interval</p>
                            <p><strong>How long after the vaccination course do I need a blood test</strong></p>
                            <p>6 weeks</p>
                            <p><strong>Cost</strong></p>
                            <p>£70 per dose</p>
                            <p>&nbsp;</p>
                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>
                        </div>
                    </div>
                    <div class="dropdown">
                        <h2>Diphtheria/Tetanus/Polio vaccination</h2>
                        <div class="dropdown_content">
                            <p><strong>Dose</strong></p>
                            <p>1 dose (as long as you have had a full course of&nbsp;immunisations)</p>
                            <p><strong>How long after the vaccination course do I need a blood test</strong></p>
                            <p>3 months</p>
                            <p><strong>Cost</strong></p>
                            <p>£35</p>
                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a>
                            </p>
                        </div>
                    </div>
                    <div class="dropdown">
                        <h2>Flu vaccination</h2>
                        <div class="dropdown_content">
                            <p><strong>Dose</strong></p>
                            <p>1 doses</p>
                            <p>&nbsp;</p>
                            <p><strong>Cost</strong></p>
                            <p>£15/£35</p>
                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>
                        </div>
                    </div>
                    <div class="dropdown">
                        <h2>Hepatitis B vaccination</h2>
                        <div class="dropdown_content">
                            <p><strong>Dose</strong></p>
                            <p>3 doses:</p>
                            <p>0,1 and 2 months</p>
                            <p>or</p>
                            <p>0,1 and 6 months</p>
                            <p><strong>How long after the vaccination course do I need a blood test</strong></p>
                            <p><span style="display: inline !important; float: none; background-color: transparent; color: #333333; cursor: text; font-family: Georgia,'Times New Roman','Bitstream Charter',Times,serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;">1-4 months</span></p>
                            <p>This is now strongly recommended under current healthcare guidance.</p>
                            <p><strong>Cost</strong></p>
                            <p>£45 per dose</p>
                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>     
                        </div>
                    </div>
                    <div class="dropdown">
                        <h2>MMR vaccination</h2>
                        <div class="dropdown_content">
                            <p><strong>Dose</strong></p>
                            <p>2 doses:</p>
                            <p>Minimum 1 month interval</p>
                            <p><strong>How long after the vaccination course do I need a blood test</strong></p>
                            <p>6 weeks</p>
                            <p><strong>Cost</strong></p>
                            <p>£40 per dose</p>
                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p> 
                        </div>
                    </div>
                    <div class="dropdown">
                        <h2>Meningitis ACWY vaccination</h2>
                        <div class="dropdown_content">
                            <p><strong>Dose</strong></p>
                            <p>1 dose</p>
                            <p><strong>How long after the vaccination course do I need a blood test</strong></p>
                            <p>6-8&nbsp;weeks</p>
                            <p><strong>Cost</strong></p>
                            <p>£60 per dose</p>
                            <p class="book">Book an appointment now - <a href="tel:01132213533">0113 221 3533</a> or <a href="tel:01138730242">0113 873 0242</a> - <a href="mailto:info@sfvaccinations.co.uk">info@sfvaccinations.co.uk</a></p>
                        </div>
                    </div>
                </div>
                <div class="dropdown_section">
                    <h2>Further information on the hepatitis B vaccine for occupational health reasons</h2>
                    <div class="dropdown">
                        <h2>Do I need hepatitis B for work?</h2>
                        <div class="dropdown_content">
                            <p>The Department of Health has identified the following occupational groups to be at an increased risk of exposure to blood-borne viruses (BBV) and recommends that they be may be immunised against hepatitis B:</p>
                            <ul>
                                <li>laboratory staff handling biological material that may be contaminated with BBV;</li>
                                <li>staff of residential and other accommodation for those with learning difficulties;</li>
                                <li>those handling cadavers and other human remains, such as anatomical pathology technologists (APTs), funeral directors, embalmers and pathologists;</li>
                                <li>prison service staff in regular contact with inmates; and</li>
                                <li>certain members of the emergency frontline services, such as the police, ambulance and fire and rescue services, may require vaccination, but only if local risk assessment indicates a need, and only if occupational health advice supports this requirement.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="dropdown">
                        <h2>How many hepatitis B vaccinations will I need?</h2>
                        <div class="dropdown_content">
                            <p>A course of 3 vaccinations is usually required over a period of 3 months or 6 months.</p>
                        </div>
                    </div>
                    <div class="dropdown">
                        <h2>Will I need a blood test?</h2>
                        <div class="dropdown_content">
                            <p>Yes.</p>
                            <p>All patients who have completed a course of hepatitis B vaccinations for occupational health reasons should have a hepatitis B surface antibody test done 1-4 months after completing the course of vaccinations. This is to check you have responded to the vaccination course and produced the adequate amount of antibodies.</p>
                            <p><span style="display: inline !important; float: none; background-color: transparent; color: #333333; cursor: text; font-family: Georgia,'Times New Roman','Bitstream Charter',Times,serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;">If antibodies have not developed, the hepatitis B vaccination course should be repeated. Our healthcare professional will discuss this with you.</span></p>
                            <p>We can arrange for a blood test at our private vaccination clinic. Please <a href="mailto:info@sfvaccinations.co.uk">email</a> or phone the clinic to ask for a telephone appointment and then we will ring you back to discuss your requirements.</p>
                        </div>
                    </div>
                    <div class="dropdown">
                        <h2>How long will my immunity last?</h2>
                        <div class="dropdown_content">
                            <p><span style="display: inline !important; float: none; background-color: transparent; color: #333333; cursor: text; font-family: Georgia,'Times New Roman','Bitstream Charter',Times,serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;">The </span><strong>immunity will last for a minimum of 20 years&nbsp;</strong><span style="display: inline !important; float: none; background-color: transparent; color: #333333; cursor: text; font-family: Georgia,'Times New Roman','Bitstream Charter',Times,serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;">if you have shown to have responded to the vaccination course.</span></p>
                        </div>
                    </div>
                </div>
            </div>
            <div style="clear:both;"></div>
        </div>
    </section>
    <section class="newsletter-style-1 section-padding-30 bg-light-blue">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="newsletter-content-wrapper mb-md-40">
                        <h3 class="text-custom-white fw-600">Booking</h3>
                        <p class="text-custom-white">You can book an appointment by:</p>
                        <p class="text-custom-white">Phoning 0113 2213533 or 0113 8730242</p>
                        <p class="text-custom-white">Emailing info@sfvaccinations.co.uk</p>
                        <a href="#" class="btn btn-warning">Book Online</a>
                    </div>
                </div>
                <!-- <div class="col-lg-6">
                    <div class="newsletter-form-wrapper">
                        <form>
                            <div class="input-group">
                                <input type="email" name="#" class="form-control form-control-custom" placeholder="Email Id">
                                <div class="input-group-append">
                                    <button type="submit">Subscribe</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div> -->
            </div>
        </div>
    </section>
    <section class="section-padding-30"></section>
</div>
@endsection