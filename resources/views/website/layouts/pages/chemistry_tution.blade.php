@extends('website.layouts.base')
@section('content')
<input type="hidden" value="PK" id='hiddenchekuae'>	<div id="content" style="width: 100%;">
				<div id="post-1735" class="post-1735 page type-page status-publish hentry">
			<span class="entry-title" style="display: none;">Chemistry Tuition</span><span class="vcard" style="display: none;"><span class="fn"><a href="../index.html" title="Posts by Aamir Azam" rel="author">Aamir Azam</a></span></span><span class="updated" style="display:none;">2018-07-30T12:15:46+00:00</span>																		<div class="post-content">
				<div class="fusion-fullwidth fullwidth-box fusion-fullwidth-1  fusion-parallax-none nonhundred-percent-fullwidth remove-spacing" style="border-color:#eae9e9;border-bottom-width: 0px;border-top-width: 0px;border-bottom-style: solid;border-top-style: solid;padding-bottom:0px;padding-top:60px;padding-left:px;padding-right:px;"><style type="text/css" scoped="scoped">.fusion-fullwidth-1 {
                            padding-left: px !important;
                            padding-right: px !important;
                        }</style><div class="fusion-row"><div class="fusion-title title fusion-title-size-three custom_heading"><h3 class="title-heading-left">Trusted Chemistry tutors for Junior Level, IGCSE, GCSE, A Level and IB</h3><div class="title-sep-container"><div class="title-sep sep-single"></div></div></div><div class="fusion-sep-clear"></div><div class="fusion-separator sep-single" style="border-color:#e0dede;border-top-width:1px;margin-left: auto;margin-right: auto;margin-top:px;margin-bottom:30px;width:100%;max-width:55%;"></div><p style="font-weight: 400;">Do chemical reactions and complex formulas confuse you? Well, you’re not the only one! We deal with all these confusions on a daily basis and we know how to cope up with them. Our expert Chemistry tutors will not only build your concepts but will give you detailed tips and techniques in order to ace your exams. They will make sure that you go through ample practice past papers so that you’re able to achieve high grades.</p>
<p style="font-weight: 400;">For Chemistry tutoring, click on <a href="https://www.mytutorsource.com/get-started-for-free/">&#8220;Online Trial (Free!)&#8221;</a><span  class='uae_cust_private_link'> or <a href="https://www.mytutorsource.com/high-quality-private-online-tutoring/">&#8220;Private Tutoring&#8221;.</a></p>
<p class="custom_home_tutor_adword" style="font-weight: 400;">If you&#8217;re looking for a Chemistry home tutor, please click on <a href="../high-quality-home-tutoring/index.html">&#8220;Home Tutoring&#8221;</a></p>
<p class="custom_home_tutor_adword2" style="font-weight: 400;">If you&#8217;re looking for a Chemistry home tutor, please click on <a href="../high-quality-home-tutoring-3/index.html">&#8220;Home Tutoring&#8221;</a></p>
</div></div>
							</div>
																	</div>
					</div>
@endsection