<nav id="sidebar" class="active">
    <div class="custom-new-menu">
        <button type="button" id="sidebarCollapse" class="btn btn-primary">
          <i class="fa fa-bars"></i>
          <span class="sr-only">Toggle Menu</span>
        </button>
    </div>
    <div class="p-4">
    <!-- <img src="https://www.sfvaccinations.co.uk/wp-content/themes/sfsv/images/logo.jpg">  -->
    <!-- <h1><a href="index.html" class="logo">SF <span>Screaning & Vaccination</span></a></h1> -->
    <ul class="list-unstyled components mb-5">
      <li class="active">
        <a href="{{ url('/') }}">Home</a>
      </li>
      <li>
          <a href="{{ url('covid-19-service-and-tests') }}">COVID-19 Tests and Services</a>
      </li>
      <li>
          <a href="{{ url('travel/clinic') }}">Travel clinic services</a>
      </li>
      <li>
      <a href="{{ url('corporate_service') }}">Corporate services</a>
      </li>
      <li>
          <a href="{{ url('private-vaccinations') }}">Private vaccination</a>
      </li>
      <li>
          <a href="{{ url('tests/tests-a-z/A') }}">Private blood tests</a>
      </li>
      <li>
      <a href="{{ url('occupational-health-services') }}">Occupational health services</a>
      </li>
      <li>
      <a href="{{ url('vitamin-deficiencies') }}">Vitamin deficeincies</a>
      </li>
      <li>
      <a href="{{ url('employee_health') }}"> Wellbeing service</a>
      </li>
      <li>
      <a href="{{ url('occupational-health-blood-test') }}">Occupational blood tests</a>
      </li>
      <li>
      <a href="{{ url('hyperhidrosis') }}">Hyperhidrosis treatment</a>
      </li>
      <li>
      <a href="{{ url('chronic-migrain') }}">Chronic migraine treatment</a>
      </li>
      <li>
      <a href="{{ url('contact_us') }}">Contact us</a>
      </li>
    </ul>
  </div>
</nav>