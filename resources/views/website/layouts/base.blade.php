<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta name="keywords" content="#">
        <meta name="description" content="#">
        <title>SF Vaccination | Homepage</title>
        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('sfvaccination/assets/images/favicon.ico') }}">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('sfvaccination/assets/images/favicon.ico') }}">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('sfvaccination/assets/images/favicon.ico') }}">
        <link rel="apple-touch-icon-precomposed" href="#">
        <link rel="shortcut icon" href="{{ asset('sfvaccination/assets/images/favicon.ico') }}">
        <!-- Bootstrap -->
        <link href="{{ asset('sfvaccination/assets/css/bootstrap.min.css') }}" rel="stylesheet">
        <!-- Fontawesome -->
        <link href="{{ asset('sfvaccination/assets/css/font-awesome.css') }}" rel="stylesheet">
        <!-- Flaticons -->
        <link href="{{ asset('sfvaccination/assets/css/font/flaticon.css') }}" rel="stylesheet">
        <!-- Slick Slider -->
        <link href="{{ asset('sfvaccination/assets/css/slick.css') }}" rel="stylesheet">
        <!-- Range Slider -->
        <link href="{{ asset('sfvaccination/assets/css/ion.rangeSlider.min.css') }}" rel="stylesheet">
        <!-- Datepicker -->
        <link href="{{ asset('sfvaccination/assets/css/datepicker.css') }}" rel="stylesheet">
        <!-- magnific popup -->
        <link href="{{ asset('sfvaccination/assets/css/magnific-popup.css') }}" rel="stylesheet">
        <!-- Nice Select -->
        <link href="{{ asset('sfvaccination/assets/css/nice-select.css') }}" rel="stylesheet">
        <!-- Custom Stylesheet -->
        <link href="{{ asset('sfvaccination/assets/css/style.css') }}" rel="stylesheet">
        <!-- Custom Responsive -->
        <link href="{{ asset('sfvaccination/assets/css/responsive.css') }}" rel="stylesheet">
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">

        <!-- <title>Sidebar 05</title> -->
        <!-- <meta charset="utf-8"> -->
        <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->

        <!-- <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet"> -->
            
            <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> -->
        <link rel="stylesheet" href="{{ asset('css/website/wordpress.css') }}">
        <link rel="stylesheet" href="{{ asset('sfvaccination/css/style.css') }}">
        @yield('styling')
    </head>
    <body class="product-template-default single single-product postid-67728 tm-sidebar-a-right tm-sidebars-1 tm-noblog wp-product-single wp-cat-191775 wp-cat-231442 tm-article-blank theme-city_theme woocommerce woocommerce-page woocommerce-js wcz-woocommerce">
        <div class="wrap">
            <div class="container-fluid">
                <div class="row justify-content-between">
                    <div class="col d-flex justify-content-start">
                        <p class="mb-0 phone contact_details"><span class="fa fa-phone mr-3"></span> <a href="#">0113 221 3533</a></p>
                        <p class="mb-0 phone contact_details"><span class="fa fa-phone mr-3"></span> <a href="#">0113 873 0242</a></p>
                        <p class="mb-0 phone contact_details"><span class="fa fa-mobile mr-3"></span> <a href="#">07597 028245</a></p>
                        <p class="mb-0 phone contact_details"><span class="fa fa-envelope mr-3"></span><a href="#">info@sfvaccinations.co.uk</a></p>
                        <p class="mb-0 phone contact_details"><span class="fa fa-book mr-3"></span><a href="#">Book online</a></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper d-flex align-items-stretch">
            @include('website.layouts.header')    
        <!-- Page Content  -->
            <div id="content" class="p-4 p-md-5 pt-5">
                <div class="main-body">
                    <!-- Start Content -->
                    @yield('content')
                    <!-- Start Footer -->
                    @include('website.layouts.footer')
                    <!-- End Footer -->
                </div>
             </div>
        </div>
    <!-- End Main Body -->
    <!-- Place all Scripts Here -->
    <!-- jQuery -->
    <script src="{{ asset('sfvaccination/assets/js/jquery.min.js') }}"></script>
    <!-- Popper -->
    <script src="{{ asset('sfvaccination/assets/js/popper.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('sfvaccination/assets/js/bootstrap.min.js') }}"></script>
    <!-- Range Slider -->
    <script src="{{ asset('sfvaccination/assets/js/ion.rangeSlider.min.js') }}"></script>
    <!-- Slick Slider -->
    <script src="{{ asset('sfvaccination/assets/js/slick.min.js') }}"></script>
    <!-- Datepicker -->
    <script src="{{ asset('sfvaccination/assets/js/datepicker.js') }}"></script>
    <script src="{{ asset('sfvaccination/assets/js/datepicker.en.js') }}"></script>
    <!-- Nice Select -->
    <script src="{{ asset('sfvaccination/assets/js/jquery.nice-select.js') }}"></script>
    <!-- Nice Select -->
    <script src="{{ asset('sfvaccination/assets/js/particles.js') }}"></script>
    <!-- Magnific Popup -->
    <script src="{{ asset('sfvaccination/assets/js/jquery.magnific-popup.min.js') }}"></script>
    <!-- Google Map -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDnd9JwZvXty-1gHZihMoFhJtCXmHfeRQg"></script>
    <!-- Isotope Gallery -->
    <script src="{{ asset('sfvaccination/assets/js/isotope.pkgd.min.js') }}"></script>
    <!-- Custom Js -->
    <script src="{{ asset('sfvaccination/assets/js/custom.js') }}"></script>
    <!-- /Place all Scripts Here -->
    <script src="{{ asset('sfvaccination/js/main.js') }}"></script>
    @yield('script')
    </body>
</html>






 

 