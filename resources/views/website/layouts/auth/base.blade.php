
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link type="text/css" rel="stylesheet" href="{{ asset('lightbox/assests/theme.cssab55.html?ln=primefaces-aristo') }}" />


    <title>Login | My Tutor Source</title>



    <link href="{{ asset('lightbox/assests/browser-reset344a.css?dt=06062016') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('lightbox/assests/skin-frontend344a.css?dt=06062016') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('lightbox/assests/main344a.css?dt=06062016') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('lightbox/assests/responsive344a.css?dt=06062016') }}" rel="stylesheet" type="text/css" />

    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,600,400italic" rel="stylesheet"
        type="text/css" />
    <link href="http://fonts.googleapis.com/css?family=Arvo" rel="stylesheet" type="text/css" />
    <link href="{{ asset('make_payment/updated_custom_css.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/website/auth.css') }}" rel="stylesheet" type="text/css" />
</head>

<body class="frontend">
    <div id="container">
        <div class="clear"></div>
        <div id="allcontent" style="padding-top: 0px;">
            @yield('content')
        </div>
    </div>

    <script type='text/javascript' src="{{ asset('lightbox/assests/jquery-1.12.4.min.js') }}"></script>
    <script type='text/javascript'>
    $(document).keyup(function(e) {
        if (e.which == 27) {
            window.location.href = "{{url('/')}}";
        }
    });



    $(document).ready(function() {



        $("#loginform_btn").click(function() {

            var email = $("#parentsignupform_email").val();
            var password = $("#parentsignupform_password").val();

            if (email.length < 1) {
                $("#email_err").fadeIn(500);
                return false;
            }




            function isValidEmailAddress(email) {
                var pattern =
                    /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
                return pattern.test(email);
            };


            if (!isValidEmailAddress(email)) {
                $("#email_err2").fadeIn(500);
                return false;
            }



            if (password.length < 1) {
                $("#password_err").fadeIn(500);
                return false;
            }



            $("#loadericon").show();
            $.ajax({
                type: "POST",
                url: "{{url('login_account')}}",
                data: "email=" + email + "&password=" + password + "&_token={{ csrf_token() }}",
                success: function(msg) { //alert(msg);
                    if (msg.status) {
                        window.location.href = "{{url('user/profile')}}";
                    } else {
                        $("#loadericon").hide();
                        $("#login_err").fadeIn(500);
                        return false;
                    }
                }

            });


        });

        $("#add_students").click(function() {
            var st_hidden = $("#st_hidden").val();
            st_hidden++;
            $("#st_hidden").val(st_hidden);


            var html_date =
                "<div style='width: calc(50% - 10px); float: left; margin-right: 20px'><span id='add_st_fname'><span id='' class='inputRegion'><label id='' class='label' for='parentsignupform:firstname:input'>First name<span class='required'>*</span></label><span class='input'><input id='parentsignupform_firstname_" +
                st_hidden +
                "' name='parentsignupform:firstname:input' type='text' size='0' style='' aria-required='true' class='ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all'><div id='f_name_err_" +
                st_hidden +
                "' class='errors_final'><span class='error errors'>First name is required.</span><div class='clear'></div></div></span></span></span></div><div style='width: calc(50% - 10px); float: left'><span id='add_st_lname'><span id='' class='inputRegion'><label id='' class='label' for='parentsignupform:lastname:input'>Last name<span class='required'>*</span></label><span class='input'><input id='parentsignupform_lastname_" +
                st_hidden +
                "' name='parentsignupform:lastname:input' type='text' size='0' style='' aria-required='true' class='ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all  '> <div id='l_name_err_" +
                st_hidden +
                "' class='errors_final'><span class='error errors'>Last name is required.</span><div class='clear'></div></div></span></span></span></div>";
            $("#st_1").append(html_date);


        });

    });
    </script>
     @yield('js')
</body>


<!-- Mirrored from dev.mytutorsource.com/account-login/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 16 Jul 2020 06:14:48 GMT -->
</html>