@extends('website.layouts.auth.base')
@section('content')
	<div class="signupbackground"><img src="../lightbox/assests/findtutor2.jpg" /></div>
            <div class="inner center" style="margin-top: 100px">
                <div class="col signup lightgreyfill">
                    <div>
                        <a href="{{ url('/') }}">
		                    <img src="{{ asset('wp-content/uploads/2018/04/Floating-logo1.png') }}"
		                        style="max-width: 160px; margin-bottom: 20px;" />
		                </a>
                    </div>
                    <h3 class="font2 main_headings" style="margin-bottom: 30px">Create your free account<br />
                        <span style="font-size: 0.6em;" class="main_headings2">It's free to contact and meet
                            tutors</span>
                    </h3>

                    <div id="parentsignupform:j_id_4s" class="ui-messages ui-widget" aria-live="polite"></div>

                    <div style="width: calc(50% - 10px); float: left; margin-right: 20px">
                        <span id="parentsignupform:firstname:firstname">
                            <span id="parentsignupform:firstname:container" class="inputRegion">
                                <label id="parentsignupform:firstname:label" class="label labelsheading"
                                    for="parentsignupform:firstname:input">First name<span
                                        class="required">*</span></label>
                                <span class="input">
                                    <input id="parentsignupform_firstname" name="parentsignupform:firstname:input"
                                        type="text" size="0" style="" aria-required="true"
                                        onkeypress="return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)"
                                        class="ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all  " />

                                    <div id="f_name_err" class='errors_final'>
                                        <span class="error errors">First name is required.</span>
                                        <div class="clear"></div>
                                    </div>

                                    <div id="f_name_err2" class='errors_final'>
                                        <span class="error errors">Numbers are not allowed.</span>
                                        <div class="clear"></div>
                                    </div>

                                </span>
                            </span>
                        </span>
                    </div>

                    <div style="width: calc(50% - 10px); float: left">
                        <span id="parentsignupform:lastname:lastname">
                            <span id="parentsignupform:lastname:container" class="inputRegion">
                                <label id="parentsignupform:lastname:label" class="label labelsheading"
                                    for="parentsignupform:lastname:input">Last name<span
                                        class="required">*</span></label>
                                <span class="input">
                                    <input id="parentsignupform_lastname" name="parentsignupform:lastname:input"
                                        type="text" size="0" style="" aria-required="true"
                                        onkeypress="return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)"
                                        class="ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all  " />

                                    <div id="l_name_err" class='errors_final'>
                                        <span class="error errors">Last name is required.</span>
                                        <div class="clear"></div>
                                    </div>

                                    <div id="l_name_err2" class='errors_final'>
                                        <span class="error errors">Numbers are not allowed.</span>
                                        <div class="clear"></div>
                                    </div>
                                </span>
                            </span>
                        </span>
                    </div>
                    <div class="clear"></div>
                    <span id="parentsignupform:email:email">
                        <span id="parentsignupform:email:container" class="inputRegion">
                            <label id="parentsignupform:email:label" class="label labelsheading"
                                for="parentsignupform:email:input">Email address<span class="required">*</span></label>
                            <span class="input">
                                <input id="parentsignupform_email" name="parentsignupform:email:input" type="email"
                                    size="0" style="" aria-required="true"
                                    class="ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all" />
                                <div class="inputfieldinfo privacy">
                                    <p>All contact details will be kept confidential.</p>
                                </div>

                                <div id="email_err" class='errors_final'>
                                    <span class="error errors">Email is required.</span>
                                    <div class="clear"></div>
                                </div>
                                <div id="email_err2" class='errors_final'>
                                    <span class="error errors">Email format is invalid.</span>
                                    <div class="clear"></div>
                                </div>

                                <div id="email_exits_err" class='errors_final'>
                                    <span class="error errors">Email already exists.</span>
                                    <div class="clear"></div>
                                </div>


                            </span></span></span>

                    <div class="clear"></div>
                    <span id="parentsignupform:password">
                        <div id="parentsignupform:container" style="position: relative" class="inputRegion">
                            <label id="parentsignupform:label" class="label labelsheading"
                                for="parentsignupform:passwordinput">Password<span class="required">*</span></label>
                            <span class="input">
                                <input id="parentsignupform_password" name="parentsignupform:passwordinput"
                                    type="password"
                                    class="ui-inputfield ui-password ui-widget ui-state-default ui-corner-all "
                                    autocomplete="off" aria-required="true" />
                                <div id="password_err" class='errors_final'>
                                    <span class="error errors">Password is required.</span>
                                    <div class="clear"></div>
                                </div>
                                <div id="password_err_six" class='errors_final'>
                                    <span class="error errors">Password should be six characters.</span>
                                    <div class="clear"></div>
                                </div>


                            </span>
                            <div class="inputfieldinfo">
                                <p>Use at least six characters and one number.</p>

                            </div>

                            <div class="clear"></div>
                            <span id="parentsignupform:password">
                                <div id="parentsignupform:container" style="position: relative" class="inputRegion">
                                    <label id="parentsignupform:label" class="label labelsheading"
                                        for="parentsignupform:passwordinput">Confirm Password<span
                                            class="required">*</span></label>
                                    <span class="input">
                                        <input id="parentsignupform_password2" name="parentsignupform:password_confirmation"
                                            type="password"
                                            class="ui-inputfield ui-password ui-widget ui-state-default ui-corner-all "
                                            autocomplete="off" aria-required="true" />
                                        <div id="password_err2" class='errors_final'>
                                            <span class="error errors">Password is required.</span>
                                            <div class="clear"></div>
                                        </div>
                                        <div id="password_err_confirm" class='errors_final'>
                                            <span class="error errors">Password do not match.</span>
                                            <div class="clear"></div>
                                        </div>
                                        <div id="password_err_number" class='errors_final'>
                                            <span class="error errors">Use atleast One number.</span>
                                            <div class="clear"></div>
                                        </div>

                                    </span>
                                    <div class="inputfieldinfo">
                                        <p>Use at least six characters and one number.</p>

                                    </div>


                                    <div id="parentsignupform:message" class="error errors"></div>
                                </div>
                            </span>

                            <div class="clear"></div>
                            <span id="parentsignupform:phone:phone">
                                <span id="parentsignupform:phone:container" class="inputRegion"><label
                                        id="parentsignupform:phone:label" class="label labelsheading"
                                        for="parentsignupform:phone:input">Telephone number<span
                                            class="required">*</span></label>
                                    <span class="input"><input id="parentsignupform_telephone"
                                            name="parentsignupform:phone:input" type="tel" size="0" style=""
                                            aria-required="true"
                                            class="ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all  " />
                                        <div class="inputfieldinfo">
                                            <p>Some things are better sorted with a chat.</p>
                                        </div>
                                        <div id="telephone_err" class='errors_final'>
                                            <span class="error errors">Telephone is required.</span>
                                            <div class="clear"></div>
                                        </div>
                                    </span></span>


                                <div class="clear"></div>
                                <!-- <span id="parentsignupform:refercode:container" class="inputRegion"><label
                                            id="parentsignupform:refercode:label" class="label labelsheading"
                                            for="parentsignupform:refercode:input">Referral Code</label>
                                        <span class="input"><input id="parentsignupform_refercode"
                                                name="parentsignupform:refercode:input" type="text" size="0" style=""
                                                aria-required="true"
                                                class="ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all" />
                                            <div class="inputfieldinfo">
                                                <p>Earn money by reffering students.</p>
                                            </div>
                                            <div id="refferal_err" class='errors_final'>
                                                <span class="error errors">Invalid Referral Code.</span>
                                                <div class="clear"></div>
                                            </div>
                                        </span>
										</span> -->
                                <span id="parentsignupform:location:location">
                                    <span id="parentsignupform:location:container" class="inputRegion"><label
                                            id="parentsignupform:location:label" class="label labelsheading"
                                            for="parentsignupform:location:input">Location<span
                                                class="required">*</span></label>
                                        <span class="select">
                                            <select id="parentsignupform_location"
                                                name="parentsignupform:location:input"
                                                style="width: 100%;font-size: 1em;background: #ffffff;border-radius: 4px;border: 1px solid #cccccc;height: 38px;padding: 5px;color: inherit;margin-bottom: 5px;"
                                                aria-required="true"
                                                class="ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all">
                                                <option value="AF">Afghanistan</option>
                                                <option value="AL">Albania</option>
                                                <option value="DZ">Algeria</option>
                                                <option value="AD">Andorra</option>
                                                <option value="AO">Angola</option>
                                                <option value="AG">Antigua and Barbuda</option>
                                                <option value="AR">Argentina</option>
                                                <option value="AM">Armenia</option>
                                                <option value="AU">Australia</option>
                                                <option value="AT">Austria</option>
                                                <option value="AZ">Azerbaijan</option>
                                                <option value="BS">Bahamas</option>
                                                <option value="BH">Bahrain</option>
                                                <option value="BD">Bangladesh</option>
                                                <option value="BB">Barbados</option>
                                                <option value="BY">Belarus</option>
                                                <option value="BE">Belgium</option>
                                                <option value="BZ">Belize</option>
                                                <option value="BJ">Benin</option>
                                                <option value="BT">Bhutan</option>
                                                <option value="BO">Bolivia (Plurinational State of)</option>
                                                <option value="BA">Bosnia and Herzegovina</option>
                                                <option value="BW">Botswana</option>
                                                <option value="BR">Brazil</option>
                                                <option value="BN">Brunei Darussalam</option>
                                                <option value="BG">Bulgaria</option>
                                                <option value="BF">Burkina Faso</option>
                                                <option value="BI">Burundi</option>
                                                <option value="KH">Cambodia</option>
                                                <option value="CM">Cameroon</option>
                                                <option value="CA">Canada</option>
                                                <option value="CV">Cape Verde</option>
                                                <option value="CF">Central African Republic</option>
                                                <option value="TD">Chad</option>
                                                <option value="CL">Chile</option>
                                                <option value="CN">China</option>
                                                <option value="CO">Colombia</option>
                                                <option value="KM">Comoros</option>
                                                <option value="CG">Congo</option>
                                                <option value="CR">Costa Rica</option>
                                                <option value="CI">C么te DIvoire</option>
                                                <option value="HR">Croatia</option>
                                                <option value="CU">Cuba</option>
                                                <option value="CY">Cyprus</option>
                                                <option value="CZ">Czech Republic</option>
                                                <option value="KP">Democratic Peoples Republic of Korea</option>
                                                <option value="CD">Democratic Republic of the Congo&nbsp;</option>
                                                <option value="DK">Denmark</option>
                                                <option value="DJ">Djibouti</option>
                                                <option value="DM">Dominica</option>
                                                <option value="DO">Dominican Republic</option>
                                                <option value="EC">Ecuador</option>
                                                <option value="EG">Egypt</option>
                                                <option value="SV">El Salvador</option>
                                                <option value="GQ">Equatorial Guinea</option>
                                                <option value="ER">Eritrea</option>
                                                <option value="EE">Estonia</option>
                                                <option value="ET">Ethiopia</option>
                                                <option value="FJ">Fiji</option>
                                                <option value="FI">Finland</option>
                                                <option value="FR">France</option>
                                                <option value="GA">Gabon</option>
                                                <option value="GM">Gambia</option>
                                                <option value="GE">Georgia</option>
                                                <option value="DE">Germany</option>
                                                <option value="GH">Ghana</option>
                                                <option value="GR">Greece</option>
                                                <option value="GD">Grenada</option>
                                                <option value="GT">Guatemala</option>
                                                <option value="GN">Guinea</option>
                                                <option value="GW">Guinea Bissau</option>
                                                <option value="GY">Guyana</option>
                                                <option value="HK">Hong Kong</option>
                                                <option value="HT">Haiti</option>
                                                <option value="HN">Honduras</option>
                                                <option value="HU">Hungary</option>
                                                <option value="IS">Iceland</option>
                                                <option value="IN">India</option>
                                                <option value="ID">Indonesia</option>
                                                <option value="IR">Iran (Islamic Republic of)</option>
                                                <option value="IQ">Iraq</option>
                                                <option value="IE">Ireland</option>
                                                <option value="IL">Israel</option>
                                                <option value="IT">Italy</option>
                                                <option value="JM">Jamaica</option>
                                                <option value="JP">Japan</option>
                                                <option value="JO">Jordan</option>
                                                <option value="KZ">Kazakhstan</option>
                                                <option value="KE">Kenya</option>
                                                <option value="KI">Kiribati</option>
                                                <option value="KW">Kuwait</option>
                                                <option value="KG">Kyrgyzstan</option>
                                                <option value="LA">Lao People鈥檚 Democratic Republic</option>
                                                <option value="LV">Latvia</option>
                                                <option value="LB">Lebanon</option>
                                                <option value="LS">Lesotho</option>
                                                <option value="LR">Liberia</option>
                                                <option value="LY">Libya</option>
                                                <option value="LI">Liechtenstein</option>
                                                <option value="LT">Lithuania</option>
                                                <option value="LU">Luxembourg</option>
                                                <option value="MK">Macedonia</option>
                                                <option value="MG">Madagascar</option>
                                                <option value="MW">Malawi</option>
                                                <option value="MY">Malaysia</option>
                                                <option value="MV">Maldives</option>
                                                <option value="ML">Mali</option>
                                                <option value="MT">Malta</option>
                                                <option value="MH">Marshall Islands</option>
                                                <option value="MR">Mauritania</option>
                                                <option value="MU">Mauritius</option>
                                                <option value="MX">Mexico</option>
                                                <option value="FM">Micronesia (Federated States of)</option>
                                                <option value="MC">Monaco</option>
                                                <option value="MN">Mongolia</option>
                                                <option value="ME">Montenegro</option>
                                                <option value="MA">Morocco</option>
                                                <option value="MZ">Mozambique</option>
                                                <option value="MM">Myanmar</option>
                                                <option value="NA">Namibia</option>
                                                <option value="NR">Nauru</option>
                                                <option value="NP">Nepal</option>
                                                <option value="NL">Netherlands</option>
                                                <option value="NZ">New Zealand</option>
                                                <option value="NI">Nicaragua</option>
                                                <option value="NE">Niger</option>
                                                <option value="NG">Nigeria</option>
                                                <option value="NO">Norway</option>
                                                <option value="OM">Oman</option>
                                                <option value="PK">Pakistan</option>
                                                <option value="PW">Palau</option>
                                                <option value="PA">Panama</option>
                                                <option value="PG">Papua New Guinea</option>
                                                <option value="PY">Paraguay</option>
                                                <option value="PE">Peru</option>
                                                <option value="PH">Philippines</option>
                                                <option value="PL">Poland</option>
                                                <option value="PT">Portugal</option>
                                                <option value="QA">Qatar</option>
                                                <option value="KR">Republic of Korea</option>
                                                <option value="MD">Republic of Moldova</option>
                                                <option value="RO">Romania</option>
                                                <option value="RU">Russian Federation</option>
                                                <option value="RW">Rwanda</option>
                                                <option value="KN">Saint Kitts and Nevis</option>
                                                <option value="LC">Saint Lucia</option>
                                                <option value="VC">Saint Vincent and the Grenadines</option>
                                                <option value="WS">Samoa</option>
                                                <option value="SM">San Marino</option>
                                                <option value="ST">Sao Tome and Principe</option>
                                                <option value="SA">Saudi Arabia</option>
                                                <option value="SN">Senegal</option>
                                                <option value="RS">Serbia</option>
                                                <option value="SC">Seychelles</option>
                                                <option value="SL">Sierra Leone</option>
                                                <option value="SG">Singapore</option>
                                                <option value="LK">SriLanka</option>
                                                <option value="SK">Slovakia</option>
                                                <option value="SI">Slovenia</option>
                                                <option value="SB">Solomon Islands</option>
                                                <option value="SO">Somalia</option>
                                                <option value="ZA">South Africa</option>
                                                <option value="KR">South Korea</option>
                                                <option value="SS">South Sudan</option>
                                                <option value="ES">Spain</option>
                                                <option value="LK">Sri Lanka</option>
                                                <option value="SD">Sudan</option>
                                                <option value="SR">Suriname</option>
                                                <option value="SZ">Swaziland</option>
                                                <option value="SE">Sweden</option>
                                                <option value="CH">Switzerland</option>
                                                <option value="SY">Syrian Arab Republic</option>
                                                <option value="TJ">Tajikistan</option>
                                                <option value="TZ">Tanzania</option>
                                                <option value="TH">Thailand</option>
                                                <option value="TL">Timor-Leste</option>
                                                <option value="TG">Togo</option>
                                                <option value="TO">Tonga</option>
                                                <option value="TT">Trinidad and Tobago</option>
                                                <option value="TN">Tunisia</option>
                                                <option value="TR">Turkey</option>
                                                <option value="TM">Turkmenistan</option>
                                                <option value="TV">Tuvalu</option>
                                                <option value="UG">Uganda</option>
                                                <option value="UA">Ukraine</option>
                                                <option value="AE">United Arab Emirates</option>
                                                <option value="GB">United Kingdom</option>
                                                <option value="US">United States of America</option>
                                                <option value="UY">Uruguay</option>
                                                <option value="UZ">Uzbekistan</option>
                                                <option value="VU">Vanuatu</option>
                                                <option value="VE">Venezuela (Bolivarian Republic of)</option>
                                                <option value="VN">Viet Nam</option>
                                                <option value="YE">Yemen</option>
                                                <option value="ZM">Zambia</option>
                                                <option value="ZW">Zimbabwe</option>
                                            </select>
                                            <div id="location_err" class="errors_final">
                                                <span class="error errors">Location is required.</span>
                                                <div class="clear"></div>
                                            </div>
                                        </span>
                                    </span>
                                </span>
                                <div class="clear spacer"></div>
                                <div id='loadericon' style='display:none;text-align: center;'><img
                                        src='../make_payment/495.gif'></div>
                                <button id="parentsignupform_btn" name="parentsignupform:j_id_6w"
                                    class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button cusombtn"
                                    style="width: 100%" type="button"><span class="ui-button-text ui-c">Get
                                        started</span></button>
                                <!-- <p class="terms">By signing up you agree to our <a target="_blank" href="/terms-and-condition-of-use/" title="Terms and conditions">terms of use</a></p> --><input
                                    type="hidden" name="parentsignupform_SUBMIT" value="1" /><input type="hidden"
                                    name="javax.faces.ViewState" id="j_id__v_0:javax.faces.ViewState:1" value="aaa" />
                                <hr style="margin-top: 30px; margin-bottom: 20px;" />
                                <p>Already have an account? <a href="{{url('login_account')}}">Log in </a></p>

                        </div>
                </div>
            </div>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready(function(){
        $("#parentsignupform_btn").click(function() {
            var email = $("#parentsignupform_email").val();
            var password = $("#parentsignupform_password").val();
            var first_name = $("#parentsignupform_firstname").val();
            var last_name = $("#parentsignupform_lastname").val();
            var password_confirmation = $("#parentsignupform_password2").val();
            var phone = $("#parentsignupform_telephone").val();
            var location = $("#parentsignupform_location").val();

            if (first_name.length < 1) {
                $("#f_name_err").fadeIn(500);
                return false;
            }
            console.log(last_name.length);
            if (last_name.length < 1) {
                $("#l_name_err").fadeIn(500);
                return false;
            }

            if (email.length < 1) {
                $("#email_err").fadeIn(500);
                return false;
            }

            if (password_confirmation.length < 1) {
                $("#password_err2").fadeIn(500);
                return false;
            }

            if (password !== password_confirmation) {
                $("#password_err_confirm").fadeIn(500);
                return false;
            }

            if (phone.length < 1) {
                $("#telephone_err").fadeIn(500);
                return false;
            }

            if (location.length < 1) {
                $("#location_err").fadeIn(500);
                return false;
            }

            function isValidEmailAddress(email) {
                var pattern =
                    /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
                return pattern.test(email);
            };

            if (!isValidEmailAddress(email)) {
                $("#email_err2").fadeIn(500);
                return false;
            }

            if (password.length < 1) {
                $("#password_err").fadeIn(500);
                return false;
            }

            $("#loadericon").show();

            let record = "first_name="+ first_name + "&last_name="+ last_name + "&phone="+ phone + "&password_confirmation="+ password_confirmation + "&location="+ location + "&user_type="+ 1 + "&email=" + email + "&password=" + password + "&_token={{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{url('register_account')}}",
                data: record,
                success: function(msg) { //alert(msg);
                    if (msg.status) {
                        window.location.href = "{{url('login_account')}}";
                    } else {
                        $("#loadericon").hide();
                        $("#login_err").fadeIn(500);
                        return false;
                    }
                }

            });
        })
    })
</script>
@endsection