@extends('website.layouts.auth.base')
@section('content')
<div class="signupbackground"><img src="{{ asset('lightbox/assests/findtutor2.jpg') }}" /></div>
    <div class="inner center" style="margin-top: 100px">
        <div class="col signup lightgreyfill">
            <div>
                <a href="{{ url('/') }}">
                    <img src="{{ asset('wp-content/uploads/2018/04/Floating-logo1.png') }}"
                        style="max-width: 160px; margin-bottom: 20px;" />
                </a>
            </div>
            <h3 class="font2 main_headings custombhtn_heading" style="margin-bottom: 30px">Login<br /></h3>
            <div id="parentsignupform:j_id_4s" class="ui-messages ui-widget" aria-live="polite"></div>

            <div class="clear"></div>
            <span id="parentsignupform:email:email">
                <span id="parentsignupform:email:container" class="inputRegion">
                    <label id="parentsignupform:email:label" class="label labelsheading"
                        for="parentsignupform:email:input">Email address<span class="required">*</span></label>
                    <span class="input">
                        <input id="parentsignupform_email" name="parentsignupform:email:input" type="email"
                            size="0" style="" aria-required="true"
                            class="ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all  " />

                        <div class="inputfieldinfo privacy">
                            <p>All contact details will be kept confidential.</p>
                        </div>

                        <div id="email_err" class='errors_final'>
                            <span class="error errors">Email is required.</span>
                            <div class="clear"></div>
                        </div>
                        <div id="email_err2" class='errors_final'>
                            <span class="error errors">Email format is invalid.</span>
                            <div class="clear"></div>
                        </div>
                    </span></span></span>

            <div class="clear"></div>
            <span id="parentsignupform:password">
                <div id="parentsignupform:container" style="position: relative" class="inputRegion">
                    <label id="parentsignupform:label" class="label labelsheading"
                        for="parentsignupform:passwordinput">Password<span class="required">*</span></label>
                    <span class="input">
                        <input id="parentsignupform_password" name="parentsignupform:passwordinput"
                            type="password"
                            class="ui-inputfield ui-password ui-widget ui-state-default ui-corner-all "
                            autocomplete="off" aria-required="true" />
                        <div id="password_err" class='errors_final'>
                            <span class="error errors">Password is required.</span>
                            <div class="clear"></div>
                        </div>

                    </span>
                    <div class="inputfieldinfo">
                        <p>Use at least six characters and one number.</p>

                    </div>
                    <div id="parentsignupform:message" class="error errors"></div>
                </div>
            </span>

            <div class="clear"></div>





            <div class="clear spacer"></div>
            <button id="loginform_btn" name="parentsignupform:j_id_6w"
                class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only button cusombtn"
                style="width: 100%" type="button"><span class="ui-button-text ui-c">Login</span></button>
            <div id='loadericon' style='display:none'><img src='../make_payment/495.gif'></div>
            <span class="error errors" id='login_err' style='display:none'>Email or Password is invalid</span>

            <hr style="margin-top: 30px; margin-bottom: 20px;" />

            <p>Forget Password <a href="{{url('forget_password')}}">Click here </a></p>
            <p>Do not have a account? <a href="{{url('registeration_account')}}">Signup </a></p>


        </div>
    </div>
    <div class="clear"></div>
@endsection