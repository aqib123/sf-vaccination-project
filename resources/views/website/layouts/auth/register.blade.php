@extends('website.layouts.auth.base')
@section('content')
	<div class="signupbackground"><img src="../wp-content/uploads/2018/04/findtutor2.jpg" /></div>
	<div class="inner center" style="margin-top: 200px">
		<div class="col signup lightgreyfill" style="padding: 30px 50px;">
			<div class="center">
				<a href="{{ url('/') }}">
                    <img src="{{ asset('wp-content/uploads/2018/04/Floating-logo1.png') }}"
                        style="max-width: 160px; margin-bottom: 20px;" />
                </a>
			</div>
			<h3 class="font2 main_headings custombhtn_heading" style="margin-bottom: 30px">Signup</h3>
			<a class="button darkskybluefill cusombtn" href="{{url('signup_parent')}}" style="width: 100%; margin-bottom: 10px;">I'm a parent</a> 
			<a class="button cusombtn" href="{{url('signup_student')}}" style="width: 100%; margin-top: 0px">I'm a student</a>
			<hr style="margin-top: 30px; margin-bottom: 20px;">
			<p>Already have an account? <a href="../account-login/index.html">Log in </a></p>
		</div>
	</div>
@endsection