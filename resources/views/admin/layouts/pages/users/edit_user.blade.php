@extends('layouts.app')
@section('content')
    <div class="row page-title-header">
        <div class="col-12">
            <div class="page-header">
                <h4 class="page-title">Edit User</h4>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('update.user') }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{ Crypt::encryptString($user->id) }}" />
                            <div class="row form-group">
                                <label class="col-sm-2 col-form-label">Name/Phone number</label>
                                <div class="col-sm-9">
                                    <input type="text" name="name" value="{{ $user->name }}" class="form-control" placeholder="Enter name" />
                                </div>
                            </div>

                            <div class="row form-group">
                                <label class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-9">
                                    <input type="text" name="email" value="{{ (empty($user->email) == true) ? $user->phone_number : $user->email }}" class="form-control" placeholder="Enter email" readonly/>
                                </div>
                            </div>

                            <div class="row form-group">
                                <label class="col-sm-2 col-form-label">Password</label>
                                <div class="col-sm-9">
                                    <input type="password" name="password" value="" class="form-control" placeholder="Enter Password" />
                                </div>
                            </div>

                            <div class="row form-group">
                                <label class="col-sm-2 col-form-label">Confirm Password</label>
                                <div class="col-sm-9">
                                    <input type="password" name="password_confirmation" value="" class="form-control" placeholder="Enter Password" />
                                </div>
                            </div>

                            <div class="row form-group">
                                <label class="col-sm-2 col-form-label">Role Type</label>
                                <div class="col-sm-9">
                                    <select name="role" value="" class="form-control">
                                        <option value="0">Select Role</option>
                                        @foreach($roles as $role)
                                            <option value="{{ Crypt::encryptString($role->id) }}" {{ ($role->id == $user->role) ? "selected" : "" }}>{{ $role->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-11">
                                    <a href="{{ route('user.list') }}" class="btn btn-primary float-right m-sm-1">Cancel</a>&nbsp;
                                    <button type="submit" class="btn btn-primary float-right m-sm-1">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
