@extends('admin.layouts.base')
@section('css')
    <link rel="stylesheet" href="{{ asset('datatable/datatables.min.css') }}">
@endsection
@section('content')
<div class="container-fluid">
    <h3 class="page-title"></h3>
    <div class="row">
        <div class="col-md-12">
            <!-- BASIC TABLE -->
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Sample Type List</h3>
                </div>
                <div class="panel-heading">
                    <a class="btn btn-primary" href="{{url('admin/add/sample/type')}}">Add Sample Type</a>
                </div>
                <div class="panel-body">
                    <table class="table table-responsive-sm table-bordered table-striped table-sm">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Sample Type</th>
                                <th>Vacutainer</th>
                                <th>Anticoagulant</th>
                                <th>Capacity</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                </div>
            </div>
            <!-- END BASIC TABLE -->
        </div>
    </div>
</div>
@endsection
@section('script')
    <script src="{{ asset('datatable/datatables.min.js') }}"></script>
    <script type="text/javascript">
        $(function ($) {
            $(document).ready(function ($) {
                var result = [];
                $('.table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url:"{{ route('sample.type.datatable.request') }}",
                        type: "POST",
                        data: {
                            _token: "{{ csrf_token() }}",
                        },
                    },
                    columns: [

                        { data: 'DT_RowIndex', name: 'DT_RowIndex' , orderable: false, searchable: false},
                        { data: "smaple_type", name: 'smaple_type', render:function (data, type, full, meta)
                        	{
                        		let sampleType = data.split('@');
                                return '<span class="badge '+sampleType[0]+'">'+sampleType[1]+'</span>';
                            }
                        },
                        { data: "vacutainer", name: 'vacutainer' },
                        { data: "anticoagulant", name: 'anticoagulant' },
                        { data: "capacity", name: 'capacity' },
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
		        $.fn.dataTable.ext.errMode = 'none';
            });
        });
    </script>
@endsection

