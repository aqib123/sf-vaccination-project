@extends('admin.layouts.base')
@section('content')
@php
    $smaple_type = null;
    $vacutainer = null;
    $anticoagulant = null;
    $capacity = null;
    $heading = 'Add Sample Type';
    if(isset($sampleType)) {
        $heading = 'Update Sample Type';
        $smaple_type = $sampleType['smaple_type'];
        $vacutainer = $sampleType['vacutainer'];
        $anticoagulant = $sampleType['anticoagulant'];
        $capacity = $sampleType['capacity'];
    }
@endphp
<div class="container-fluid">
    <h3 class="page-title"></h3>
    <div class="row">
        <div class="col-md-12">
            @include('error.flash_message')
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">{{$heading}}</h3>
                </div>
                <div class="panel-body">
                    <form action="{{ route('add.sample.type') }}" method="post">
                        {{ csrf_field() }}
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Sample Type</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="smaple_type">
                                    <option value="">Select Options</option>
                                    <option value="bg-magenda@A" {{ (old('smaple_type', $smaple_type) == 'bg-magenda@A' ? "selected":"") }}><span class="badge badge-pill bg-bg-magenda">A</span></option>
                                    <option value="badge-warning@B" {{ (old('smaple_type', $smaple_type) == 'badge-warning@B' ? "selected":"") }}><span class="dot gold">B</span></option>
                                    <option value="bg-primary@C" {{ (old('smaple_type', $smaple_type) == 'bg-primary@C' ? "selected":"") }}><span class="dot light-blue">C</span></option>
                                    <option value="badge-danger@F" {{ (old('smaple_type', $smaple_type) == 'badge-danger@F' ?"selected":"") }}><span class="dot red">F</span></option>
                                    <option value="badge-secondary@G" {{ (old('smaple_type', $smaple_type) == 'badge-secondary@G' ?"selected":"") }}><span class="dot grey">G</span></option>
                                    <option value="badge-success@H" {{ (old('smaple_type', $smaple_type) == 'badge-success@H' ?"selected":"") }}><span class="dot green">H</span></option>
                                    <option value="bg-dark-blue@K" {{ (old('smaple_type', $smaple_type) == 'bg-dark-blue@K' ?"selected":"") }}><span class="dot dark-blue">K</span></option>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Vacutainer</label>
                            <div class="col-sm-9">
                                <input type="text" name="vacutainer" value="{{$vacutainer}}" class="form-control" placeholder="Enter Vacutainer" />
                                @if(isset($sampleType))
                                    <input type="hidden" name="id" value="{{$sampleType['id']}}">
                                @endif
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Anticoagulant</label>
                            <div class="col-sm-9">
                                <input type="text" name="anticoagulant" value="{{$anticoagulant}}" class="form-control" placeholder="Enter Anticoagulant" />
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Capacity</label>
                            <div class="col-sm-9">
                                <input type="text" name="capacity" value="{{$capacity}}" class="form-control" placeholder="Enter Capacity" />
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-11">
                                <button type="submit" class="btn btn-primary float-right offset-sm-2">Create</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
