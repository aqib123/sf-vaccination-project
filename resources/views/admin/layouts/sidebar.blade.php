@inject('GeneralFunctions', 'App\Role')
<div id="sidebar-nav" class="sidebar">
	<div class="sidebar-scroll">
		<nav>
			<ul class="nav">
				@if($GeneralFunctions->checkViewPermission('dashboard'))
				<li><a href="{{url('admin/dashboard')}}" class="active"><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>
				@endif
				@if($GeneralFunctions->checkViewPermission('tutorial.request'))
				<li><a href="{{url('admin/tutorial/request')}}" class=""><i class="lnr lnr-cog"></i> <span>Booking Request</span></a></li>
				@endif
				@if($GeneralFunctions->checkViewPermission('parents.list'))
				<li><a href="{{url('admin/register/parents/details')}}" class=""><i class="lnr lnr-chart-bars"></i> <span>FAQ'S Details</span></a></li>
				@endif
				@if($GeneralFunctions->checkViewPermission('student.list'))
				<li><a href="{{url('admin/register/students/details')}}" class=""><i class="lnr lnr-cog"></i> <span>Contact Us Details</span></a></li>
				@endif
				@if($GeneralFunctions->checkViewPermission('student.list'))
				<li><a href="{{url('admin/register/students/details')}}" class=""><i class="lnr lnr-cog"></i> <span>Clients Details</span></a></li>
				@endif
				{{-- @if($GeneralFunctions->checkViewPermission('roles.list'))
				<li>
					<a href="#rolesPage" data-toggle="collapse" class="collapsed"><i class="lnr lnr-file-empty"></i> <span>Ecommerce Section</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
					<div id="rolesPage" class="collapse ">
						<ul class="nav">
							<li><a href="{{url('admin/add/role')}}" class="">Product Details</a></li>
							<li><a href="{{url('admin/role/list')}}" class="">Orders Details</a></li>
							<li><a href="{{url('admin/role/list')}}" class="">Transaction Reports</a></li>
						</ul>
					</div>
				</li>
				@endif --}}
				@if($GeneralFunctions->checkViewPermission('roles.list'))
				<li>
					<a href="#rolesPage" data-toggle="collapse" class="collapsed"><i class="lnr lnr-file-empty"></i> <span>Roles</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
					<div id="rolesPage" class="collapse ">
						<ul class="nav">
							<li><a href="{{url('admin/add/role')}}" class="">Add Role</a></li>
							<li><a href="{{url('admin/role/list')}}" class="">Roles List</a></li>
						</ul>
					</div>
				</li>
				@endif
				@if($GeneralFunctions->checkViewPermission('user.list'))
				<li>
					<a href="#usersPage" data-toggle="collapse" class="collapsed"><i class="lnr lnr-file-empty"></i> <span>Admin Users</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
					<div id="usersPage" class="collapse ">
						<ul class="nav">
							<li><a href="{{url('admin/add/user')}}" class="">Add User</a></li>
							<li><a href="{{url('admin/user/list')}}" class="">User List</a></li>
						</ul>
					</div>
				</li>
				@endif
			</ul>
		</nav>
	</div>
</div>