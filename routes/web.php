<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*----------  Website Pages  ----------*/
Route::get('/', function () {
    return view('website.layouts.pages.home');
});

Route::get('tests/sample-requirements', 'WebControllers\TestsTypesController@getSampleTypeRecords');

Route::get('tests/special-instructions', 'WebControllers\TestsTypesController@getspecialInstrauctionsRecord');

Route::get('tests/tests-a-z/{code}', 'WebControllers\TestsTypesController@getTestAtoZ');

Route::get('tests/tests-a-z/{code}/{testId}', 'WebControllers\TestsTypesController@getTestDetails');

Route::get('travel/clinic', 'WebControllers\TravelClinicController@index');

Route::get('occupational-health-services', 'WebControllers\OccupationalHealthController@index');

Route::get('occupation-health-vaccination', 'WebControllers\OccupationalHealthController@occupationalHealthVaccination');

Route::get('occupational-health-blood-test', 'WebControllers\OccupationalHealthController@occupationalHealthBloodTest');

Route::get('medical', 'WebControllers\OccupationalHealthController@occupationalMedical');

Route::get('corporate_service', 'WebControllers\CorporateServiceController@index');

Route::get('onsite_vaccination_clinic', 'WebControllers\CorporateServiceController@onsiteVaccinationClinic');

Route::get('onsite_testing_service', 'WebControllers\CorporateServiceController@onsiteTestingService');

Route::get('travel/vaccination', 'WebControllers\CorporateServiceController@travelVaccination');

Route::get('anti/malarial', 'WebControllers\CorporateServiceController@antiMalarial');

Route::get('employee_health', 'WebControllers\CorporateServiceController@employeeHealth');

Route::get('/private-vaccinations', function () {
    return view('website.layouts.pages.privatevaccination');
});

Route::get('/covid-19-service-and-tests', function () {
    return view('website.layouts.pages.covidservicetest');
});

Route::get('/vitamin-deficiencies', function () {
    return view('website.layouts.pages.vitamindeficiencies');
});

Route::get('/covid-19-fit-to-fly', function () {
    return view('website.layouts.pages.covidfittofly'); 
});

Route::get('/covid-19-london-clinic-testing', function () {
    return view('website.layouts.pages.londonclinictesting'); 
});

Route::get('/covid-19-international-arrivals-testing-day-2-and-8', function () {
    return view('website.layouts.pages.covidtestingday2and8'); 
});

Route::get('/product/covid-19-day-2-international-arrivals', function () {
    return view('website.layouts.pages.covid19and2internationalarrivals'); 
});

// Route::get('vitamin-deficiencies', 'WebControllers\OccupationalHealthController@vitaminDeficiencies');

Route::get('hyperhidrosis', 'WebControllers\OccupationalHealthController@hyperhidrosis');

Route::get('chronic-migrain', 'WebControllers\OccupationalHealthController@chronicMigrain');

Route::get('/about', function () {
    return view('website.layouts.pages.about');
});

Route::get('/contact_us', function () {
    return view('website.layouts.pages.contact_us');
});

Route::get('/login_account', 'WebControllers\UserController@getLoginScreen');
Route::post('login_account', 'WebControllers\UserController@login');

Route::get('/registeration_account', function () {
    return view('website.layouts.auth.register');
});

Route::get('/signup_parent', function () {
    return view('website.layouts.auth.signup_parent');
});

Route::get('/signup_student', function () {
    return view('website.layouts.auth.signup_student');
});

Route::post('register_account', 'WebControllers\UserController@register');

Route::group(['middleware' => 'client.auth'], function () {
    Route::prefix('user')->group(function () {
        Route::get('profile', function () {
            return view('user.layouts.pages.profile');
        })->name('user.profile');

        Route::get('discount', function () {
            return view('user.layouts.pages.discount');
        })->name('user.discount');

        Route::get('logout', 'WebControllers\UserController@logout');
    });
});
/*----------  End of Website Routes  ----------*/

/*================================================
=            Section for Admin Portal           =
================================================*/
Auth::routes();
Route::prefix('admin')->group(function () {
	Route::get('login', function () {
        return view('admin.layouts.pages.login');
    })->name('admin.login')->middleware('guest');

    Route::get('forget/password', function () {
        return view('auth.passwords.email');
    })->name('admin.forget.password');
    
    Route::group(['middleware' => 'admin.auth'], function () {
        Route::get('dashboard', 'AdminControllers\DashboardController@index');

        Route::get('tutorial/request', 'AdminControllers\TutorialRequestController@index');

        Route::get('register/parents/details', 'AdminControllers\ClientDetailController@parentsList');

        Route::get('register/students/details', 'AdminControllers\ClientDetailController@studentsList');

        /*=======================================================
        =            Section For Access Control List            =
        =======================================================*/
        Route::get('add/role', 'AdminControllers\RolesController@roleForm')->name('role.form');
        Route::post('add/role', 'AdminControllers\RolesController@addRoleWithPermissions')->name('add.role');
        Route::get('role/list', 'AdminControllers\RolesController@rolesList')->name('role.list');
        Route::get('edit/role/{id}', 'AdminControllers\RolesController@editRole')->name('edit.role');
        Route::get('role/delete/{id}', 'AdminControllers\RolesController@deleteRole')->name('delete.role');
        Route::post('role/list', 'AdminControllers\RolesController@dataTableRequest')->name('role.datatable.request');

        Route::get('add/user', 'AdminControllers\UserController@userForm')->name('add.user.form');
        Route::post('add/user', 'AdminControllers\UserController@addUser')->name('add.user');
        Route::get('user/list', 'AdminControllers\UserController@userList')->name('user.list');
        Route::get('edit/user/{id}', 'AdminControllers\UserController@editUser')->name('edit.user');
        Route::post('update/user', 'AdminControllers\UserController@updateUser')->name('update.user');
        Route::get('delete/user/{id}', 'AdminControllers\UserController@deleteUser')->name('delete.user');
        Route::post('user/list', 'AdminControllers\UserController@dataTableRequest')->name('user.datatable.request'); 
        /*=====  End of Section For Access Control List  ======*/

        /*========================================================
        =            Section for Sample Types Actions            =
        ========================================================*/
        Route::get('add/sample/type', 'AdminControllers\SampleTypesController@sampleTypeForm')->name('add.sample.type.form');
        Route::post('add/sample/type', 'AdminControllers\SampleTypesController@store')->name('add.sample.type');
        Route::get('sample/type/list', 'AdminControllers\SampleTypesController@index')->name('sample.type.list');
        Route::get('edit/sample/type/{id}', 'AdminControllers\SampleTypesController@editSampleType')->name('edit.sample_type.form');
        Route::get('delete/sample/type/{id}', 'AdminControllers\SampleTypesController@deleteSampleType')->name('delete.sample.type');
        Route::post('sample/type/list', 'AdminControllers\SampleTypesController@dataTableRequest')->name('sample.type.datatable.request');
        /*=====  End of Section for Sample Types Actions  ======*/
        
        
    });
});
/*=====  End of Section for Admin Portal ======*/
