<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class ScreenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $carbon = Carbon::now();
        DB::table('screens_details')->insert([
            [
                'name' => 'Dashboard',
                'code' => 'dashboard',
                'created_at' => $carbon
            ],
            [
                'name' => 'Tutorial Request',
                'code' => 'tutorial.request',
                'created_at' => $carbon
            ],
            [
                'name' => 'Parents_list',
                'code' => 'parents.list',
                'created_at' => $carbon
            ],
            [
                'name' => 'Students List',
                'code' => 'student.list',
                'created_at' => $carbon
            ],
        ]);

        DB::table('screen_uri')->insert([
            [
                'screen_id' => 1,
                'uri' => '/dashboard'
            ],[
                'screen_id' => 2,
                'uri' => '/tutorial/request'
            ],[
                'screen_id' => 3,
                'uri' => '/register/parents/details'
            ],[
                'screen_id' => 4,
                'uri' => '/register/students/details'
            ],
            ]
        );
    }
}
