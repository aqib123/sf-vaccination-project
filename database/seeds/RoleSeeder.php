<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('roles')->insert([[
            'name' => 'Admin',
            'created_by' => 1,
            'created_at' => \Carbon\Carbon::now()
        ],
        [
            'name' => 'Manager',
            'created_by' => 1,
            'created_at' => \Carbon\Carbon::now()
        ],
        ]);
    }
}
