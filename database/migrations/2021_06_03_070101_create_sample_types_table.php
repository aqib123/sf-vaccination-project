<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSampleTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sample_types', function (Blueprint $table) {
            $table->id();
            $table->string('smaple_type', 300)->comment="https://www.tdlpathology.com/tests/sample-requirements/";
            $table->string('vacutainer', 300);
            $table->string('anticoagulant', 300)->nullable();
            $table->string('capacity', 300)->nullable();
            $table->index(['smaple_type', 'vacutainer']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sample_types', function (Blueprint $table)
        {
            $table->dropIndex(['smaple_type', 'vacutainer']);
        });
        Schema::dropIfExists('sample_types');
    }
}
