<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormRequestMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_request_migration', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email', 50)->index();
            $table->string('full_name', 100);
            $table->tinyInteger('type')->index();
            $table->string('country',100)->index();
            $table->string('phone_no',100);
            $table->tinyInteger('status')->index();
            $table->text('subjects');
            $table->text('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_request_migration');
    }
}
