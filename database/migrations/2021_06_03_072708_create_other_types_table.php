<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOtherTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('other_types', function (Blueprint $table) {
            $table->id();
            $table->string('smaple_type', 300)->comment="https://www.tdlpathology.com/tests/sample-requirements/";
            $table->text('description')->nullable();
            $table->timestamps();
            $table->index(['smaple_type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('other_types', function (Blueprint $table)
        {
            $table->dropIndex(['smaple_type']);
        });
        Schema::dropIfExists('other_types');
    }
}
